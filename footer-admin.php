			<div class="divider"></div>
			<footer class="container">
				<p class="center-text"><?php copyright(); ?></p>
			</footer>
			<?php get_template_part('templates/modals/modals'); ?>
			<?php get_template_part('templates/app/loader'); ?>
		</main>	
		<?php wp_footer(); ?>
	</body>
</html>