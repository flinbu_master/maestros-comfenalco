<?php
    $current_url = $_SERVER['HTTP_HOST'];
    if ('maestros.comfenalcovalle.com.co' == $current_url) {
        wp_redirect(get_bloginfo('url'), 301);
        exit;
    }

    //Theme
    get_template_part('inc/theme/bootstrap-menu');
    get_template_part('inc/theme/acf-nav-menu-field');
    get_template_part('inc/theme/functions');
    get_template_part('inc/theme/features');
    get_template_part('inc/theme/image-size');
    get_template_part('inc/theme/menu');
    get_template_part('inc/theme/post-types');
    get_template_part('inc/theme/sidebar');
    get_template_part('inc/theme/taxonomy');
    get_template_part('inc/theme/app-launcher');
    get_template_part('inc/theme/registro-concurso');
    get_template_part('inc/theme/data');
    get_template_part('inc/theme/ajax');
    get_template_part('inc/theme/permalinks');
    get_template_part('inc/theme/scripts');
    get_template_part('inc/theme/loader');  
?>