<?php
    function add_nav_menus() {
        register_nav_menus(array(
            'header_menu' => 'Menú Cabecera',
            'footer_menu' => 'Menú Pie de Página',
            'admin_menu' => 'Menú Backend'
        ));
    }    
    add_action('init', 'add_nav_menus');

    //Add custom items
    function add_custom_menu_items($items, $args) {
        if ($args->theme_location == 'header_menu') {
            $custom_items = array();
            if (is_user_logged_in()) {
                $custom_items[] = '<a href="' . get_permalink(get_theme_option('dashboard_page')) . '" title="Dashboard">Administrar</a>';
                $custom_items[] = '<a href="#logout" class="logout-btn">Cerrar Sesión</a>';
            } else {
                $custom_items[] = '<a href="#login" data-target="#login-modal" data-toggle="modal">Iniciar Sesión</a>';
            }
            
            foreach($custom_items as $item) {
                $items .= '<li class="menu-item">' . $item . '</li>';
            }
            
            return $items;
        }
    }
    add_filter('wp_nav_menu_items', 'add_custom_menu_items', 10, 2);
?>