<?php
    function logout(){
    	$nonce = $_REQUEST['nonce'];
    	if(!wp_verify_nonce($nonce, 'user_nonce')){
    		die('Security Check Fail');
		} else {
			wp_logout();
			echo'logged_out';
			die();
		}    	
    }
    add_action('wp_ajax_logout', 'logout');
    add_action('wp_ajax_nopriv_logout', 'logout');
    
	function login_app(){
        $nonce = $_REQUEST['nonce'];
		if(!wp_verify_nonce($nonce, 'user_nonce')){
			die('Security Check Fail');
		} else {
			$credentials = array(
				'user_login' => $_REQUEST['log'],
				'user_password' => $_REQUEST['pwd']
			);
			$login = wp_signon($credentials, true);
			if(is_wp_error($login)){
				$back = array(
					'error' => error_message($login->get_error_code())
				);
				echo json_encode($back);
				die();
			} else {
                $back = array(
                    'user' => $login
                );
                echo json_encode($back);
			 die();
			}
			die();
		}
	}
    add_action('wp_ajax_login_app', 'login_app');
    add_action('wp_ajax_nopriv_login_app', 'login_app');

    function update_user() {
    	if (!wp_verify_nonce($_POST['nonce'], 'user_nonce')) die('Security Fail!');

    	$user_id = $_POST['user_id'];
    	$name = $_POST['name'];
		$second_name = $_POST['second_name'];
		$lastname = $_POST['lastname'];
		$second_lastname = $_POST['second_lastname'];
		$city = $_POST['city'];
		$phone = $_POST['phone'];
		$address = $_POST['address'];
		$hood = $_POST['hood'];
		$marital_status = $_POST['marital_status'];
		$scale = $_POST['scale'];
		$decree = $_POST['decree'];
		$home = $_POST['home'];
		$saves = $_POST['saves'];
		$sports = $_POST['sports'];
		$family_activities = $_POST['family_activities'];
		$family_place = $_POST['family_place'];
		$family_programs = $_POST['family_programs'];
		$programs = $_POST['programs'];
		$pwd = $_POST['pwd'];
		$asesor = $_POST['asesor'];
        $institute = $_POST['institute'];
		$childrens = $_POST['childrens'];
		$child_1 = $_POST['child_1'];
		$child_2 = $_POST['child_2'];
		$child_3 = $_POST['child_3'];
		$child_4 = $_POST['child_4'];
		$child_5 = $_POST['child_5'];
		$child_6 = $_POST['child_6'];
		$afiliation = $_POST['afiliation'];
		$ter_con = $_POST['ter_con'];
		$birthday = $_POST['birthday'];
		$display_name = $name . ' ' . $lastname;

		$user_data = array(
			'ID' => $user_id,
			'display_name' => $display_name,
			'first_name' => $name,
			'last_name' => $lastname
		);

		$update_user = wp_update_user($user_data);

		if (!is_wp_error($update_user)) {
	        $hijo_1 = ($child_1) ? $child_1 : 0;
	        $hijo_2 = ($child_2) ? $child_2 : 0;
	        $hijo_3 = ($child_3) ? $child_3 : 0;
	        $hijo_4 = ($child_4) ? $child_4 : 0;
	        $hijo_5 = ($child_5) ? $child_5 : 0;
	        $hijo_6 = ($child_6) ? $child_6 : 0;

		    $edades_hijos = "$hijo_1/$hijo_2/$hijo_3/$hijo_4/$hijo_5/$hijo_6";
		    $update_data = "`NOMBRE`='$name',
		    				`SEGUNDO_NOMBRE`='$second_name',
		    				`APELLIDO`='$lastname',
		    				`SEGUNDO_APELLIDO`='$second_lastname',
		    				`CIUDAD`='$city',
		    				`CELULAR`='$phone',
		    				`DIRECCION`='$address',
		    				`BARRIO`='$hood',
		    				`INSTITUCION`='$institute',
		    				`CANT_HIJOS`='$childrens',
		    				`EDADES_HIJOS`='$edades_hijos',
		    				`EST_CIVIL`='$marital_status',
		    				`ESCALAFON`='$scale',
		    				`DECRETO`='$decree',
		    				`NOM_ASESOR`='$asesor',
		    				`VIVIENDA`='$home',
		    				`DEPORTE`='$sports',
		    				`ACT_TL_FAMILIA`='$family_activities',
		    				`LUGAR_TL_FAMILIA`='$family_place',
		    				`PROG_EDU`='$programs',
		    				`CURSOS_ESP_PADRES`='$family_programs',
		    				`AHORROS`='$saves',
		    				`CUMPLEANOS`='$birthday'";

		    $db_data = get_db_connection_data();
		    $document = get_user_meta($user_id, 'document', true);
		    $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
		    mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());
		    $query = "UPDATE `wp_registro_maestro` SET $update_data WHERE `NUM_DOCUMENTO`='$document'";
		    mysql_query($query, $sql_maestros_connect);

		    if (is_pioneer($user_id)) {
		    	$pquery = "UPDATE `wp_registro_maestro` SET `PIONEER`='0' WHERE `NUM_DOCUMENTO`='$document'";
		    	mysql_query($pquery, $sql_maestros_connect) or die(mysql_error());

		    	$userdata = get_userdata($user_id);
		    	$email = $userdata->user_email; 
			    $puntos = get_theme_option('update_points');
			    $accion_punto = 'Actualización de datos';
			    $query_puntos_2 = "INSERT INTO `wp_act_puntos`(`CEDULA`,`email_act_puntos`,`punto_recibido`,`accion_punto`,`email_referido`,`fecha_de_ingreso`,`fecha_activacion`,`ACTIVE`) VALUES ('$document','$email','$puntos','$accion_punto','null',NOW(),NOW(),'1')";
		    	mysql_query($query_puntos_2, $sql_maestros_connect) or die(mysql_error());
		    }

		    mysql_close($sql_maestros_connect);

		    if ($pwd) {
		    	wp_set_password($pwd, $user_id);
			    $response = array(
			    	'success' => 2
			    );
		    } else {
			    $response = array(
			    	'success' => 1
			    );		    	
		    }
		} else {
			$response = array(
				'error' => $update_user->get_error_message()
			);
		}

		echo json_encode($response);
		wp_die();
    }
    add_action('wp_ajax_update_user', 'update_user');
    add_action('wp_ajax_nopriv_update_user', 'update_user');

    function register_refered() {
    	if (!wp_verify_nonce($_POST['nonce'], 'ref_nonce')) die('Security Fail!');
        
		$name = $_POST['name'];
		$second_name = $_POST['second_name'];
		$lastname = $_POST['lastname'];
		$second_lastname = $_POST['second_lastname'];
		$doc_type = $_POST['doc_type'];
		$document = $_POST['documento'];
		$city = $_POST['city'];
		$phone = $_POST['phone'];
		$address = $_POST['address'];
		$hood = $_POST['hood'];
		$marital_status = $_POST['marital_status'];
		$scale = $_POST['scale'];
		$decree = $_POST['decree'];
		$home = $_POST['home'];
		$saves = $_POST['saves'];
		$sports = $_POST['sports'];
		$family_activities = $_POST['family_activities'];
		$family_place = $_POST['family_place'];
		$family_programs = $_POST['family_programs'];
		$programs = $_POST['programs'];
		$ref_code = $_POST['ref_code'];
		$email = $_POST['email'];
		$pwd = $_POST['pwd'];
		$asesor = $_POST['asesor'];
        $institute = $_POST['institute'];
		$childrens = $_POST['childrens'];
		$child_1 = $_POST['child_1'];
		$child_2 = $_POST['child_2'];
		$child_3 = $_POST['child_3'];
		$child_4 = $_POST['child_4'];
		$child_5 = $_POST['child_5'];
		$child_6 = $_POST['child_6'];
		$afiliation = $_POST['afiliation'];
		$ter_con = $_POST['ter_con'];
		$birthdat = $_POST['birthday'];
        $maestro = $_POST['maestro'];

		$display_name = $name . ' ' . $lastname;

		$temporal_username = $email;
		$temporal_email = email_exists($email);

		if (!$temporal_email) {
            
			$new_user_id = wp_create_user($temporal_username, $pwd, $email);
			$the_new_user = wp_update_user(array(
				'ID' => $new_user_id,
				'user_nicename' => sanitize_title($display_name . '-' . $new_user_id),
				'display_name' => $display_name,
				'first_name' => $name,
				'last_name' => $lastname
			));
			if (!is_wp_error($the_new_user)) {
				add_user_meta($new_user_id, 'document', $document);

			    $db_data = get_db_connection_data();

			    $cod_to_refeer = strtoupper(substr($name, 0, 1) . substr($lastname, 0, 1)) . $new_user_id; 
                
                $hijo_1 = ($child_1) ? $child_1 : 0;
                $hijo_2 = ($child_2) ? $child_2 : 0;
                $hijo_3 = ($child_3) ? $child_3 : 0;
                $hijo_4 = ($child_4) ? $child_4 : 0;
                $hijo_5 = ($child_5) ? $child_5 : 0;
                $hijo_6 = ($child_6) ? $child_6 : 0;

			    $edades_hijos = "$hijo_1/$hijo_2/$hijo_3/$hijo_4/$hijo_5/$hijo_6";
			    $fields = "`NOMBRE`,`SEGUNDO_NOMBRE`,`APELLIDO`,`SEGUNDO_APELLIDO`,`TIPO_DOCUMENTO`,`NUM_DOCUMENTO`,`CIUDAD`,`CELULAR`,`DIRECCION`,`BARRIO`,`INSTITUCION`,`EMAIL`,`TER_CON`,`CANT_HIJOS`,`EDADES_HIJOS`,`EST_CIVIL`,`ESCALAFON`,`DECRETO`,`NOM_ASESOR`,`VIVIENDA`,`DEPORTE`,`LUGAR_DEPORTE`,`ACT_TL_FAMILIA`,`LUGAR_TL_FAMILIA`,`PROG_EDU`,`CURSOS_ESP_PADRES`,`CURSOS_HIJOS`,`AHORROS`,`DIGITADOR`,`EMPRESA`,`VAL_AFILIADO`,`COD_REFERIDOS`,`FECHA_INGRESO`,`CUMPLEANOS`,`MAESTRO`";
			    $values = "'$name','$second_name','$lastname','$second_lastname','$doc_type','$document','$city','$phone','$address','$hood','$institute','$email','$ter_con','$childrens','$edades_hijos','$marital_status','$scale','$decree','$asesor','$home','$sports',0,'$family_activities','$family_place','$programs','$family_programs',0,'$saves',0,0,$afiliation,'$cod_to_refeer',NOW(),'$birthday','$maestro'";
			    $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
			    mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());
                
			    $query = "INSERT INTO `wp_registro_maestro`($fields) VALUES ($values)";
                
			    mysql_query($query, $sql_maestros_connect) or die(mysql_error());

			    $refer_query = "UPDATE `lista_referidos` SET `fecha_activacion`=NOW(),`ACTIVE`='1' WHERE `EMAIL`='$email'";
			    mysql_query($refer_query, $sql_maestros_connect) or die(mysql_error());
                
                if ($maestro) {
			         $query_puntos = "UPDATE `wp_act_puntos` SET `ACTIVE`='1',`fecha_activacion`=NOW() WHERE `email_referido`='$email'";
                } else {
                    $puntos_no_maestro = get_theme_option('ref_no_maestro_points');
                    $query_puntos = "UPDATE `wp_act_puntos` SET `ACTIVE`='1',`fecha_activacion`=NOW(),`punto_reibido`='$puntos_no_maestro' WHERE `email_referido`='$email'";
                }
			    mysql_query($query_puntos, $sql_maestros_connect);

			    $puntos = get_theme_option('reg_points');
			    $accion_punto = 'Registro completo';
			    $query_puntos_2 = "INSERT INTO `wp_act_puntos`(`CEDULA`,`email_act_puntos`,`punto_recibido`,`accion_punto`,`email_referido`,`fecha_de_ingreso`,`fecha_activacion`,`ACTIVE`) VALUES ('$document','$email','$puntos','$accion_punto','null',NOW(),NOW(),'1')";
			    mysql_query($query_puntos_2, $sql_maestros_connect) or die(mysql_error());

			    mysql_close($sql_maestros_connect);

			    update_user_points($document);
                
                if ($maestro == 0) {
        
                    //EMAIL

                    $html = '<h3>Datos del usuario no afiliado:</h3><br />';
                    $html .= "<strong>Nombres:</strong> $name $second_name<br />";
                    $html .= "<strong>Apellidos:</strong> $lastname $second_lastname<br />";
                    $html .= "<strong>Tipo de documento:</strong> $doc_type<br />";
                    $html .= "<strong>Número de documento:</strong> $document<br />";
                    $html .= "<strong>Fecha de nacimiento:</strong> $birthday<br />";
                    $html .= "<strong>Ciudad:</strong> $city<br />";
                    $html .= "<strong>Celular:</strong> $phone<br />";
                    $html .= "<strong>Dirección:</strong> $address<br />";
                    $html .= "<strong>Barrio:</strong> $hood<br />";
                    $html .= "<strong>Institución Educativa:</strong> $institute<br />";
                    $html .= "<strong>Correo Electrónico:</strong> $email<br />";
                    $html .= "<strong>Número de Hijos:</strong> $childrens<br />";
                    $ages = explode('/', $edades_hijos);
                    for ($i = 0; $i < $childrens; $i++) {
                        $n = $i + 1;
                        $age = $ages[$i];
                        $html .= "<strong>Edad Hijo $n:</strong> $age<br />";
                    }
                    $html .= "<strong>Estado Civil:</strong> $marital_status<br />";
                    $html .= "<strong>Escalafón:</strong> $scale<br />";
                    $html .= "<strong>Decreto que lo ampara:</strong> $decree<br />";
                    $html .= "<strong>Asesor que lo atendió:</strong> $asesor<br />";
                    $html .= "<strong>¿Tiene vivienda propia?:</strong> " . ($home == 1) ? 'Si' : 'No' . "<br />";
                    $html .= "<strong>¿Tiene ahorros para adquirir vivienda propia?:</strong> " . ($saves == 1) ? 'Si' : 'No' . "<br />";
                    $html .= "<strong>Deporte que practica:</strong> $sports<br />";
                    $html .= "<strong>Actividades en familia:</strong> $family_activities<br />";
                    $html .= "<strong>Lugar actividades en familia:</strong> $family_place<br />";
                    $html .= "<strong>Programas educativos:</strong> $programs<br />";
                    $html .= "<strong>Programas educativos para familia / hijos:</strong> $family_programs<br />";
                    $html .= "<strong>Programas educativos para familia / hijos:</strong> $family_programs<br /><br />";
                    $auth_link = get_bloginfo('url') . '/backend/authorize/' . $document . '/';
                    $html .= '<strong>Para autorizar este usuario, haga click <a href="' . $auth_link . '">AQUÍ</a></strong><br /><br />Ó, copie y pegue en su navegador: ' . $auth_link;

                    $email_destinatario = get_theme_option('email_verificacion');

                    $headers = "From: Maestros de la gente <no-reply@maestroscomfenalco.co>\r\n";
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

                    $subject = get_theme_option('asunto_verificacion');

                    $mail = wp_mail($email_destinatario, $subject, $html, $headers);
                }

				$response = array(
					'success' => 1
				);
			} else {
				$response = array(
					'error' => $the_new_user->get_error_message()
				);
			}
		} else {
			$response = array(
				'error' => 'El email ya se encuentra registrado'
			);
		}
        
        echo json_encode($response);
        wp_die();
    }
    add_action('wp_ajax_register_refered', 'register_refered');
    add_action('wp_ajax_nopriv_register_refered', 'register_refered');

    function register_user() {
    	if (!wp_verify_nonce($_POST['nonce'], 'ref_nonce')) die('Security Fail!');
        
		$name = (isset($_POST['name'])) ? $_POST['name'] : 'null';
		$second_name = (isset($_POST['second_name'])) ? $_POST['second_name'] : 'null';
		$lastname = (isset($_POST['lastname'])) ? $_POST['lastname'] : 'null';
		$second_lastname = (isset($_POST['second_lastname'])) ? $_POST['second_lastname'] : 'null';
		$doc_type = (isset($_POST['doc_type'])) ? $_POST['doc_type'] : 'null';
		$document = (isset($_POST['documento'])) ? $_POST['documento'] : 'null';
		$city = (isset($_POST['city'])) ? $_POST['city'] : 'null';
		$phone = (isset($_POST['phone'])) ? $_POST['phone'] : 'null';
		$address = (isset($_POST['address'])) ? $_POST['address'] : 'null';
		$hood = (isset($_POST['hood'])) ? $_POST['hood'] : 'null';
		$marital_status = (isset($_POST['marital_status'])) ? $_POST['marital_status'] : 'null';
		$scale = (isset($_POST['scale'])) ? $_POST['scale'] : 'null';
		$decree = (isset($_POST['decree'])) ? $_POST['decree'] : 'null';
		$home = (isset($_POST['home'])) ? $_POST['home'] : 'null';
		$saves = (isset($_POST['saves'])) ? $_POST['saves'] : 'null';
		$sports = (isset($_POST['sports'])) ? $_POST['sports'] : 'null';
		$family_activities = (isset($_POST['family_activities'])) ? $_POST['family_activities'] : 'null';
		$family_place = (isset($_POST['family_place'])) ? $_POST['family_place'] : 'null';
		$family_programs = (isset($_POST['family_programs'])) ? $_POST['family_programs'] : 'null';
		$programs = (isset($_POST['programs'])) ? $_POST['programs'] : 'null';
		$ref_code = (isset($_POST['ref_code'])) ? $_POST['ref_code'] : 'null';
		$email = (isset($_POST['email'])) ? $_POST['email'] : 'null';
		$pwd = (isset($_POST['pwd'])) ? $_POST['pwd'] : 'null';
		$asesor = (isset($_POST['asesor'])) ? $_POST['asesor'] : 'null';
        $institute = (isset($_POST['institute'])) ? $_POST['institute'] : 'null';
		$childrens = (isset($_POST['childrens'])) ? $_POST['childrens'] : 'null';
		$child_1 = (isset($_POST['child_1'])) ? $_POST['child_1'] : 'null';
		$child_2 = (isset($_POST['child_2'])) ? $_POST['child_2'] : 'null';
		$child_3 = (isset($_POST['child_3'])) ? $_POST['child_3'] : 'null';
		$child_4 = (isset($_POST['child_4'])) ? $_POST['child_4'] : 'null';
		$child_5 = (isset($_POST['child_5'])) ? $_POST['child_5'] : 'null';
		$child_6 = (isset($_POST['child_6'])) ? $_POST['child_6'] : 'null';
		$afiliation = (isset($_POST['afiliation'])) ? $_POST['afiliation'] : 'null';
		$ter_con = (isset($_POST['ter_con'])) ? $_POST['ter_con'] : 'null';
		$birthday = (isset($_POST['birthday'])) ? $_POST['birthday'] : 'null';
        $maestro = $_POST['maestro'];

		$digitador = (isset($_POST['user_id'])) ? $_POST['user_id'] : 'null';

		$display_name = $name . ' ' . $lastname;

		$temporal_username = $email;
		$temporal_email = email_exists($email);

		if (!$temporal_email) {
            
			$new_user_id = wp_create_user($temporal_username, $pwd, $email);
			$the_new_user = wp_update_user(array(
				'ID' => $new_user_id,
				'user_nicename' => sanitize_title($display_name . '-' . $new_user_id),
				'display_name' => $display_name,
				'first_name' => $name,
				'last_name' => $lastname
			));
			if (!is_wp_error($the_new_user)) {
				add_user_meta($new_user_id, 'document', $document);

			    $db_data = get_db_connection_data();

			    $cod_to_refeer = strtoupper(substr($name, 0, 1) . substr($lastname, 0, 1)) . $new_user_id; 
                
                $hijo_1 = ($child_1) ? $child_1 : 0;
                $hijo_2 = ($child_2) ? $child_2 : 0;
                $hijo_3 = ($child_3) ? $child_3 : 0;
                $hijo_4 = ($child_4) ? $child_4 : 0;
                $hijo_5 = ($child_5) ? $child_5 : 0;
                $hijo_6 = ($child_6) ? $child_6 : 0;

			    $edades_hijos = "$hijo_1/$hijo_2/$hijo_3/$hijo_4/$hijo_5/$hijo_6";
			    $fields = "`NOMBRE`,`SEGUNDO_NOMBRE`,`APELLIDO`,`SEGUNDO_APELLIDO`,`TIPO_DOCUMENTO`,`NUM_DOCUMENTO`,`CIUDAD`,`CELULAR`,`DIRECCION`,`BARRIO`,`INSTITUCION`,`EMAIL`,`TER_CON`,`CANT_HIJOS`,`EDADES_HIJOS`,`EST_CIVIL`,`ESCALAFON`,`DECRETO`,`NOM_ASESOR`,`VIVIENDA`,`DEPORTE`,`LUGAR_DEPORTE`,`ACT_TL_FAMILIA`,`LUGAR_TL_FAMILIA`,`PROG_EDU`,`CURSOS_ESP_PADRES`,`CURSOS_HIJOS`,`AHORROS`,`DIGITADOR`,`EMPRESA`,`VAL_AFILIADO`,`COD_REFERIDOS`,`FECHA_INGRESO`,`CUMPLEANOS`,`PIONEER`";
			    $values = "'$name','$second_name','$lastname','$second_lastname','$doc_type','$document','$city','$phone','$address','$hood','$institute','$email','$ter_con','$childrens','$edades_hijos','$marital_status','$scale','$decree','$asesor','$home','$sports',0,'$family_activities','$family_place','$programs','$family_programs',0,'$saves','$digitador',0,0,'$cod_to_refeer',NOW(),'$birthday','1'";
			    $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
			    mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());
                
			    $query = "INSERT INTO `wp_registro_maestro`($fields) VALUES ($values)";
			    mysql_query($query, $sql_maestros_connect) or die(mysql_error());

			    $puntos = get_theme_option('reg_points');
			    $accion_punto = 'Registro';
			    $query_puntos_2 = "INSERT INTO `wp_act_puntos`(`CEDULA`,`email_act_puntos`,`punto_recibido`,`accion_punto`,`email_referido`,`fecha_de_ingreso`,`fecha_activacion`,`ACTIVE`) VALUES ('$document','$email','$puntos','$accion_punto','null',NOW(),NOW(),'1')";
			    mysql_query($query_puntos_2, $sql_maestros_connect) or die(mysql_error());

			    mysql_close($sql_maestros_connect);

			    update_user_points($document);
                
                if ($maestro == 0) {
        
                    //EMAIL

                    $html = '<h3>Datos del usuario no afiliado:</h3><br />';
                    $html .= "<strong>Nombres:</strong> $name $second_name<br />";
                    $html .= "<strong>Apellidos:</strong> $lastname $second_lastname<br />";
                    $html .= "<strong>Tipo de documento:</strong> $doc_type<br />";
                    $html .= "<strong>Número de documento:</strong> $document<br />";
                    $html .= "<strong>Fecha de nacimiento:</strong> $birthday<br />";
                    $html .= "<strong>Ciudad:</strong> $city<br />";
                    $html .= "<strong>Celular:</strong> $phone<br />";
                    $html .= "<strong>Dirección:</strong> $address<br />";
                    $html .= "<strong>Barrio:</strong> $hood<br />";
                    $html .= "<strong>Institución Educativa:</strong> $institute<br />";
                    $html .= "<strong>Correo Electrónico:</strong> $email<br />";
                    $html .= "<strong>Número de Hijos:</strong> $childrens<br />";
                    $ages = explode('/', $edades_hijos);
                    for ($i = 0; $i < $childrens; $i++) {
                        $n = $i + 1;
                        $age = $ages[$i];
                        $html .= "<strong>Edad Hijo $n:</strong> $age<br />";
                    }
                    $html .= "<strong>Estado Civil:</strong> $marital_status<br />";
                    $html .= "<strong>Escalafón:</strong> $scale<br />";
                    $html .= "<strong>Decreto que lo ampara:</strong> $decree<br />";
                    $html .= "<strong>Asesor que lo atendió:</strong> $asesor<br />";
                    $html .= "<strong>¿Tiene vivienda propia?:</strong> " . ($home == 1) ? 'Si' : 'No' . "<br />";
                    $html .= "<strong>¿Tiene ahorros para adquirir vivienda propia?:</strong> " . ($saves == 1) ? 'Si' : 'No' . "<br />";
                    $html .= "<strong>Deporte que practica:</strong> $sports<br />";
                    $html .= "<strong>Actividades en familia:</strong> $family_activities<br />";
                    $html .= "<strong>Lugar actividades en familia:</strong> $family_place<br />";
                    $html .= "<strong>Programas educativos:</strong> $programs<br />";
                    $html .= "<strong>Programas educativos para familia / hijos:</strong> $family_programs<br />";
                    $html .= "<strong>Programas educativos para familia / hijos:</strong> $family_programs<br /><br />";
                    $auth_link = get_bloginfo('url') . '/backend/authorize/' . $document . '/';
                    $html .= '<strong>Para autorizar este usuario, haga click <a href="' . $auth_link . '">AQUÍ</a></strong><br /><br />Ó, copie y pegue en su navegador: ' . $auth_link;

                    $email_destinatario = get_theme_option('email_verificacion');

                    $headers = "From: Maestros de la gente <no-reply@maestroscomfenalco.co>\r\n";
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

                    $subject = get_theme_option('asunto_verificacion');

                    $mail = wp_mail($email_destinatario, $subject, $html, $headers);
                }

				$response = array(
					'success' => 1
				);
			} else {
				$response = array(
					'error' => $the_new_user->get_error_message()
				);
			}
		} else {
			$response = array(
				'error' => 'El email ya se encuentra registrado'
			);
		}
        
        echo json_encode($response);
        wp_die();
    }
    add_action('wp_ajax_register_user', 'register_user');
    add_action('wp_ajax_nopriv_register_user', 'register_user');


    function register_no_afiliated() {
    	if (!wp_verify_nonce($_POST['nonce'], 'ref_nonce')) die('Security Fail!');
        
		$name = $_POST['name'];
		$second_name = $_POST['second_name'];
		$lastname = $_POST['lastname'];
		$second_lastname = $_POST['second_lastname'];
		$doc_type = $_POST['doc_type'];
		$document = $_POST['documento'];
		$city = $_POST['city'];
		$phone = $_POST['phone'];
		$address = $_POST['address'];
		$hood = $_POST['hood'];
		$marital_status = $_POST['marital_status'];
		$scale = $_POST['scale'];
		$decree = $_POST['decree'];
		$home = $_POST['home'];
		$saves = $_POST['saves'];
		$sports = $_POST['sports'];
		$family_activities = $_POST['family_activities'];
		$family_place = $_POST['family_place'];
		$family_programs = $_POST['family_programs'];
		$programs = $_POST['programs'];
		$ref_code = $_POST['ref_code'];
		$email = $_POST['email'];
		$pwd = $_POST['pwd'];
		$asesor = $_POST['asesor'];
        $institute = $_POST['institute'];
		$childrens = $_POST['childrens'];
		$child_1 = $_POST['child_1'];
		$child_2 = $_POST['child_2'];
		$child_3 = $_POST['child_3'];
		$child_4 = $_POST['child_4'];
		$child_5 = $_POST['child_5'];
		$child_6 = $_POST['child_6'];
		$afiliation = $_POST['afiliation'];
		$ter_con = $_POST['ter_con'];
		$birthday = $_POST['birthday'];
                
        $hijo_1 = ($child_1) ? $child_1 : 0;
        $hijo_2 = ($child_2) ? $child_2 : 0;
        $hijo_3 = ($child_3) ? $child_3 : 0;
        $hijo_4 = ($child_4) ? $child_4 : 0;
        $hijo_5 = ($child_5) ? $child_5 : 0;
        $hijo_6 = ($child_6) ? $child_6 : 0;

        $db_data = get_db_connection_data();

        $edades_hijos = "$hijo_1/$hijo_2/$hijo_3/$hijo_4/$hijo_5/$hijo_6";
        $fields = "`NOMBRE`,`SEGUNDO_NOMBRE`,`APELLIDO`,`SEGUNDO_APELLIDO`,`TIPO_DOCUMENTO`,`NUM_DOCUMENTO`,`CIUDAD`,`CELULAR`,`DIRECCION`,`BARRIO`,`INSTITUCION`,`EMAIL`,`TER_CON`,`CANT_HIJOS`,`EDADES_HIJOS`,`EST_CIVIL`,`ESCALAFON`,`DECRETO`,`NOM_ASESOR`,`VIVIENDA`,`DEPORTE`,`LUGAR_DEPORTE`,`ACT_TL_FAMILIA`,`LUGAR_TL_FAMILIA`,`PROG_EDU`,`CURSOS_ESP_PADRES`,`CURSOS_HIJOS`,`AHORROS`,`DIGITADOR`,`EMPRESA`,`VAL_AFILIADO`,`COD_REFERIDOS`,`FECHA_INGRESO`,`CUMPLEANOS`";
        $values = "'$name','$second_name','$lastname','$second_lastname','$doc_type','$document','$city','$phone','$address','$hood','$institute','$email','$ter_con','$childrens','$edades_hijos','$marital_status','$scale','$decree','$asesor','$home','$sports',0,'$family_activities','$family_place','$programs','$family_programs',0,'$saves','$digitador',0,$afiliation,'null',NOW(),'$birthday'";
        $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
        mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());

        $query = "INSERT INTO `wp_registro_maestro`($fields) VALUES ($values)";

        mysql_query($query, $sql_maestros_connect) or die(mysql_error());

        $refer_query = "UPDATE `lista_referidos` SET `fecha_activacion`=NOW(),`ACTIVE`='1' WHERE `EMAIL`='$email'";
        mysql_query($refer_query, $sql_maestros_connect) or die(mysql_error());

        $not_success_points = 0;
        
        $accion_punto = 'Referido no válido (NA)' . $display_name . ' <' . $email . '>';

	    $query_puntos = "UPDATE `wp_act_puntos` SET `punto_recibido`='$not_success_points',`ACTIVE`='1',`fecha_activacion`=NOW(),`ENABLE`='0',`accion_punto`='$accion_punto' WHERE `email_referido`='$email'";
	    mysql_query($query_puntos, $sql_maestros_connect) or die(mysql_error());

	    mysql_close($sql_maestros_connect);

        $response = array(
            'success' => 2
        );
        
        //EMAIL
        
        $html = '<h3>Datos del usuario no afiliado:</h3><br />';
        $html .= "<strong>Nombres:</strong> $name $second_name<br />";
        $html .= "<strong>Apellidos:</strong> $lastname $second_lastname<br />";
        $html .= "<strong>Tipo de documento:</strong> $doc_type<br />";
        $html .= "<strong>Número de documento:</strong> $document<br />";
        $html .= "<strong>Fecha de nacimiento:</strong> $birthday<br />";
        $html .= "<strong>Ciudad:</strong> $city<br />";
        $html .= "<strong>Celular:</strong> $phone<br />";
        $html .= "<strong>Dirección:</strong> $address<br />";
        $html .= "<strong>Barrio:</strong> $hood<br />";
        $html .= "<strong>Institución Educativa:</strong> $institute<br />";
        $html .= "<strong>Correo Electrónico:</strong> $email<br />";
        $html .= "<strong>Número de Hijos:</strong> $childrens<br />";
        $ages = explode('/', $edades_hijos);
        for ($i = 0; $i < $childrens; $i++) {
            $n = $i + 1;
            $age = $ages[$i];
            $html .= "<strong>Edad Hijo $n:</strong> $age<br />";
        }
        $html .= "<strong>Estado Civil:</strong> $marital_status<br />";
        $html .= "<strong>Escalafón:</strong> $scale<br />";
        $html .= "<strong>Decreto que lo ampara:</strong> $decree<br />";
        $html .= "<strong>Asesor que lo atendió:</strong> $asesor<br />";
        $html .= "<strong>¿Tiene vivienda propia?:</strong> " . ($home == 1) ? 'Si' : 'No' . "<br />";
        $html .= "<strong>¿Tiene ahorros para adquirir vivienda propia?:</strong> " . ($saves == 1) ? 'Si' : 'No' . "<br />";
        $html .= "<strong>Deporte que practica:</strong> $sports<br />";
        $html .= "<strong>Actividades en familia:</strong> $family_activities<br />";
        $html .= "<strong>Lugar actividades en familia:</strong> $family_place<br />";
        $html .= "<strong>Programas educativos:</strong> $programs<br />";
        $html .= "<strong>Programas educativos para familia / hijos:</strong> $family_programs<br />";
        $html .= "<strong>Programas educativos para familia / hijos:</strong> $family_programs<br />";
        
        $email_destinatario = get_theme_option('email_afiliaciones');

        $headers = "From: Maestros de la gente <no-reply@maestroscomfenalco.co>\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        $subject = get_theme_option('asunto_afiliaciones');

        $mail = wp_mail($email_destinatario, $subject, $html, $headers);
        
        echo json_encode($response);
        wp_die();
    }
    add_action('wp_ajax_register_no_afiliated', 'register_no_afiliated');
    add_action('wp_ajax_nopriv_register_no_afiliated', 'register_no_afiliated');


    function register_user_no_afiliated() {
    	if (!wp_verify_nonce($_POST['nonce'], 'ref_nonce')) die('Security Fail!');
        
		$name = $_POST['name'];
		$second_name = $_POST['second_name'];
		$lastname = $_POST['lastname'];
		$second_lastname = $_POST['second_lastname'];
		$doc_type = $_POST['doc_type'];
		$document = $_POST['documento'];
		$city = $_POST['city'];
		$phone = $_POST['phone'];
		$address = $_POST['address'];
		$hood = $_POST['hood'];
		$marital_status = $_POST['marital_status'];
		$scale = $_POST['scale'];
		$decree = $_POST['decree'];
		$home = $_POST['home'];
		$saves = $_POST['saves'];
		$sports = $_POST['sports'];
		$family_activities = $_POST['family_activities'];
		$family_place = $_POST['family_place'];
		$family_programs = $_POST['family_programs'];
		$programs = $_POST['programs'];
		$email = $_POST['email'];
		$pwd = $_POST['pwd'];
		$asesor = (isset($_POST['asesor'])) ? $_POST['asesor'] : '';
        $institute = $_POST['institute'];
		$childrens = $_POST['childrens'];
		$child_1 = $_POST['child_1'];
		$child_2 = $_POST['child_2'];
		$child_3 = $_POST['child_3'];
		$child_4 = $_POST['child_4'];
		$child_5 = $_POST['child_5'];
		$child_6 = $_POST['child_6'];
		$afiliation = $_POST['afiliation'];
		$ter_con = $_POST['ter_con'];
		$birthday = $_POST['birthday'];
                
        $hijo_1 = ($child_1) ? $child_1 : 0;
        $hijo_2 = ($child_2) ? $child_2 : 0;
        $hijo_3 = ($child_3) ? $child_3 : 0;
        $hijo_4 = ($child_4) ? $child_4 : 0;
        $hijo_5 = ($child_5) ? $child_5 : 0;
        $hijo_6 = ($child_6) ? $child_6 : 0;

        $db_data = get_db_connection_data();

        $digitador = $_POST['user_id'];

        $edades_hijos = "$hijo_1/$hijo_2/$hijo_3/$hijo_4/$hijo_5/$hijo_6";
        $fields = "`NOMBRE`,`SEGUNDO_NOMBRE`,`APELLIDO`,`SEGUNDO_APELLIDO`,`TIPO_DOCUMENTO`,`NUM_DOCUMENTO`,`CIUDAD`,`CELULAR`,`DIRECCION`,`BARRIO`,`INSTITUCION`,`EMAIL`,`TER_CON`,`CANT_HIJOS`,`EDADES_HIJOS`,`EST_CIVIL`,`ESCALAFON`,`DECRETO`,`NOM_ASESOR`,`VIVIENDA`,`DEPORTE`,`LUGAR_DEPORTE`,`ACT_TL_FAMILIA`,`LUGAR_TL_FAMILIA`,`PROG_EDU`,`CURSOS_ESP_PADRES`,`CURSOS_HIJOS`,`AHORROS`,`DIGITADOR`,`EMPRESA`,`VAL_AFILIADO`,`COD_REFERIDOS`,`FECHA_INGRESO`,`CUMPLEANOS`,`PIONEER`";
        $values = "'$name','$second_name','$lastname','$second_lastname','$doc_type','$document','$city','$phone','$address','$hood','$institute','$email','$ter_con','$childrens','$edades_hijos','$marital_status','$scale','$decree','$asesor','$home','$sports',0,'$family_activities','$family_place','$programs','$family_programs',0,'$saves','$digitador',0,$afiliation,'null',NOW(),'$birthday','1'";
        $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
        mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());

        $query = "INSERT INTO `wp_registro_maestro`($fields) VALUES ($values)";

        mysql_query($query, $sql_maestros_connect) or die(mysql_error());

	    mysql_close($sql_maestros_connect);
        
        //EMAIL
        
        $html = '<h3>Datos del usuario no afiliado:</h3><br />';
        $html .= "<strong>Nombres:</strong> $name $second_name<br />";
        $html .= "<strong>Apellidos:</strong> $lastname $second_lastname<br />";
        $html .= "<strong>Tipo de documento:</strong> $doc_type<br />";
        $html .= "<strong>Número de documento:</strong> $document<br />";
        $html .= "<strong>Fecha de nacimiento:</strong> $birthday<br />";
        $html .= "<strong>Ciudad:</strong> $city<br />";
        $html .= "<strong>Celular:</strong> $phone<br />";
        $html .= "<strong>Dirección:</strong> $address<br />";
        $html .= "<strong>Barrio:</strong> $hood<br />";
        $html .= "<strong>Institución Educativa:</strong> $institute<br />";
        $html .= "<strong>Correo Electrónico:</strong> $email<br />";
        $html .= "<strong>Número de Hijos:</strong> $childrens<br />";
        $ages = explode('/', $edades_hijos);
        for ($i = 0; $i < $childrens; $i++) {
            $n = $i + 1;
            $age = $ages[$i];
            $html .= "<strong>Edad Hijo $n:</strong> $age<br />";
        }
        $html .= "<strong>Estado Civil:</strong> $marital_status<br />";
        $html .= "<strong>Escalafón:</strong> $scale<br />";
        $html .= "<strong>Decreto que lo ampara:</strong> $decree<br />";
        $html .= "<strong>Asesor que lo atendió:</strong> $asesor<br />";
        $html .= "<strong>¿Tiene vivienda propia?:</strong> " . ($home == 1) ? 'Si' : 'No' . "<br />";
        $html .= "<strong>¿Tiene ahorros para adquirir vivienda propia?:</strong> " . ($saves == 1) ? 'Si' : 'No' . "<br />";
        $html .= "<strong>Deporte que practica:</strong> $sports<br />";
        $html .= "<strong>Actividades en familia:</strong> $family_activities<br />";
        $html .= "<strong>Lugar actividades en familia:</strong> $family_place<br />";
        $html .= "<strong>Programas educativos:</strong> $programs<br />";
        $html .= "<strong>Programas educativos para familia / hijos:</strong> $family_programs<br />";
        $html .= "<strong>Programas educativos para familia / hijos:</strong> $family_programs<br />";
        
        $email_destinatario = get_theme_option('email_afiliaciones');

        $headers = "From: Maestros de la gente <no-reply@maestroscomfenalco.co>\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        $subject = get_theme_option('asunto_afiliaciones');

        $mail = wp_mail($email_destinatario, $subject, $html, $headers);

        $response = array(
            'success' => 2
        );
        
        echo json_encode($response);
        wp_die();
    }
    add_action('wp_ajax_register_user_no_afiliated', 'register_user_no_afiliated');
    add_action('wp_ajax_nopriv_register_user_no_afiliated', 'register_user_no_afiliated');

    function verify_document() {
    	if (!wp_verify_nonce($_POST['nonce'], 'db_nonce')) die('Security Fail!');

	    $db_data = get_db_connection_data();
	    $try_documents = get_try_documents();

	    $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
	    mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());

	    $document = $_POST['document'];

	    $query = "SELECT * FROM `wp_registro_maestro` WHERE NUM_DOCUMENTO='$document'";
	    $rs = mysql_query($query, $sql_maestros_connect) or die(mysql_error());

    	if (mysql_num_rows($rs) > 0) {
    		$response = array(
    			'error_1' => 'La cédula ingresada ya está registrada'
    		);
    	} else {
    		if (!ComprobacionWebService($document)) {
    			if (!in_array($document, $try_documents)) {
	    			$response = array(
	    				'error' => 'El número de identificación no se encuentra afiliado a Comfenalco Valle, de igual manera haremos llegar tus datos a nuestros agentes quienes se contactarán contigo'
	    			);
    			} else {
                    $maestro_query = "SELECT * FROM `wp_concurso` WHERE `numeroDoc`='$document'";
                    $rs = mysql_query($maestro_query, $sql_maestros_connect);
                    if (mysql_num_rows($rs) > 0) {
                        $response = array(
                            'success' => 1
                        );
                    } else {
                        $response = array(
                            'success' => 2
                        );
                    }
    			}
    		} else {
                $maestro_query = "SELECT * FROM `wp_concurso` WHERE `numeroDoc`='$document'";
                $rs = mysql_query($maestro_query, $sql_maestros_connect);
                if (mysql_num_rows($rs) > 0) {
                    $response = array(
                        'success' => 1
                    );
                } else {
                    $response = array(
                        'success' => 3
                    );   
                }
			}
    	}
    	mysql_close($sql_maestros_connect);
		echo json_encode($response);
		wp_die();
    }
    add_action('wp_ajax_verify_document', 'verify_document');
    add_action('wp_ajax_nopriv_verify_document', 'verify_document');

    function verify_refcode() {
    	if (!wp_verify_nonce($_POST['nonce'], 'db_nonce')) die('Security Fail!');

    	$code = $_POST['code'];
    	$email = $_POST['email'];

	    $db_data = get_db_connection_data();

	    $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
	    mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());

	    $query = "SELECT * FROM `lista_referidos` WHERE `EMAIL`='$email'";
	    $rs = mysql_query($query, $sql_maestros_connect);

	    if (mysql_num_rows($rs) > 0) {
	    	while ($data = mysql_fetch_array($rs)) {
	    		if ($data['CODREFERIDO'] == $code) {
	    			$back = array(
	    				'success' => true
	    			);
	    		} else {
	    			$back = array(
	    				'error' => 'El código de referido ingresado no es válido'
	    			);
	    		}
	    	}
	    } else {
	    	$back = array(
	    		'error' => 'El email ingresado no ha sido referido por otro usuario'
	    	);
	    }
	    mysql_close($sql_maestros_connect);
	    echo json_encode($back);
	    wp_die();
    }
    add_action('wp_ajax_verify_refcode', 'verify_refcode');
    add_action('wp_ajax_nopriv_verify_refcode', 'verify_refcode');

    function invite_people() {
    	if (!wp_verify_nonce($_POST['nonce'], 'ref_nonce')) die('Security Fail!');

    	$user_id = $_POST['user_id'];
    	$name = $_POST['name'];
    	$email = $_POST['email'];
    	$db_data = get_db_connection_data();

    	$user = get_user_by('id', $user_id);
    	$document = get_user_meta($user_id, 'document', true);
    	$cod_referidos = get_user_refer_code($user_id);

    	$sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
    	mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());

    	$query_1 = "SELECT * FROM `lista_referidos` WHERE `EMAIL`='$email'";
    	$rs_1 = mysql_query($query_1, $sql_maestros_connect);

    	if (mysql_num_rows($rs_1) == 0) {
	    	$html = file_get_contents(get_permalink(get_theme_option('email_invitation_page')) . $user_id . '/' . urlencode($email) . '/');
	    	$display_name = $user->data->display_name;
	    	$user_email = $user->data->user_email;

	    	$headers = "From: $display_name <$user_email>\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

	    	$subject = "Tu amigo $display_name te ha invitado";

	    	$mail = wp_mail($email, $subject, $html, $headers);

	    	if ($mail) {
	    		//Insert referido in lista_referidos
	    		$query_2 = "INSERT INTO `lista_referidos`(`NOMBRE`,`EMAIL`,`CODREFERIDO`,`CEDULA`,`fecha_ingreso`,`fecha_activacion`,`ACTIVE`) VALUES ('$name','$email','$cod_referidos','$document',NOW(),'null','0')";
	    		mysql_query($query_2, $sql_maestros_connect);

	    		$puntos_referido = get_theme_option('ref_points');
	    		$accion_punto = 'Referido ' . $name . ' <' . $email . '>';

	    		//Insert points into wp_act_puntos
	    		$query_puntos = "INSERT INTO `wp_act_puntos`(`CEDULA`,`email_act_puntos`,`punto_recibido`,`accion_punto`,`email_referido`,`fecha_de_ingreso`,`fecha_activacion`,`ACTIVE`) VALUES ('$document','$user_email','$puntos_referido','$accion_punto','$email',NOW(),'null','0')";
	    		mysql_query($query_puntos, $sql_maestros_connect);

			    update_user_points($document);

	    		$response = array(
	    			'success' => 1
	    		);
	    	} else {
	    		$response = array(
	    			'error' => 'Ocurrio un error al tratar de enviar la invitación a <strong>' . $email . '</strong>'
	    		);
	    	}
    	} else {
    		$response = array(
    			'error' => 'El email <strong>' . $email . '</strong> ya ha sido referido por otro usuario'
    		);
    	}
    	mysql_close($sql_maestros_connect);
    	echo json_encode($response);
    	wp_die();
    }
    add_action('wp_ajax_invite_people', 'invite_people');
    add_action('wp_ajax_nopriv_invite_people', 'invite_people');

    function user_points() {
    	if (!wp_verify_nonce($_POST['nonce'], 'user_nonce')) die('Security Fail!');

    	$data = $_POST['data'];
    	$user_id = $_POST['user_id'];

    	$points = get_user_points($user_id);

    	if ($data == 'active') {
    		echo $points->active;
    	} elseif ($data == 'pending') {
    		echo $points->pending;
    	}

    	wp_die();
    }
    add_action('wp_ajax_user_points', 'user_points');
    add_action('wp_ajax_nopriv_user_points', 'user_points');

    function seach_user_info(){
    	if (!wp_verify_nonce($_POST['nonce'], 'search_nonce')) die('Security Fail!');
    	get_template_part('templates/admin/admin-search-results');
    	wp_die();
    }
    add_action('wp_ajax_search_user_info', 'search_user_info');
    add_action('wp_ajax_nopriv_search_user_info', 'search_user_info');

    function update_user_data() {
    	if (!wp_verify_nonce($_POST['nonce'], 'ref_nonce')) die('Security Fail!');
        
        $user_id = $_POST['user'];
        $field = $_POST['field'];
        $value = $_POST['value'];
        $document = get_user_meta($user_id, 'document', true);
        
        $query = "UPDATE `wp_registro_maestro` SET `$field`='$value' WHERE `NUM_DOCUMENTO`='$document'";
        $db_data = get_db_connection_data();
        $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
        mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());
        mysql_query($query, $sql_maestros_connect);
        mysql_close($sql_maestros_connect);
        if (is_maestro($user_id) == 1) {
            echo 'Usuario actualizado exitosamente';
        } else {
            echo '(' . is_maestro($user_id) . ') Ha ocurrido un error al intentar actualizar la información del usuario, por favor inténtalo de nuevo más tarde';
        }
        wp_die();
    }
    add_action('wp_ajax_update_user_data', 'update_user_data');
    add_action('wp_ajax_nopriv_update_user_data', 'update_user_data');

?>