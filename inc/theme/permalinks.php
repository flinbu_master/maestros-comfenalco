<?php
    function add_my_vars($public_query_vars) {
        $public_query_vars[] = 'uemail';
        $public_query_vars[] = 'ref_code';
        $public_query_vars[] = 'uname';
        $public_query_vars[] = 'ucode';
        $public_query_vars[] = 'uaction';
        $public_query_vars[] = 'udoc';
        return $public_query_vars;
    }
    add_action('query_vars', 'add_my_vars', 10, 1);

    function do_rewrite_register() {
        $register_page = get_post(get_theme_option('register_page'));
        $email_invitation_page = get_post(get_theme_option('email_invitation_page'));
        $reg_expression = '([^/]+)'; 
        
        add_rewrite_rule('('. $register_page->post_name . ')/' . $reg_expression . '/' . $reg_expression . '/?$', 'index.php?pagename=' . $register_page->post_name . '&uemail=$matches[2]&ref_code=$matches[3]', 'top');
        add_rewrite_rule('('. $email_invitation_page->post_name . ')/' . $reg_expression . '/' . $reg_expression . '/?$', 'index.php?pagename=' . $email_invitation_page->post_name . '&uname=$matches[2]&uemail=$matches[3]', 'top');
        add_rewrite_rule('(actualizar-datos)/?$', 'index.php?pagename=' . $register_page->post_name . '&uaction=update', 'top');
        add_rewrite_rule('(nuevo-registro)/?$', 'index.php?pagename=' . $register_page->post_name . '&uaction=new', 'top');
        
        add_rewrite_rule('(backend)/(search)/?$', 'index.php?pagename=backend&uaction=search', 'top');
        add_rewrite_rule('(backend)/(authorize)/' . $reg_expression . '/?$', 'index.php?pagename=backend&uaction=authorize&udoc=$matches[3]', 'top');
        add_rewrite_rule('(backend)/(authorize)/?$', 'index.php?pagename=backend&uaction=authorize', 'top');
        add_rewrite_rule('(backend)/(manager)/?$', 'index.php?pagename=backend&uaction=manager', 'top');
    }
    add_action('init', 'do_rewrite_register');
?>