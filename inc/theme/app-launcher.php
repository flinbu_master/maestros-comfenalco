<?php
    function app_launcher() {
        if (get_theme_option('app_launcher')) {
            $icon_id = get_theme_option('app_icon');
            $icons = array(
                '60x60' => wp_get_attachment_image_src($icon_id, 'app-icon-60x60'),
                '76x76' => wp_get_attachment_image_src($icon_id, 'app-icon-76x76'),
                '120x120' => wp_get_attachment_image_src($icon_id, 'app-icon-120x120'),
                '128x128' => wp_get_attachment_image_src($icon_id, 'app-icon-128x128'),
                '152x152' => wp_get_attachment_image_src($icon_id, 'app-icon-152x152'),
                '192x192' => wp_get_attachment_image_src($icon_id, 'app-icon-192x192'),
                '512x512' => wp_get_attachment_image_src($icon_id, 'app-icon-512x512')
            );
            
            $custom_app_title = get_theme_option('app_launcher_custom_title');
            
            echo '<meta name="mobile-web-app-capable" content="yes" />';
            echo '<meta name="apple-mobile-web-app-capable" content="yes" />';
            echo '<meta name="apple-mobile-web-app-status-bar-style" content="' . get_theme_option('apple_status_bar_style') . '" />';
            echo '<meta name="apple-mobile-web-app-title" content="' . ($custom_app_title) ? $custom_app_title : get_bloginfo('name') . '" />';
            
            $apple_icon_sizes = array('60x60', '76x76', '120x120', '152x152', '192x192', '512x512');
            
            foreach ($icons as $icon => $data) {
                if ($icon) {
                    if (in_array($icon, $apple_icon_sizes)) {
                        $rel = 'apple-touch-icon';
                    } else {
                        $rel = 'icon';
                    }
                    echo '<link rel="' . $rel . '" sizes="' . $icon . '" href="' . $data[0] . '" />';
                }
            }
        }
    }
    add_action('wp_head', 'app_launcher');
?>