<?php
	/**
	 * Get option from options page (advance custom fields pro plugin required)
	 * @param  {string} $option [opiton id]
	 * @return {mix} return data required, array, OBJECT, string, int, etc...
	 */
	 function get_theme_option($option){
	 	if(function_exists('get_field')){
	 		return get_field($option, 'option');
	 	}
	 }

	/**
	 * Get the device type mobileDetect class required
	 * @return {string} Device type
	 */
	function device_class(){
		$device = new Mobile_Detect();

		if( $device->isMobile() ){
			$back = 'mobile';
		}
		if( $device->isTablet() ){
			$back = 'tablet';
		}
		if( !$device->isMobile() && !$device->isTablet() ){
			$back = 'desktop';
		}
		return $back;
	}

	/**
	 * Add son span tags in the string
	 * @param  {string} $title The string to stylize
	 * @return {string} The string stylized
	 */
	function stylize_this_title($title){
		$title = utf8_decode($title);
		$special_char = '&ÁÉÍÓÚÑáéíóúñ¡¿';
	    $module_title = str_word_count($title, 1);
	    $middle_title = floor(count($module_title) / 2);
	    $the_module_title = '';
	    for ($i=0; $i < count($module_title); $i++) { 
	        if($i != $middle_title){
	            $the_module_title .= $module_title[$i] . ' ';
	        }else{
	            $the_module_title .= '<span>' . $module_title[$i] . ' ';
	        }
	        if($i == count($module_title)-1){
	            $the_module_title .= '</span>';
	        }
	    }
	    return utf8_encode($the_module_title);
	}

	/**
	 * Print the current page title to the header title tag
	 * @return {void} The formatted title
	 */
	function page_title(){
		wp_title('&raquo;&nbsp;', TRUE, 'right');
		bloginfo('name'); 
	}

	/**
	 * Print some meta tags
	 * @return {void} The HTML meta tags
	 */
	function meta_tags(){
		echo '<meta charset="'.get_bloginfo('charset').'" />';
		echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
		echo '<meta name="viewport" content="width=device-width, user-scalable=0, initial-scale=1">';
	}

	/**
	 * Cut the excerpt
	 * @param {string} $charlength String to cut.
	 * @return {string} The shorted string
	 */
	function get_lim_excerpt($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;

		if (mb_strlen($excerpt) > $charlength) {
			$subex = mb_substr($excerpt, 0, $charlength - 5);
			$exwords = explode(' ', $subex);
			$excut = -( mb_strlen($exwords[count($exwords) - 1]));
			if ($excut < 0) {
				return mb_substr($subex, 0, $excut);
			} else {
				return $subex;
			}
		} else {
			return $excerpt;
		}
	}

	/**
	 * Pritn get_lim_excerpt return
	 * @param {string} $charlength String to cut.
	 * @return {void} The shorted string
	 */
	function lim_excerpt($chartlength) {
		echo get_lim_excerpt($chartlength);
	}

	/**
	 * Cut the WP Object title
	 * @param {strig} $charlength String to cut.
	 * @return {string} The shorted string
	 */
	function get_lim_title($charlength) {
		$title = get_the_title();
		$charlength++;

		if (mb_strlen($title) > $charlength) {
			$subti = mb_substr($title, 0, $charlength - 5);
			$exwords = explode(' ', $subti);
			$excut = -( mb_strlen($exwords[count($exwords) - 1]));
			if ($excut < 0) {
				return mb_substr($subti, 0, $excut) . '...';
			} else {
				return $subti . '...';
			}
		} else {
			return $title;
		}
	}

	/**
	 * Print the get_lim_title result
	 * @param {string} $charlength String to cut.
	 * @return {void} The shorted string
	 */
	function lim_title($charlength) {
		echo get_lim_title($charlength);
	}

	/**
	 * Return the WP Object publish date
	 * @param {int} $postID WP Object ID to search
	 * @return {array} Array with the date info (['day'], ['month'], ['year'])
	 */
	function get_array_date($postID) {
		$d = get_the_time('j-F-Y', $postID);
		$e = explode('-', $d);
		$date = array('day' => $e[0], 'month' => $e[1], 'year' => $e[2]);
		return $date;
	}

	/**
	 * Print spanish formated get_array_date result
	 * @return {void} The formatted date
	 */
	function this_date() {
		$postID = get_the_ID();
		$date = get_array_date($postID);
		echo 'El ' . $date['day'] . ' de ' . $date['month'] . ' del ' . $date['year'];
	}

	/**
	 * Register theme script to WP
	 * @param {array} $scripts Script to register
	 */
	 function register_this( $scripts ){
	 	foreach( $scripts as $script ){
			$deps = (isset($script['deps'])) ? $script['deps'] : FALSE;
			$version = (isset($script['version'])) ? $script['version'] : '1.0.0';
	 		if( $script['type'] == 'script' ){
	 			$in_footer = (isset($script['footer'])) ? $script['footer'] : FALSE;
	 			wp_register_script( $script['name'] , $script['location'], $deps, $version, $in_footer );
			} elseif( $script['type'] == 'style' ){
				wp_register_style( $script['name'] , $script['location'], $deps, $version );
			}
	 	}
	 }

	 /**
	  * Enqueue script to load with WP
	  * @param {array} $scripts Scripts to enqueue (['name'], ['type'])
	  */
	 function enqueue_this( $scripts ){
	 	foreach ($scripts as $script) {
	 		if( $script['type'] == 'script' ){
	 			wp_enqueue_script($script['name']);
	 		} elseif( $script['type'] == 'style' ){
	 			wp_enqueue_style($script['name']);
	 		}
	 	}
	 }

	 /**
	  * Register the theme features
	  * @param {array} $features The theme features
	  */
	 function support_this($features){
	 	foreach ($features as $feature) {
	 		if(isset($feature[1])){
	 			add_theme_support($feature[0], $feature[1]);
	 		} else {
	 			add_theme_support($feature[0]);
	 		}
	 	}
	 }

	 /**
	  * Add image sizes to WP
	  * @param {srray} $sizes The sizes to add ([0] => {string} name, [1] => {int} width, [2] => {int} height, [3] => {bool} cut)
	  */
	 function add_image_sizes($sizes){
	 	foreach ($sizes as $size){
	 		add_image_size($size[0], $size[1], $size[2], $size[3]);
	 	}
	 }

	 /**
	  * Register sidebars to WP
	  * @param {array} $sidebars Array whith arrays sidebar data (['name'], ['slug'], ['desc'])
	  */
	 function register_this_sidebars($sidebars){
	 	global $theme_slug;
	 	foreach ($sidebars as $sidebar){
	 		register_sidebar(array(
	 			'name' => $sidebar['name'],
	 			'id' => $theme_slug . $sidebar['slug'],
	 			'description' => $sidebar['desc'],
	 			'class' => $sidebar['slug'],
	 			'before_widget' => '<li id="%1$s" class="widget col-xs-12 col-sm-4 col-md-12 %2$s">',
	 			'after_widget' => '</li>',
	 			'before_title' => '<h3 class="title">',
	 			'after_title' => '</h3>'
	 		));
	 	}
	 }

	/**
	 * Check if a plugin is active
	 * @param  {string}  $plugin Plugin name
	 * @return {bool}
	 */
	 function is_this_plugin_active($plugin){
	 	switch ($plugin) {
	 		case 'woocommerce':
	 			$path = 'woocommerce/woocommerce.php';
	 			break;
	 	}
	 	return in_array($path, apply_filters('active_plugins', get_option('active_plugins')));
	 }

	/**
	 * Register custom post type to WP
	 * @param  {array} $post_types Array with data
	 */
	 function register_custom_post_types($post_types, $lang = 'es'){
		switch($lang){
			case 'es':
				$t = array(
					'add_new' => 'Añadir nuevo',
					'edit' => 'Editar',
					'new' => 'Nuevo',
					'all' => 'Todos',
					'see' => 'Ver',
					'search' => 'Buscar',
					'no_found' => 'no encontrados',
					'not_found_in_trash' => 'no encontrados en la papelera'
				);
				break;
			case 'en':
				$t = array(
					'add_new' => 'Add new',
					'edit' => 'Edit',
					'new' => 'New',
					'all' => 'All',
					'see' => 'See',
					'search' => 'Search',
					'no_found' => 'not found',
					'not_found_in_trash' => 'not found in the trash'
				);
				break;
		}
	 	foreach ($post_types as $type){
	 		if($type['level'] == 0){
	 			$show_menu = TRUE;
	 		} else {
	 			$show_menu = 'edit.php?post_type=' . $type['parent_post_type'];
	 		}
	 		$labels = array(
	 			'name' => $type['general_name'],
	 			'singular_name' => _x($type['singular_name'], 'post type general name'),
	 			'add_new' => _x($t['add_new'] . ' ' . $type['singular_name'], $type['name']),
	 			'add_new_item' => __($t['add_new'] . ' ' . $type['singular_name']),
	 			'edit_item' => __($t['edit'] . ' ' . $type['singular_name']),
	 			'new_item' => __($t['new']),
	 			'all_items' => ($type['level'] == 0) ? __($t['all'] . ' ' . $type['gen_conector'] . ' ' . $type['general_name']) : __($type['general_name']),
	 			'view_item' => __($t['see'] . ' ' . $type['singular_name']),
	 			'seach_items' => __($t['search'] . ' ' . $type['singular_name']),
	 			'not_found' => __($type['general_name'] . ' ' . $t['not_found']),
	 			'not_found_in_trash' => __($type['general_name'] . ' ' . $t['not_found_in_trash']),
	 			'parent_item_colon' => '',
	 			'menu_name' => $type['general_name']
	 		);
	 		$args = array(
	 			'labels' => $labels,
	 			'public' => TRUE,
	 			'publicity_querable' => TRUE,
	 			'show_ui' => TRUE,
	 			'show_in_menu' => $show_menu,
	 			'query_var' => TRUE,
	 			'rewrite' => $type['rewrite'],
	 			'capability_type' => 'post',
	 			'has_archive' => TRUE,
	 			'hierarchical' => TRUE,
	 			'menu_position' => 20,
	 			'exclude_from_search' => $type['exclude_from_search'],
	 			'supports' => $type['supports'],
	 			'menu_icon' => $type['icon']
	 		);
	 		register_post_type( $type['name'], $args );
	 	}
	 }

	/**
	 * Register taxonomies on Wordpress
	 * @param  {array} $taxonomies array with taxonomies to register
	 * @param  {string} $lang  the language of the labels
	 * @return {void}
	 */
	 function register_this_taxonomies($taxonomies, $lang = 'es'){
		switch($lang){
			case 'es':
				$t = array(
					'add_new' => 'Añadir nuevo',
					'add' => 'Añadir',
					'remove' => 'eliminar',
					'edit' => 'Editar',
					'new' => 'Nuevo',
					'all' => 'Todos',
					'see' => 'Ver',
					'search' => 'Buscar',
					'no_found' => 'no encontrados',
					'not_found_in_trash' => 'no encontrados en la papelera',
					'update' => 'Actualizar',
					'parent' => 'Parent',
					'name' => 'Nombre',
					'popular' => 'populares',
					'separate_items_with_commas' => 'Separe los items con comas',
					'choose_from_most_used' => 'Escoge entre los más usados'
				);
				break;
			case 'en':
				$t = array(
					'add_new' => 'Add new',
					'add' => 'Add',
					'remove' => 'remove',
					'edit' => 'Edit',
					'new' => 'New',
					'all' => 'All',
					'see' => 'See',
					'search' => 'Search',
					'no_found' => 'not found',
					'not_found_in_trash' => 'not found in the trash',
					'update' => 'Update',
					'parent' => 'Parent',
					'name' => 'name',
					'popular' => 'popular',
					'separate_items_with_commas' => 'Separate items with commas',
					'choose_from_most_used' => 'Chose from most used'
				);
				break;
		}
	 	foreach($taxonomies as $tax){
	 		if($tax['hierarchical']){
				$labels = array(
					'name'              => _x( $tax['general_name'], 'taxonomy general name' ),
					'singular_name'     => _x( $tax['singular_name'], 'taxonomy singular name' ),
					'search_items'      => __( $t['search'] . ' ' . $tax['general_name'] ),
					'all_items'         => __( $t['all'] . ' ' . $tax['gen_conector'] . ' ' . $tax['general_name'] ),
					'parent_item'       => __( $tax['singular_name'] . ' ' . $t['parent'] ),
					'parent_item_colon' => __( $tax['singular_name'] . ' ' . $t['parent'] . ':' ),
					'edit_item'         => __( $t['edit'] . ' ' . $tax['singular_name'] ),
					'update_item'       => __( $t['update'] . ' ' . $tax['singular_name'] ),
					'add_new_item'      => __( $t['add_new'] . ' ' . $tax['singular_name'] ),
					'new_item_name'     => __( $t['name'] . ' ' . $t['new'] . ' ' . $tax['singular_name'] ),
					'menu_name'         => __( $tax['singular_name'] ),
				);
		 		$args = array(
					'hierarchical'      => true,
					'labels'            => $labels,
					'show_ui'           => ($tax['show_ui']) ? $tax['show_ui'] : true,
					'show_admin_column' => ($tax['show_admin_column']) ? $tax['show_admin_column'] : true,
					'query_var'         => ($tax['query_var']) ? $tax['query_var'] : true,
					'rewrite'           => $tax['rewrite']
				);
	 		} else {
				$labels = array(
					'name'                       => _x( $tax['general_name'], 'taxonomy general name' ),
					'singular_name'              => _x( $tax['singular_name'], 'taxonomy singular name' ),
					'search_items'               => __( $t['search'] . ' ' . $tax['general_name'] ),
					'popular_items'              => __( $tax['general_name'] . ' ' . $t['popular'] ),
					'all_items'                  => __( $t['all'] . ' ' . $tax['gen_conector'] . ' ' . $tax['general_name'] ),
					'parent_item'                => null,
					'parent_item_colon'          => null,
					'edit_item'                  => __( $t['edit'] . ' ' . $tax['singular_name'] ),
					'update_item'                => __( $t['update'] . ' ' . $tax['singular_name'] ),
					'add_new_item'               => __( $t['add_new'] . ' ' . $tax['singular_name'] ),
					'new_item_name'              => __( $t['name'] . ' ' . $t['new'] . ' ' . $tax['singular_name'] ),
					'separate_items_with_commas' => __( $t['separate_items_with_commas'] ),
					'add_or_remove_items'        => __( $t['add'] . ' ' . $tax['sufix'] . ' ' .$t['remove'] . ' ' . $tax['general_name'] ),
					'choose_from_most_used'      => __( $t['choose_from_most_used'] ),
					'not_found'                  => __( $tax['general_name'] . ' ' . $t['not_found'] ),
					'menu_name'                  => __( $tax['general_name'] ),
				);
				$args = array(
					'hierarchical'          => false,
					'labels'                => $labels,
					'show_ui'               => ($tax['show_ui']) ? $tax['show_ui'] : true,
					'show_admin_column'     => ($tax['show_admin_column']) ? $tax['show_admin_column'] : true,
					'update_count_callback' => '_update_post_term_count',
					'query_var'             => ($tax['query_var']) ? $tax['query_var'] : true,
					'rewrite'               => $tax['rewrite']
				);
	 		}
	 		register_taxonomy( $tax['name'], $tax['post_type'], $args );
	 	}
	 }

	/**
	 * Add shortcodes to Wordpress
	 * @param {array} $shortcodes array with shortcode name and function to add
	 * @param {string} $path path to shorcodes files
	 */
	function add_this_shortcodes($shortcodes, $path){
		foreach ($shortcodes as $shortcode){
			require_once $path . $shortcode['func'] . '.php';
			add_shortcode($shortcode['name'], $shortcode['func']);
		}
	}

	/**
	 * Devuelve o imprime herramientas sociales de Facebook
	 * @param $what (string)(req) La herramienta social necesitada  (Ver documentación en http://flinbu.com/themes/docs/functions/social/#get_facebook)
	 * @param $params (array) Parametros para las herramientas sociales  (Ver documentación en http://flinbu.com/themes/docs/functions/social/#get_facebook)
	 * @param $return (boolean) TRUE, devuelve un string - FALSE (default) Imprime el string
	 * @return string, void
	 */
	 function get_facebook($what, $params = array(), $return = FALSE) {
	 	if($what == 'scripts') {
	 		$fb_app_id = get_theme_option( 'facebook_app_id' );
	        $back = '<div id="fb-root"></div>
	                <script>
	                    (function(d, s, id) {
	                      var js, fjs = d.getElementsByTagName(s)[0];
	                      if (d.getElementById(id)) return;
	                      js = d.createElement(s); js.id = id;
	                      js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.4&appId=' . $fb_app_id . '";
	                      fjs.parentNode.insertBefore(js, fjs);
	                    }(document, \'script\', \'facebook-jssdk\'));
	                </script>';
		} elseif( $what == 'comments_count' ){
			$url = $params['url'];
		  	$json = json_decode(file_get_contents('https://graph.facebook.com/?ids=' . $url));
		  	$back = isset($json->$url->comments) ? $json->$url->comments : 0;
		} else {
			$fb_class = '';
			$data = '';
			switch ($what) {
				case 'like_button' :
					$fb_class = 'like';
					break;
				case 'share_button' :
					$fb_class = 'share-button';
					break;
				case 'send_button' :
					$fb_class = 'send';
					break;
				case 'embedded_posts' :
					$fb_class = 'post';
					break;
				case 'follow_button' :
					$fb_class = 'follow';
					break;
				case 'comments' :
					$fb_class = 'comments';
					break;
				case 'activity_feed' :
					$fb_class = 'activity';
					break;
				case 'recommendations_feed' :
					$fb_class = 'recommendations';
					break;
				case 'recommendations_bar' :
					$fb_class = 'recommendations-bar';
					break;
				case 'like_box' :
					$fb_class = 'like-box';
					break;
				case 'facepile' :
					$fb_class = 'facepile';
					break;
			}		
			foreach ($params as $key => $value) {
				switch($key) {
					case 'url' :
						$data .= 'data-href="'.$value.'" ';
						break;
					case 'width' :
						$data .= 'data-width="'.$value.'" ';
						break;
					case 'height' :
						$data .= 'data-height="'.$value.'" ';
						break;
					case 'layout' :
						$data .= 'data-layout="'.$value.'" ';
						break;
					case 'action' :
						$data .= 'data-action="'.$value.'" ';
						break;
					case 'show_faces' :
						$data .= 'data-show-faces="'.$value.'" ';
						break;
					case 'share_button' :
						$data .= 'data-share="'.$value.'" ';
						break;
					case 'colorscheme' :
						$data .= 'data-colorscheme="'.$value.'" ';
						break;
					case 'numberposts' :
						$data .= 'data-numposts="'.$value.'" ';
						break;
					case 'max_rows' :
						$data .= 'data-max-rows="'.$value.'" ';
						break;
					case 'domain' :
						$data .= 'data-site="'.$value.'" ';
						break;
					case 'show_header' :
						$data .= 'data-show-header="'.$value.'" ';
						break;
					case 'show_posts' :
						$data .= 'data-show-posts="'.$value.'" ';
						break;
					case 'show_border' :
						$data .= 'data-show-border="'.$value.'" ';
						break;
					case 'show_count' :
						$data .= 'data-show-count="'.$value.'" ';
						break;
					case 'photo_size' :
						$data .= 'data-size="'.$value.'" ';
						break;
					case 'mobile' : 
						$data .= 'data-mobile="'.$value.'" ';
						break;
				}
			}
			$back = '<div class="fb-'.$fb_class.'" '.$data.'></div>';
		}
		if ($return) {
			return $back;
		} else {
			echo $back;
		}
	 }

	/**
	 * Get comments number from Facebook
	 * @param  {int} $post_id WP Object ID
	 * @return {int} Total comments number
	 */
	function get_facebook_comments_count($post_id){
	    $url = get_permalink($post_id);
	    $fb_connect = 'https://graph.facebook.com/v2.3/?fields=share{comment_count}&id=' . $url;
	    $fb_data = file_get_contents($fb_connect);
	    $fb_data = json_decode($fb_data);
	    
	    return $fb_data->share->comment_count;
	}

	/**
	 * Print comments label
	 * @param {int} $post_id WP Object ID
	 * @param {string} [$device = 'desktop'] The out format
	 */
	function facebook_comments_number($post_id, $device = 'desktop'){
	    $fb_count = get_facebook_comments_count($post_id);
	    switch($device){
	        case 'desktop':
	            if($fb_count == 1){
	                $fb_label = ' comentario';
	            } else {
	                $fb_label = ' comentarios';
	            }
	            $back = $fb_count . $fb_label;
	            break;
	        case 'mobile':
	            $back = $fb_count;
	            break;
	    }
	    echo $back;
	}

	/**
	 * Print formatted whit <pre> tag the print_r resutls
	 * @param {object, array, string, int, bool} $w The object to format
	 */
	function print_pre($w) {
		echo '<pre>';
		print_r($w);
		echo '</pre>';
	}

	/**
	 * Limit string
	 * @param {string} $str The string to limit
	 * @param {int} $long Characters length
	 * @param {bool} $returnIndica TRUE to print, FALSE to return
	 * @return {void, string}
	 */
	function get_lim_this($str, $long, $return = FALSE) {
		$excerpt = $str;
		$charlenth = $long;
		$charlength++;
		if (mb_strlen($excerpt) > $charlength) {
			$subex = mb_substr($excerpt, 0, $charlength - 5);
			$exwords = explode(' ', $subex);
			$excut = -( mb_strlen($exwords[count($exwords) - 1]));
			if ($excut < 0) {
				$back = mb_substr($subex, 0, $excut);
			} else {
				$back = $subex;
			}
		} else {
			$back = $excerpt;
		}
		if ($return) {
			return $back;
		} else {
			echo $back;
		}
	}

	/**
	 * Return the WP Post parent ID
	 * @param {int} $post_id WP Post ID
	 * @return {int, array}
	 */
	function get_post_parent($post_id) {
		$postObj = get_post($post_id);
		$parent = $postObj -> post_parent;
		return $parent;
	}

	/**
	 * Get and object with the post thumbnail
	 * @param  {int} $id   [post id]
	 * @param  {string} $size [image size]
	 * @return {array}       [object with data]
	 */
	function get_thumb($id, $size) {
		$opid = get_post_thumbnail_id($id);
		return wp_get_attachment_image_src($opid, $size);
	}

	/**
	 * To generate breadcrumb with the yoast breadcrumb plugin
	 * @return {void} [print html]
	 */
	function breadcrumbs() {
		if (function_exists('yoast_breadcrumb')) :
			yoast_breadcrumb('<div id="breadcrumb">', '</div>');
		endif;
	}

	/**
	 * Return month name from a month number
	 * @param  {int}  $num  [month number]
	 * @param  {string}  $how  [full name or short name]
	 * @param  boolean $echo [print or return]
	 * @return [void, string]        [the month name]
	 */
	function get_month_by_number($num, $how, $echo = TRUE) {
		switch($num) {
			case 01 :
				$month = 'Enero';
				$resum = 'Ene';
				break;
			case 02 :
				$month = 'Febrero';
				$resum = 'Feb';
				break;
			case 03 :
				$month = 'Marzo';
				$resum = 'Mar';
				break;
			case 04 :
				$month = 'Abril';
				$resum = 'Abr';
				break;
			case 05 :
				$month = 'Mayo';
				$resum = 'May';
				break;
			case 06 :
				$month = 'Junio';
				$resum = 'Jun';
				break;
			case 07 :
				$month = 'Julio';
				$resum = 'Jul';
				break;
			case 08 :
				$month = 'Agosto';
				$resum = 'Ago';
				break;
			case 09 :
				$month = 'Septiembre';
				$resum = 'Sep';
				break;
			case 10 :
				$month = 'Octubre';
				$resum = 'Oct';
				break;
			case 11 :
				$month = 'Noviembre';
				$resum = 'Nov';
				break;
			case 12 :
				$month = 'Diciembre';
				$resum = 'Dic';
				break;
		}
		switch($how) {
			case 'full' :
				$back = $month;
				break;
			case 'min' :
				$back = $resum;
		}
		if ($echo) {
			echo $back;
		} else {
			return $back;
		}
	}

	/**
	 * Grab youtube id video from URL
	 * @param  {string} $url [Youtube video url]
	 * @return {string}      [Video ID]
	 */
	function the_youtube_id($url) {
		$url_string = parse_url($url, PHP_URL_QUERY);
		parse_str($url_string, $args);
		$video = ($args['v']) ? $args['v'] : $url;
		if (strpos($video, '.be/')) {
			$vid_arr = explode('/', $video);
			$vid_id = $vid_arr[3];
		} elseif (strpos($video, '/v/')) {
			$vid_arr = explode('/', $video);
			$vid_id = $vid_arr[4];
		} elseif (strpos($video, '/embed/')) {
			$vid_arr = explode('/', $video);
			$vid_id = $vid_arr[4];
		} else {
			$vid_id = $video;
		}
		return $vid_id;
	}

	/**
	 * Grab vimeo video id from url
	 * @param  {string} $url [Vimeo video url]
	 * @return {string}      [Video id]
	 */
	function the_vimeo_id($url){
		$url = explode('/',$url);
		return $url[3];
	}

	/**
	 * Convert an array to XML
	 * @param  array $array   The array to convert
	 * @param  string $lastkey descthe close keyription
	 * @return string  xml structure
	 */
	function arrayToXml($array, $lastkey) {
	    $buffer.="<".$lastkey.">\n";
	    if (!is_array($array))
	    {$buffer.=$array;}
	    else
	    {
	        foreach($array as $key=>$value)
	        {
	            if (is_array($value))
	            {
	                if ( is_numeric(key($value)))
	                {
	                    foreach($value as $bkey=>$bvalue)
	                    {
	                        $buffer.=arrayToXml($bvalue,$key);
	                    }
	                }
	                else
	                {
	                    $buffer.=arrayToXml($value,$key);
	                }
	            }
	            else
	            {
	                $buffer.=arrayToXml($value,$key);
	            }
	        }
	    }
	    $buffer.="</".$lastkey.">\n";
	    return $buffer;
	}

	/**
	 * Grab values from meta key
	 * @param  string $key    Meta key
	 * @param  string $type   Post type
	 * @param  string $status Post status
	 * @return mix Meta value
	 */
	function get_meta_values( $key = '', $type = 'post', $status = 'publish' ) {
	    global $wpdb;
	    if( empty( $key ) )
	        return;
	    $r = $wpdb->get_col( $wpdb->prepare( "
	        SELECT pm.meta_value FROM {$wpdb->postmeta} pm
	        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
	        WHERE pm.meta_key = '%s' 
	        AND p.post_status = '%s' 
	        AND p.post_type = '%s'
	    ", $key, $status, $type ) );
	    return array_unique($r);
	}

	/**
	 * Get user role
	 * @return string user role
	 */
	function get_user_role() {
		global $current_user;

		$user_roles = $current_user->roles;
		$user_role = array_shift($user_roles);

		return $user_role;
	}

	function get_share_data($post_id){
		$link = get_permalink($post_id);
		$api = 'https://free.sharedcount.com/url?url=' . $link . '&apikey=' . get_theme_option('shared_count_api_key');

		$data = file_get_contents($api);

		$data = json_decode($data);

		return $data;
	}

	function update_share_data($post_id){
		$networks = array('facebook', 'twitter', 'pinterest', 'gplus', 'whatsapp', 'email');
		$data = get_share_data($post_id);

		$new_total = 0;
		foreach ($networks as $network) {
			if($network == 'whatsapp' || $network == 'email' || $network == 'gplus') {
				$current = get_post_meta($post_id, 'share_' . $network, true);
				if(!$current){
					add_post_meta($post_id, 'share_' . $network, '0');
					$current = 0;
				}
			} else {
				$net = $network;
				if($network == 'gplus'){
					$net = 'GooglePlusOne';
				}
				$current = get_post_meta($post_id, 'share_' . $network, true);
				if(!$current){
					$new_data = $data->$network;
					if($network == 'facebook'){
						$new_data = $data->facebook->share_count;
					}
					add_post_meta($post_id, 'share_' . $network, $new_data);
				}

				$new_data = $data->$network;
				if($network == 'facebook'){
					$new_data = $data->facebook->share_count;
				}

				update_post_meta($post_id, 'share_' . $network, $new_data);
			}
		}

		$total = get_post_meta($post_id, 'share_total', true);

		$total_data = 0;
		foreach($networks as $network){
			$total_data += get_post_meta($post_id, 'share_' . $network, true);
		}
		if(!$total){
			add_post_meta($post_id, 'share_total', '0');
		}
		update_post_meta($post_id, 'share_total', $total_data);
	}

	function get_shares($post_id, $network){
		$shares = get_post_meta($post_id, 'share_' . $network, true);
		return ($shares) ? $shares : 0;
	}

	/**
	 * clear_share_meta( $post_id )
	 * Borra los meta datos con la infomación de la actividad del post en redes sociales.
	 * Deshace post_social_share_data();
	 * @param  {int} $post_id [ID del post]
	 * @return void
	 */
	function clear_share_meta( $post_id ){
		delete_post_meta( $post_id, 'share_total' );
		delete_post_meta( $post_id, 'share_facebook' );
		delete_post_meta( $post_id, 'share_twitter' );
		delete_post_meta( $post_id, 'share_gplus' );
		delete_post_meta( $post_id, 'share_pinterest' );
	}

	/**
	 * Grab post likes
	 * @param  int $post_id The post id
	 * @return int total likes
	 */
	function get_likes($post_id) {
		$likes = get_post_meta($post_id, 'likes', true);

		return ($likes) ? $likes : 0;
	}

	/**
	 * Grab post mails
	 * @param  int $post_id The post id
	 * @return int total likes
	 */
	function get_mails($post_id) {
		$mails = get_post_meta($post_id, 'mail_share', true);

		return ($mails) ? $mails : 0;
	}

	/**
	 * Check if the latest published post
	 * @param  int $post_id (optional) The post ID to check. Defautl: Current post ID in the loop.
	 * @return boolean
	 */
	function is_latest_post($post_id = null){
		$ID = ($post_id) ? $post_id : get_the_ID();
		$last = wp_get_recent_posts('1');
		if($ID == $last[0]['ID']){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Print the copyright information
	 * return void
	 **/
	function copyright(){
		$copy_text = get_theme_option('copyright_text');
		$search = array(
			'%year%',
			'%sitename%',
			'%c%'
		);
		$replace = array(
			date('Y'),
			get_bloginfo('name'),
			'&copy;'
		);
		echo str_replace($search, $replace, $copy_text);
	}

	/**
	 * Print specific location scripts added in options panel
	 * @param {string} $location The theme script location
	 * return void
	 **/
	function theme_third_scripts($location){
		$scripts = get_theme_option($location);
	    if($scripts){
	        foreach ($scripts as $script) {
	            $search = array(
	                '%facebook_app_secret%',
	                '%facebook_app_id%',
	                '%google_app_id%',
	                '%google_app_secret%',
	                '%google_app_client_id%',
	                '%home_url%'
	            );
	            $replace = array(
	                get_theme_option('facebook_app_secret'),
	                get_theme_option('facebook_app_id'),
	                get_theme_option('google_app_id'),
	                get_theme_option('google_app_secret'),
	                get_theme_option('google_app_client_id'),
	                get_bloginfo('home')
	            );

	            $the_script = str_replace($search, $replace, $script['script']);
	            echo $the_script;
	        }
	    }
	}

	/**
	 * Get the SEO information from the SEO by Yoast plugin
	 * @param  {string} $type The object type to search for
	 * @param  {int} $id   The object ID
	 * @return {object} Object whit $seo->title and $seo->description
	 */
	function get_seo($type, $id){
		$seo = new stdClass;
		switch($type){
			case 'category':
				$meta = get_option('wpseo_taxonomy_meta');
				$seo->title = $meta['category'][$id]['wpseo_title'];
				$seo->description = $meta['category'][$id]['wpseo_desc'];
				break;
			case 'post':
				$seo->title = get_post_meta($id, '_yoast_wpseo_title', true);
				$seo->description = get_post_meta($id, '_yoast_wpseo_metadesc', true);
				break;
		}
		return $seo;
	}

	/**
	 * Print or return the single ID for object
	 * @param  {string} [$content_type = 'page'] The content type of the object (category, tag post or page)
	 * @param  {bool} [$display = true] TRUE to print the result or FALSE to return it
	 * @return {void, string} The object title
	 */
	function the_page_title($content_type = 'page', $display = true){
		switch ($content_type) {
			case 'page':
				if($display){
					the_title();
					die();
				} else {
					$title = get_the_title(get_the_ID());
				}
				break;
			case 'category':
				single_cat_title('', $display);
				if($display){
					die();
				}
				break;
			case 'tag':
				single_tag_title('', $display);
				if ($display) {
					die();
				}
				break;
		}
		if(!$display){
			return $title;
	    } else {
	        echo $title;
	    }
	}

	/**
	 * Print Bootstrap formated pagination component from WP Query
	 * @param {object} [$query = ''] The loop query to paginate
	 */
	function bs_pagination($query = ''){
	    if (is_singular()) {
	        return;
	    }

	    if(!$query){
	    	global $wp_query;	
	    	$query = $wp_query;
	    }
	    /** Stop execution if there's only 1 page */
	    if ($query->max_num_pages <= 1) {
	        return;
	    }
	    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
	    $max = intval($query->max_num_pages);
	    /** Add current page to the array */
	    if ($paged >= 1) {
	        $links[] = $paged;
	    }
	    /** Add the pages around the current page to the array */
	    if ($paged >= 3) {
	        $links[] = $paged - 1;
	        $links[] = $paged - 2;
	    }
	    if (($paged + 2) <= $max) {
	        $links[] = $paged + 2;
	        $links[] = $paged + 1;
	    }
	    echo '<div class="paginator"><ul class="pagination">' . "\n";
	    /** Previous Post Link */
	    if (get_previous_posts_link()) {
	        printf('<li>%s</li>' . "\n", get_previous_posts_link('<i class="material-icon-navigate-before"></i>'));
	    }
	    /** Link to first page, plus ellipses if necessary */
	    if (!in_array(1, $links)) {
	        $class = 1 == $paged ? ' class="first active"' : ' class="first"';
	        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');
	        if (!in_array(2, $links)) {
	            echo '<li>…</li>';
	        }
	    }
	    /** Link to current page, plus 2 pages in either direction if necessary */
	    sort($links);
	    foreach ((array)$links as $link) {
	        $class = $paged == $link ? ' class="last active"' : ' class="last"';
	        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
	    }
	    /** Link to last page, plus ellipses if necessary */
	    if (!in_array($max, $links)) {
	        if (!in_array($max - 1, $links)) {
	            echo '<li><span class="btn disabled">…</span></li>' . "\n";
	        }
	        $class = $paged == $max ? ' class="active"' : '';
	        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
	    }
	    /** Next Post Link */
	    if (get_next_posts_link()) {
	        printf('<li>%s</li>' . "\n", get_next_posts_link('<i class="material-icon-navigate-next"></i>'));
	    }
	    echo '</ul></div>' . "\n";
	}

	/**
	 * Get the URL slug from and WP object
	 * @param  {int} $post_id The post id to search
	 * @return {string} The URL slug for the WP Object
	 */
	function get_the_slug($post_id) {
	    $post_data = get_post($post_id, ARRAY_A);
	    $slug = $post_data['post_name'];
	    return $slug; 
	}

	/**
	 * Print or retrieve the category name from an WP Object
	 * @param  {int} $post_id The WP Object ID
	 * @param  {bool} [$echo = false] TRUE print the category name and FALSE return it
	 * @return {void, string} The category name
	 */
	function tip_category($post_id, $echo = false){
		$cat = get_the_category($post_id);
		if($echo){
			echo $cat[0]->name;
		} else {
			return $cat[0]->name;
		}
	}

	/**
	 * Retrieve the font awesome class for a respective network icon
	 * @param  {string} $network The network name to retrive (facebook, twitter, gplus, pinterest, tumblr, whatsapp, email)
	 * @return {string} The FA class
	 */
	function get_network_icon($network){
		$icon = 'fa ';
		switch($network){
			case 'facebook':
				$icon .= 'fa-facebook';
				break;
			case 'twitter':
				$icon .= 'fa-twitter';
				break;
			case 'gplus':
				$icon .= 'fa-google-plus';
				break;
			case 'pinterest':
				$icon .= 'fa-pinterest-p';
				break;
			case 'tumblr':
				$icon .= 'fa-tumblr';
				break;
			case 'whatsapp':
				$icon .= 'fa-whatsapp';
				break;
			case 'email':
				$icon .= 'fa-envelope';
				break;
			case 'instagram':
				$icon .= 'fa-instagram';
				break;
		}
		return $icon;
	}

	/**
	 * Return the latest post ID
	 * @return {int} The WP Object ID
	 */
	function get_last_post_id(){
		$last = get_posts(array(
	        'posts_per_page' => 1,
	        'orderby' => 'menu_order',
	        'order' => 'ASC'
	    ));
		return $last[0]->ID;
	}

	/**
	 * Return the first post ID
	 * @return {int} The WP Object ID
	 */
	function get_first_post_id(){
		$last = get_posts(array(
	        'post_type' => 'post',
	        'posts_per_page' => 1,
	        'orderby' => 'menu_order',
	        'order' => 'DESC'
	    ));
		return $last[0]->ID;
	}

	/**
	 * Grab the social profiles URL
	 * @return {object} Object with the social URL
	 */
	function get_theme_social(){
	    
	    $facebook = get_theme_option('facebook');
	    $twitter = get_theme_option('twitter');
	    $instagram = get_theme_option('instagram');
	    $google = get_theme_option('google');
	    $pinterest = get_theme_option('pinterest');
	    
	    if($facebook){
	        $social['facebook'] = $facebook;
	    }
	    if($twitter){
	        $social['twitter'] = $twitter;
	    }
	    if($instagram){
	        $social['instagram'] = $instagram;
	    }
	    if($google){
	        $social['gplus'] = $google;
	    }
	    if($pinterest){
	        $social['pinterest'] = $pinterest;
	    }
	    
	    return $social;
	}

	/**
	 * Print the passed menu with Bootstrap structure using a custom walker
	 * @param {string} $menu The menu location registered in function register_menu()
	 * @return void
	 */ 
	function bootstrap_menu($menu, $classes = '') {
	    wp_nav_menu(array(
	        'theme_location' => $menu,
	        'depth' => 0,
	        'container' => false,
	        'fallback_cb' => false,
	        'menu_class' => 'nav navbar-nav ' . $classes,
	        'walker' => new BootstrapNavMenuWalker()
	    ));
	}
    
    /**
     * Busca los usuarios de Wordpress y vincula el número de cédula para acceder a los datos en la tabalap_registro_maestro
     * @param $email [string] Email del usuario
     * @param $field [string] Nombre del campo a registrar
     * @param $value [mixed] Valor a registrar en el campo
     * @return void
     */ 
    function add_extra_data_by_user_email($email, $field, $value) {
        $user = get_user_by('email', $email);
        if ($user) :
          $user_meta_value = get_user_meta($user->ID, $field, true);
          if (!$user_meta_value) {
            $user_meta = add_user_meta($user->ID, $field, $value);
            if (is_wp_error($user_meta)) {
                die($user_meta->get_error_message());
            } else {
                $user_meta_value = get_user_meta($user->ID, $field, true);
                if ($user_meta_value) {
                    $msg = 'Usuario <strong>' . $email . '</strong> vinculado exitosamente';
                    return $msg;
                    die($msg);
                } else {
                    $msg = 'Error al tratar de vincular a <strong>' . $email . '</strong>';                  
                    return $msg;
                    die($msg);
                }
            }
          } else {
            $msg = 'El <strong>' . $email . '</strong> ya ha sido vinculado anteriormente';
            return $msg;
            die($msg);          
          }
        else :
      $msg = 'El <strong>' . $email . '</strong> no existe.';
            return $msg;
            die($msg);  
        endif;
    }

    /**
     * Remplaza etiquetas por datos reales
     * @param  string $string El texto a tratar
     * @return string         El texto con las etiquetas remplazadas
     */
    function replace_labels($string) {
        $custom_labels = get_theme_option('custom_labels');
        $labels = array();
        $values = array();
        if ($custom_labels) {
            foreach ($custom_labels as $label) {
                $labels[] = '%' . $label['label'] . '%';
                $values[] = $label['value'];
            }
        }
        $labels[] = '%ref_points%';
        $values[] = get_theme_option('ref_points');
        
        return str_replace($labels, $values, $string);
    }

    /**
     * Devuelve un error en el login dependiento de un codigo
     * @param  string $code Nombre del error
     * @return string       Mensaje para mosrar
     */
    function error_message($code){
        switch($code){
            case 'invalid_username':
                $msg = 'El usuario ingresado no existe';
                break;
            case 'incorrect_password':
                $msg = 'Contraseña incorrecta';
                break;
            default:
                $msg = 'Por favor revisa los campos e inténtalo de nuevo';
                break;
        }
        return $msg;
    }

    function get_user_document($user_id) {
        return get_user_meta($user_id, 'document', true);
    }

    /**
     * Obtener datos de conexión a MySQL
     * @return array Arreglo con los datos de conexión
     */
    function get_db_connection_data() {
    	return array(
	        'name' => 'maestros_comfenalco',
	        'user' => 'maestros_admin',
	        'pass' => 'wE0_gWMtTi;J',
	        'table' => 'wp_registro_maestro',
	        'server' => 'localhost'
	    );
    }

    /**
     * Obtiene el código de referido del usuario
     * @param  integer $user_id ID del usuario a consultar
     * @return string       Código asignado
     */
    function get_user_refer_code($user_id) {
	    $db_data = get_db_connection_data();

	    $document = get_user_document($user_id);

	    $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
	    mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());

	    $query = "SELECT `COD_REFERIDOS` FROM `wp_registro_maestro` WHERE `NUM_DOCUMENTO`='$document'";
	    $rs = mysql_query($query, $sql_maestros_connect);

	    $back = 'ERROR';
	    
	    while($data = mysql_fetch_array($rs)) {
	    	$back = $data['COD_REFERIDOS'];
	    }

	    mysql_close($sql_maestros_connect);

	    return $back;
    }

    /**
     * Actualiza el total de puntos activos y pendientes del usuario según el registro en la tabla wp_act_puntos
     * @param  integer $document Número de documento
     * @return bool          Verdadero o falso dependiento del resultado del proceso
     */
    function update_user_points($document) {
    	if ($document) {
	    	$db_data = get_db_connection_data();
	    	$active_points = 0;
	    	$pending_points = 0;

	    	$sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
	    	mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());

	    	$query_actives = "SELECT * FROM `wp_act_puntos` WHERE `CEDULA`='$document' AND `ACTIVE`='1'";
	    	$query_pending = "SELECT * FROM `wp_act_puntos` WHERE `CEDULA`='$document' AND `ACTIVE`='0'";

	    	$actives = mysql_query($query_actives, $sql_maestros_connect) or die(mysql_error());
	    	$pending = mysql_query($query_pending, $sql_maestros_connect) or die(mysql_error()); 

	    	while ($puntos = mysql_fetch_array($actives)) {
	    		$active_points = $active_points + $puntos['punto_recibido'];
	    	}
	    	while ($puntos = mysql_fetch_array($pending)) {
	    		$pending_points = $pending_points + $puntos['punto_recibido'];
	    	}

	    	$update_points = "UPDATE `wp_registro_maestro` SET `TOTAL_DE_PUNTOS`='$active_points',`PUNTOS_PENDIENTES`='$pending_points' WHERE `NUM_DOCUMENTO`='$document'";
	    	mysql_query($update_points, $sql_maestros_connect) or die(mysql_error());
	    	mysql_close($sql_maestros_connect);

	    	return true;
    	} else {
    		return false;
    	}
    }

    /**
     * Consulta el total de puntos activos y pendientes
     * @param  integer $user_id ID del usuario WP
     * @return OBJECT       Objeto con las propiedades active y pending
     */
    function get_user_points($user_id) {
    	$document = get_user_document($user_id);

        $back = new stdClass;

    	if (update_user_points($document)) {
	    	$db_data = get_db_connection_data();

			$query = "SELECT * FROM `wp_registro_maestro` WHERE `NUM_DOCUMENTO`='$document'";

			$active = 0;
			$pending = 0;

			$sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
			mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());
			$rs = mysql_query($query, $sql_maestros_connect);

			while ($data = mysql_fetch_array($rs)) {
				$active = $data['TOTAL_DE_PUNTOS'];
				$pending = $data['PUNTOS_PENDIENTES'];
			}

			mysql_close($sql_maestros_connect);
			$back->active = ($active) ? $active : 0;
			$back->pending = ($pending) ? $pending : 0;

		} else {
			$back->active = 0;
			$back->pending = 0;            
        }
        return $back;
    }

    /**
     * Obtiene un top de usuarios con el puntaje más alto
     * @param  integer $top El número de registro a consultar
     * @return array        Arreglo con todas las entradas solicitadas
     */
    function get_top($top = 5) {
    	$db_data = get_db_connection_data();

    	$query = "SELECT `EMAIL`,`TOTAL_DE_PUNTOS` FROM `wp_registro_maestro` WHERE `VAL_AFILIADO`='1' ORDER BY `TOTAL_DE_PUNTOS` DESC LIMIT $top";
    	$sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
    	mysql_select_db($db_data['name'], $sql_maestros_connect);

    	$rs = mysql_query($query, $sql_maestros_connect);
    	
    	$top_data = array();

    	while ($data = mysql_fetch_array($rs)) {
    		$user = get_user_by('email', $data['EMAIL']);
    		$top_data[] = array(
    			'display_name' => get_display_name($user->ID),
    			'points' => $data['TOTAL_DE_PUNTOS']
    		);
    	}

    	mysql_close($sql_maestros_connect);

    	return $top_data;
    }

    /**
     * Consulta el historial de puntos por usuario en la tabla wp_act_puntos
     * @param  integer $user_id El ID WP del usuario
     * @return array            Arreglo con el historial de puntos y sus estados
     */
    function get_points_story($user_id) {
    	$document = get_user_document($user_id);
    	$query = "SELECT * FROM `wp_act_puntos` WHERE `CEDULA`='$document' ORDER BY `fecha_activacion` DESC";

    	$db_data = get_db_connection_data();
    	$sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
    	mysql_select_db($db_data['name'], $sql_maestros_connect) or die(mysql_error());
    	$rs = mysql_query($query, $sql_maestros_connect);

    	$points = array();

    	while ($data = mysql_fetch_array($rs)) {
    		$state = 'pendientes';
    		if ($data['ACTIVE'] == '1') {
    			$state = 'aplicados';
    		}
    		if($data['ENABLE'] == 0) {
    			$state = 'invalidos';
    		}
    		$points[] = array(
    			'action' => htmlspecialchars($data['accion_punto']),
    			'active_date' => $data['fecha_activacion'],
    			'submit_date' => $data['fecha_de_ingreso'],
    			'points' => $data['punto_recibido'],
    			'status' => $state
    		);
    	}

    	mysql_close($sql_maestros_connect);
    	return $points;
    }

    /**
     * Devuelve la fecha en un formato agradable y en español
     * @param  string $string  La fecha fea
     * @param  string $default Cuando no es posible establecer la fecha, valor por defecto
     * @return string          La fecha formateada
     */
    function get_nice_date($string, $default = '') {
    	if ($string != '0000-00-00') {
	    	setlocale(LC_ALL, 'es_ES');
	    	$date = DateTime::createFromFormat('Y-m-d', $string);
	    	return ucwords(utf8_encode(strftime('%e %B, %Y', $date->getTimestamp())));
    	} else {
    		return $default;
    	}
    }

    /**
     * Obtiene todos los campos por usuario de la tabala wp_registro_maestro
     * @param  integer $user_id El ID del usuario WP
     * @return array          	Arreglo con todos los campos
     */
    function get_all_user_data($user_id) {
        $document = get_user_document($user_id);
        $query = "SELECT * FROM `wp_registro_maestro` WHERE `NUM_DOCUMENTO`='$document' LIMIT 1";
        
        $db_data = get_db_connection_data();
        $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
        mysql_select_db($db_data['name'], $sql_maestros_connect);
        $rs = mysql_query($query, $sql_maestros_connect);        
        $response = array();
        while ($data = mysql_fetch_array($rs)) {
            $response = $data;
        }
        mysql_close($sql_maestros_connect);
        
        return $response;
    }

    /**
     * Identifica si un usuario es pionero (participante desde el reto 1)
     * @param  integer  $user_id El ID del usuario WP
     * @return boolean           TRUE o FALSE dependiendo del resultado 
     */
    function is_pioneer($user_id) {
        $document = get_user_document($user_id);
        $query = "SELECT `PIONEER` FROM `wp_registro_maestro` WHERE `NUM_DOCUMENTO`='$document' LIMIT 1";
        
        $db_data = get_db_connection_data();
        $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
        mysql_select_db($db_data['name'], $sql_maestros_connect);
        $rs = mysql_query($query, $sql_maestros_connect);        
        $response = false;
        while ($data = mysql_fetch_array($rs)) {
            $response = $data['PIONEER'];
        }
        mysql_close($sql_maestros_connect);
        
        return $response;
    }

    function is_maestro($user_id) {
        $document = get_user_document($user_id);
        $query = "SELECT `MAESTRO` FROM `wp_registro_maestro` WHERE `NUM_DOCUMENTO`='$document' LIMIT 1";
        
        $db_data = get_db_connection_data();
        $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
        mysql_select_db($db_data['name'], $sql_maestros_connect);
        $rs = mysql_query($query, $sql_maestros_connect);        
        $response = false;
        while ($data = mysql_fetch_array($rs)) {
            $response = $data['MAESTRO'];
        }
        mysql_close($sql_maestros_connect);
        
        return $response;
    }

    /**
     * Devuelve todos los perfiles publicados por un digitador, puede tardar en consultar
     * @param  integer $agent_id El ID del usuario WP
     * @return arrat             Arreglo multidimensional con todas las entradas
     */
    function get_submitted_profiles($agent_id) {
        $query = "SELECT * FROM `wp_registro_maestro` WHERE `DIGITADOR`='$agent_id' ORDER BY `FECHA_INGRESO` DESC";
        $db_data = get_db_connection_data();
        $sql_maestros_connect = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
        mysql_select_db($db_data['name'], $sql_maestros_connect);
        $rs = mysql_query($query, $sql_maestros_connect) or die(mysql_error());
        $response = array();    
        while ($data = mysql_fetch_array($rs)) {
            $response[] = array(
                'name' => $data['NOMBRE'] . ' ' . $data['APELLIDO'],
                'document' => $data['NUM_DOCUMENTO'],
                'date' => $data['FECHA_INGRESO'],
                'points' => ($data['TOTAL_DE_PUNTOS']) ? $data['TOTAL_DE_PUNTOS'] : 0
            );
        }
        mysql_close($sql_maestros_connect);
        
        return $response;
    }


    function get_try_documents() {
    	$docs = get_theme_option('try_documents');
    	$back = array();
    	if ($docs) {
    		foreach ($docs as $doc) {
    			$back[] = $doc['document'];
    		}
    	}

    	return $back;
    }

    function unutf8ize($d) {
	    if (is_array($d)) {
	        foreach ($d as $k => $v) {
	            $d[$k] = unutf8ize($v);
	        }
	    } else if (is_string ($d)) {
	        return utf8_decode($d);
	    }
	    return $d;
	}

	function get_totals() {
		$db_data = get_db_connection_data();
		$totals = new stdClass();

		$link = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
		mysql_select_db($db_data['name'], $link) or die(mysql_error());

		$query_total = "SELECT `NUM_DOCUMENTO`FROM `wp_registro_maestro`";
		$rs = mysql_query($query_total, $link) or die(mysql_error());
		$totals->all = mysql_num_rows($rs);

		$query_maestros = "SELECT `NUM_DOCUMENTO` FROM `wp_registro_maestro` WHERE `MAESTRO`='1'";
		$rs = mysql_query($query_maestros, $link) or die(mysql_error());
		$totals->maestros = mysql_num_rows($rs);

		$query_no_maestros = "SELECT `NUM_DOCUMENTO` FROM `wp_registro_maestro` WHERE `MAESTRO`='0'";
		$rs = mysql_query($query_no_maestros, $link) or die(mysql_error());
		$totals->no_maestros = mysql_num_rows($rs);

		$query_no_afiliados = "SELECT `NUM_DOCUMENTO` FROM `wp_registro_maestro` WHERE `VAL_AFILIADO`='0'";
		$rs = mysql_query($query_no_afiliados, $link) or die(mysql_error());
		$totals->no_afiliados = mysql_num_rows($rs);

		$query_afiliados = "SELECT `NUM_DOCUMENTO` FROM `wp_registro_maestro` WHERE `VAL_AFILIADO`='1'";
		$rs = mysql_query($query_afiliados, $link) or die(mysql_error());
		$totals->afiliados = mysql_num_rows($rs);

		$totals->pct_afiliados = round(($totals->afiliados * 100) / $totals->all, 1);
		$totals->pct_no_afiliados = round(($totals->no_afiliados * 100) / $totals->all, 1);
		$totals->pct_maestros = round(($totals->maestros * 100) / $totals->all, 1);
		$totals->pct_no_maestros = round(($totals->no_maestros * 100) / $totals->all, 1);

		mysql_close($link);

		return $totals;
	}

	function backend_allowed($user_id) {
		$backend_users = get_theme_option('backend_access');
		$back = false;
		if ($backend_users) {
			$users = array();
			for ($i = 0; $i < count($backend_users); $i++) {
				$users[] = $backend_users[$i]['usuario']['ID'];
			}

			if (in_array($user_id, $users)) {
				$back = true;
			}
		}
		return $back;
	}

	function get_display_name($user_id) {
		$document = get_user_meta($user_id, 'document', true);
		$db_data = get_db_connection_data();

		$query = "SELECT `NOMBRE`,`APELLIDO` FROM `wp_registro_maestro` WHERE `NUM_DOCUMENTO`='$document' LIMIT 1";
		$link = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass'], true) or die(mysql_error());
		mysql_select_db($db_data['name'], $link) or die(mysql_error());
		$rs = mysql_query($query, $link) or die(mysql_error());

		$back = get_userdata($user_id)->display_name;

		if(mysql_num_rows($rs) > 0) {
			while ($data = mysql_fetch_array($rs)) {
				$back = $data['NOMBRE'] . ' ' . $data['APELLIDO'];
			}
		}
		mysql_close($link);
		return $back;
	}
?>