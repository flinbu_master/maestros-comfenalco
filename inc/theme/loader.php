<?php
    error_reporting(E_ALL);
    function enqueue_scripts(){
        
        $scripts = array(
            array(
                'name' => 'bootstrap-styles',
                'type' => 'style'
            ),
            array(
                'name' => 'animate',
                'type' => 'style'
            ),
            array(
                'name' => 'montserrat',
                'type' => 'style'
            ),
            array(
                'name' => 'lato',
                'type' => 'style'
            ),
            array(
                'name' => 'datepicker-styles',
                'type' => 'style'
            ),
            array(
                'name' => 'freelancer-styles',
                'type' => 'style'
            ),
            array(
                'name' => 'maestros-styles',
                'type' => 'style'
            ),
            array(
                'name' => 'font-awesome',
                'type' => 'style'
            ),
            array(
                'name' => 'jquery',
                'type' => 'script'
            ),
            array(
                'name' => 'bootstrap-scripts',
                'type' => 'script'
            ),
            array(
                'name' => 'jquery-easing',
                'type' => 'script'
            ),
            array(
                'name' => 'classie',
                'type' => 'script'
            ),
            array(
                'name' => 'animate-header',
                'type' => 'script'
            ),
            array(
                'name' => 'bootstrap-validator',
                'type' => 'script'
            ),
            array(
                'name' => 'password-strength',
                'type' => 'script'
            ),
            array(
                'name' => 'footable',
                'type' => 'script'
            ),
            array(
                'name' => 'footable-sort',
                'type' => 'script'
            ),
            array(
                'name' => 'footable-filter',
                'type' => 'script'
            ),
            array(
                'name' => 'footable-paginate',
                'type' => 'script'
            ),
            array(
                'name' => 'chosen-styles',
                'type' => 'style'
            ),
            array(
                'name' => 'chosen-scripts',
                'type' => 'script'
            ),
            array(
                'name' => 'datepicker-scripts',
                'type' => 'script'
            ),
            array(
                'name' => 'maestros-scripts',
                'type' => 'script'
            )
        );
        enqueue_this($scripts);
        
        wp_localize_script('maestros-scripts', 'maestros_data', array(
            'ajax' => admin_url('admin-ajax.php'),
            'home_url' => get_bloginfo('home'),
            'current_url' => get_permalink(get_the_ID()),
            'dashboard_url' => get_permalink(get_theme_option('dashboard_page')),
            'user_nonce' => wp_create_nonce('user_nonce'),
            'db_nonce' => wp_create_nonce('db_nonce'),
            'ref_nonce' => wp_create_nonce('ref_nonce'),
            'search_nonce' => wp_create_nonce('search_nonce'),
            'rcs' => get_theme_option('recaptcha_secret')
        ));
    }
    add_action('wp_enqueue_scripts', 'enqueue_scripts');

    function state_admin_bar() {
        if (get_theme_option('no_admin_bar')) {
            return false;
        } else {
            return true;
        }
    }
    add_filter('show_admin_bar', 'state_admin_bar');

    function header_third_scripts() {
        theme_third_scripts('header_scripts');
    }
    add_action('wp_head', 'header_third_scripts');

    function footer_third_scripts() {
        theme_third_scripts('footer_scripts');
    }
    add_action('wp_footer', 'footer_third_scripts');

    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'wp_shortlink_wp_head');

    if (function_exists('acf_add_options_page')) {
        acf_add_options_page();
    }
?>