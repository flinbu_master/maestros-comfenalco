<?php
    global $theme_location;
    $css = $theme_location . '/assets/css';
    $js = $theme_location . '/assets/js';
    $plugins = $theme_location . '/assets/plugins';
    
    $scripts = array(
        //Custom JS and CSS
        array(
            'name' => 'maestros-styles',
            'type' => 'style', 
            'location' => $css . '/maestros.css',
            'deps' => array('montserrat', 'lato', 'poetsen', 'freelancer-styles')
        ),
        array(
            'name' => 'freelancer-styles',
            'type' => 'style', 
            'location' => $css . '/freelancer.css',
            'deps' => array('montserrat', 'lato', 'poetsen')
        ),
        array(
            'name' => 'animate',
            'type' => 'style', 
            'location' => $css . '/animate.css'
        ),
        array(
            'name' => 'montserrat',
            'type' => 'style',
            'location' => 'http://fonts.googleapis.com/css?family=Montserrat:400,700'
        ),
        array(
            'name' => 'lato',
            'type' => 'style',
            'location' => 'http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic'
        ),
        array(
            'name' => 'poetsen',
            'type' => 'style',
            'location' => $css . '/poetsen.css'
        ),
        array(
            'name' => 'font-awesome',
            'type' => 'style',
            'location' => $css . '/font-awesome/css/font-awesome.min.css'
        ),
        array(
            'name' => 'bootstrap-styles',
            'type' => 'style',
            'location' => $css . '/bootstrap.min.css'
        ),
        array(
            'name' => 'bootstrap-scripts',
            'type' => 'script',
            'location' => $js . '/bootstrap.min.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        array(
            'name' => 'jquery-easing',
            'type' => 'script',
            'location' => 'http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        array(
            'name' => 'classie',
            'type' => 'script',
            'location' => $js . '/classie.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        array(
            'name' => 'animate-header',
            'type' => 'script',
            'location' => $js . '/cbpAnimatedHeader.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        array(
            'name' => 'bootstrap-validator',
            'type' => 'script',
            'location' => $js . '/jqBootstrapValidation.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        array(
            'name' => 'contact-me',
            'type' => 'script',
            'location' => $js . '/contact_me.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        //reCaptcha
        array(
            'name' => 'recaptcha',
            'type' => 'script',
            'location' => 'https://www.google.com/recaptcha/api.js?onload=recaptcha_init&render=exlplicit'
        ),
        //Password Strength
        array(
            'name' => 'password-strength',
            'type' => 'script',
            'location' => $plugins . '/password-strength/pwstrength.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        //Custom JS
        array(
            'name' => 'maestros-scripts',
            'type' => 'script',
            'location' => $js . '/freelancer.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        //Footable
        array(
            'name' => 'footable',
            'type' => 'script',
            'location' => $js . '/footable/footable.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        array(
            'name' => 'footable-bookmarkable',
            'type' => 'script',
            'location' => $js . '/footable/footable.bookmarkable.js',
            'deps' => array('jquery', 'footable'),
            'footer' => true
        ),
        array(
            'name' => 'footable-filter',
            'type' => 'script',
            'location' => $js . '/footable/footable.filter.js',
            'deps' => array('jquery', 'footable'),
            'footer' => true
        ),
        array(
            'name' => 'footable-grid',
            'type' => 'script',
            'location' => $js . '/footable/footable.grid.js',
            'deps' => array('jquery', 'footable'),
            'footer' => true
        ),
        array(
            'name' => 'footable-memory',
            'type' => 'script',
            'location' => $js . '/footable/footable.memory.js',
            'deps' => array('jquery', 'footable'),
            'footer' => true
        ),
        array(
            'name' => 'footable-paginate',
            'type' => 'script',
            'location' => $js . '/footable/footable.paginate.js',
            'deps' => array('jquery', 'footable'),
            'footer' => true
        ),
        array(
            'name' => 'footable-sort',
            'type' => 'script',
            'location' => $js . '/footable/footable.sort.js',
            'deps' => array('jquery', 'footable'),
            'footer' => true
        ),
        array(
            'name' => 'footable-striping',
            'type' => 'script',
            'location' => $js . '/footable/footable.striping.js',
            'deps' => array('jquery', 'footable'),
            'footer' => true
        ),
        //Date Picker
        array(
            'name' => 'datepicker-scripts',
            'type' => 'script',
            'location' => $plugins . '/date-picker/js/bootstrap-datepicker.min.js',
            'deps' => array('jquery', 'bootstrap-scripts'),
            'footer' => true
        ),
        array(
            'name' => 'datepicker-styles',
            'type' => 'style',
            'location' => $plugins . '/date-picker/css/bootstrap-datepicker3.min.css'
        ),
        //Chosen
        array(
            'name' => 'chosen-styles',
            'type' => 'style',
            'location' => $plugins . '/chosen/chosen.min.css'
        ),
        array(
            'name' => 'chosen-scripts',
            'type' => 'script',
            'location' => $plugins . '/chosen/chosen.jquery.min.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        array(
            'name' => 'admin-styles',
            'type' => 'style',
            'location' => $css . '/admin.css'
        )
    );

    register_this($scripts);
?>