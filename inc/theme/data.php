<?php
    function get_custom_data($data) {
        switch ($data) {
            case 'valle_municipios' :
                $back = array('Alcalá','Ansermanuevo','Argelia','Bolívar','Cartago','El Aguila','El Cairo ','El Dovio','La Unión','La Victoria','Obando','Roldanillo','Toro','Ulloa','Versalles','Zarzal','Andalucía','Buga','Bugalagrande','Calima Darién','El Cerrito','Ginebra','Guacarí ','Restrepo','Riofrío','San Pedro','Trujillo','Tuluá','Yotoco','Buenaventura','Caicedonia','Sevilla','Cali','Candelaria','Dagua','Florida','Jamundí','La Cumbre','Palmira','Pradera','Vijes','Yumbo');
                sort($back);
                break;
            case 'asesores' :
                $advisers = get_theme_option('advisers');
                $back = array();
                if ($advisers) {
                    foreach ($advisers as $adviser) {
                        $back[] = array(
                            'name' => $adviser['name'],
                            'city' => $adviser['city']
                        );
                    }
                }
                break;
            case 'instituciones' :
                $db_data = get_db_connection_data();
                $query = 'SELECT `NOMBRE` FROM `instituciones_educativas`';
                $back = array();
                $link = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass']) or die(mysql_error());
                mysql_select_db($db_data['name'], $link) or die(mysql_error());
                $rs = mysql_query($query, $link);
                while ($data = mysql_fetch_array($rs)) {
                    $back[] = $data['NOMBRE'];
                }
                mysql_close();
                break;
        }
        return $back;
    }
?>