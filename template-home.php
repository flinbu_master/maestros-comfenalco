<?php
    /**
     * Template Name: Home
     * */
    get_header();
?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <?php 
                    $campaign_logo = wp_get_attachment_image_src(get_theme_option('logo_campaign'), 'full');
                    if ($campaign_logo) :
                ?>
                    <img src="<?=$campaign_logo[0];?>" alt="<?php bloginfo('name'); ?>" class="img-responsive" />
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="intro-text">
                    <span class="name poetsen"><?=get_theme_option('campaign_copy');?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <button class="btn btn-default btn-lg invitar" type="button" <?=(!is_user_logged_in()) ? 'data-toggle="modal" data-target="#login-modal"' : 'onclick="goto_page(\'' . get_permalink(get_theme_option('dashboard_page')) . '\');"'; ?>><?=get_theme_option('main_btn_label');?></button>
            </div>
        </div>
    </div>
</header>
<section class="success" id="about">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2 text-center participa">
                <p>Participa por uno de los 5 viajes a San Andrés con un acompañante.</p>
            </div>
            <div class="col-xs-12 col-md-6 col-md-offset-3 text-center interno">
                <p>Suma <?=get_theme_option('ref_points');?> puntos por cada registro efectivo y aumenta las posibilidades de ser uno de los 5 DOCENTES que viajarán a San Andrés con un acompañante.</p>
            </div>
        </div>
        <div class="row estrella">
            <div class="col-xs-12 col-md-4 col-md-offset-2">
                <?php get_template_part('templates/tables/top-5'); ?>
            </div>
            <div class="col-xs-12 col-md-4" id="login-page">
                <?php $login_place = 'page'; get_template_part('templates/forms/login'); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>