<?php get_header(); ?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-8">
                <h1><?php the_field('header_text'); ?></h1>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <?php
                    $campaign_logo = wp_get_attachment_image_src(get_theme_option('logo_campaign'), 'full');
                    if ($campaign_logo) :
                ?>
                    <img src="<?=$campaign_logo[0];?>" alt="<?php bloginfo('name'); ?>" class="img-responsive center-block" />
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>