<?php get_header('page'); ?>
	<section id="page">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="container main">
				<div class="col-xs-12">
					<?php the_content(); ?>
				</div>
			</div>
		<?php endwhile; endif; ?>
	</section>
<?php get_footer(); ?>