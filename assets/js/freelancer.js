$ = jQuery.noConflict();
/*!
 * Start Bootstrap - Freelancer Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {    
    var form_registro = $('form.register');
    if (form_registro.length > 0) {
        var childs_select = form_registro.find('select#childrens');
        childs_select.on('change', function() {
            var childs = $(this).val();
            form_registro.find('.children').addClass('hidden');
            for ($i = 1; $i <= childs; $i++) {
                form_registro.find('.children-' + $i).toggleClass('hidden');
            }
        });
    }
    
    $('.prevent-default').click(function(e) {
        e.preventDefault();
    });
    
    $('.logout-btn').click(function(e){
        e.preventDefault();
        logout();
    });

    var footable = $('.points-table table.table, .admin-points-table table.table');
    if (footable.length > 0) {
        footable.footable();
        $('select.filter-status').change(function(e){
            e.preventDefault();
            if($(this).val() == 'clear') {
                footable.trigger('footable_clear_filter');
            } else {
                $('#filter').val($(this).val());
                footable.trigger('footable_filter', {filter: $('#filter').val()});
            }
        });
    }

    $('a[href="#share-modal"]').click(function(e){
        e.preventDefault();
        $('#share-modal').modal('show');
    });

    var datePicker = $('.date-picker');
    if (datePicker.length > 0) {
        datePicker.datepicker({
            format : 'yyyy-mm-dd',
            disableTouchKeyboard : true,
            language : 'es_ES'
        });
    }
    
    var chosen = $('.chosen-select');
    if (chosen.length > 0) {
        chosen.chosen({
           no_results_text : 'No se han encontrado resultados' 
        });
    }
});
// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

function check_register(action) {
    the_loader('show');
    $('input, select').blur( function() {
        $(this).parent().removeClass('animated shake error');
    });
    var form_registro = $('form.register'),
        name = $('input#name'),
        second_name = $('input#second-name'),
        lastname = $('input#lastname'),
        second_lastname = $('input#second-lastname'),
        doc_type = $('select#doc-type'),
        documento = $('input#document'),
        city = $('select#city'),
        phone = $('input#phone'),
        address = $('input#address'),
        hood = $('input#hood'),
        marital_status = $('select#marital-status'),
        scale = $('select#scale'),
        decree = $('select#decree'),
        institute = $('select#institute'),
        home_yes = $('input#home-yes'),
        home_no = $('input#home-no'),
        saves_yes = $('input#saves-yes'),
        saves_no = $('input#saves-no'),
        sports = $('input#sport'),S
        family_activities = $('input#family-activities'),
        family_place = $('input#family-place'),
        family_programs = $('input#family-programs'),
        programs = $('input#programs'),
        ref_code = $('input#refercode'),
        email = $('input#email'),
        pwd = $('input#password'),
        ter_con = $('input#ter_con'),
        childrens = $('select#childrens'),
        child_1 = $('input#child-1'),
        child_2 = $('input#child-2'),
        child_3 = $('input#child-3'),
        child_4 = $('input#child-4'),
        child_5 = $('input#child-5'),
        child_6 = $('input#child-6'),
        birthday = $('input#birthday');
    
    if (is_empty_field('input', name)) {
        name.focus().parent().addClass('animated shake error');
        alert_modal('El <strong>NOMBRE</strong> es obligatorio');    
    } else if (is_empty_field('input', lastname)) {
        lastname.focus().parent().addClass('animated shake error');
        alert_modal('El <strong>APELLIDO</strong> es obligatorio');    
    } else if (is_empty_field('select', doc_type) && action != 'update') {
        doc_type.focus().parent().addClass('animated shake error');
        alert_modal('Seleccione un <strong>TIPO DE DOCUMENTO</strong>');    
    } else if (is_empty_field('input', documento) && action != 'update') {
        documento.focus().parent().addClass('animated shake error');
        alert_modal('El <strong>NÚMERO DE DOCUMENTO</strong> es obligatorio');    
    } else if (is_empty_field('birthday', birthday) && action != 'new_user') {
        birthday.focus().parent().addClass('animated shake error');
        alert_modal('Seleccione tu <strong>FECHA DE NACIMIENTO</strong>');    
    } else if (is_empty_field('select', city)) {
        city.focus().parent().addClass('animated shake error');
        alert_modal('Seleccione una <strong>CIUDAD</strong>');    
    } else if (is_empty_field('input', phone)) {
        phone.focus().parent().addClass('animated shake error');
        alert_modal('Ingrese un número de <strong>CELULAR</strong>');    
    } else if (is_empty_field('input', address)) {
        address.focus().parent().addClass('animated shake error');
        alert_modal('Ingrese una <strong>DIRECCIÓN</strong>');    
    } else if (is_empty_field('input', hood)) {
        hood.focus().parent().addClass('animated shake error');
        alert_modal('Ingrese el nombre del <strong>BARRIO</strong> donde reside');    
    } else if (is_empty_field('select', marital_status) && action != 'new_user') {
        marital_status.focus().parent().addClass('animated shake error');
        alert_modal('Seleccione su <strong>ESTADO CIVIL</strong>');    
    } else if (is_empty_field('select', scale)) {
        scale.focus().parent().addClass('animated shake error');
        alert_modal('Seleccione su <strong>ESCALAFÓN</strong>');
    } else if (is_empty_field('select', decree)) {
        decree.focus().parent().addClass('animated shake error');
        alert_modal('Seleccione el <strong>DECRETO QUE LO AMPARA</strong>');
    } else if (is_empty_field('select', institute)) {
        institute.focus().parent().addClass('animated shake error');
        alert_modal('Seleccione la <strong>INSTITUCIÓN</strong> a la que pertenece');
    } else if (is_empty_field('radio', home_yes) && action != 'new_user') {
        home_yes.focus().parent().parent().parent().addClass('animated shake error');
        alert_modal('Indique si tiene o no vivienda propia');
    } else if (is_empty_field('radio', saves_yes) && action != 'new_user') {
        saves_yes.focus().parent().parent().parent().addClass('animated shake error');
        alert_modal('¿Cuenta con ahorros para adquirir su vivienda propia?');
    } else if (is_empty_field('input', sports) && action != 'new_user') {
        sports.focus().parent().addClass('animated shake error');
        alert_modal('Cuentenos que <strong>DEPORTE</strong> practica');
    } else if (is_empty_field('input', family_activities) && action != 'new_user') {
        family_activities.focus().parent().addClass('animated shake error');
        alert_modal('¿Qué actividades de entretenimiento realiza con su familia en su tiempo libre?');
    } else if (is_empty_field('input', family_place) && action != 'new_user') {
        family_place.focus().parent().addClass('animated shake error');
        alert_modal('¿Donde realiza las actividades de entretenimiento con su familia?');
    } else if (is_empty_field('input', programs) && action != 'new_user') {
        programs.focus().parent().addClass('animated shake error');
        alert_modal('¿Qué programas le interesarían?');
    } else if (is_empty_field('input', family_programs) && action != 'new_user') {
        family_programs.focus().parent().addClass('animated shake error');
        alert_modal('¿Qué programas le interesarían?');
    } else if (ref_code.length > 0 && is_empty_field('input', ref_code) && action != 'update' && action != 'new_user') {
        ref_code.focus().parent().addClass('animated shake error');
        alert_modal('Por favor ingresa el <strong>CÓDIGO DE REFERIDO</strong> para continuar');
    } else if (ref_code.length > 0 && is_empty_field('input', ref_code) && action != 'update' && action != 'new_user') {
        ref_code.focus().parent().addClass('animated shake error');
        alert_modal('El <strong>CÓDIGO DE REFERIDO</strong> es obligatorio');
    } else if (pwd.length > 0 && is_empty_field('input', pwd) && action != 'update') {
        pwd.focus().parent().addClass('animated shake error');
        alert_modal('Debes ingresar una <strong>CONTRASEÑA</strong> para continuar')
    } else if (is_empty_field('checkbox', ter_con) && action != 'update' && action != 'new_user') {
        ter_con.focus().parent().addClass('animated shake error');
        alert_modal('Debes aceptar los <strong>TÉRMINOS Y CONDICIONES</strong> para continuar');
    } else if (is_empty_field('input', email) && action == 'new_user') {
        email.focus().parent().addClass('animated shake error');
        alert_modal('El <strong>EMAIL</strong> es obligatorio');
    } else {
        if (action == 'register') {
            $.post(maestros_data.ajax, {
                nonce : maestros_data.db_nonce,
                action : 'verify_refcode',
                code : ref_code.val(),
                email : email.val()
            }, function(response) {
                the_loader('hide');
                if (response.error) {
                    alert_modal(response.error);
                } else {
                    $.post(maestros_data.ajax, {
                        nonce : maestros_data.db_nonce,
                        action : 'verify_document',
                        document : documento.val()
                    }, function(response) {
                        the_loader('hide');
                        if (response.error_1) {
                            alert_modal(response.error_1);
                        } else {
                            if (response.error) {
                                var afiliation = 0,
                                    action = 'register_no_afiliated',
                                    maestro = 0;
                            } else {
                                var afiliation = 1,
                                    action = 'register_refered',
                                    maestro = 0;
                                if (response.success == 1) {
                                    maestro = 1;
                                }
                            }
                            if (home_yes[0].checked) {
                                home = 1;
                            } else {
                                home = 0;
                            }
                            if (saves_yes[0].checked) {
                                saves = 1;
                            } else {
                                saves = 0;
                            }
                            $.post(maestros_data.ajax, {
                                action : action,
                                nonce : maestros_data.ref_nonce,
                                name : name.val(),
                                second_name : second_name.val(),
                                lastname : lastname.val(),
                                second_lastname : second_lastname.val(),
                                doc_type : doc_type.val(),
                                documento : documento.val(),
                                city : city.val(),
                                phone : phone.val(),
                                address : address.val(),
                                hood : hood.val(),
                                marital_status : marital_status.val(),
                                scale : scale.val(), 
                                decree : decree.val(), 
                                institute : institute.val(), 
                                home : home, 
                                saves : saves, 
                                sports : sports.val(), 
                                family_activities : family_activities.val(), 
                                family_place : family_place.val(), 
                                family_programs : family_programs.val(), 
                                programs : programs.val(), 
                                ref_code : ref_code.val(), 
                                email : email.val(), 
                                pwd : pwd.val(),
                                afiliation : afiliation,
                                ter_con : 1,
                                childrens : childrens.val(),
                                child_1 : child_1.val(),
                                child_2 : child_2.val(),
                                child_3 : child_3.val(),
                                child_4 : child_4.val(),
                                child_5 : child_5.val(),
                                child_6 : child_6.val(),
                                birthday : birthday.val(),
                                maestro : maestro
                            }, function(response) {
                                the_loader('hide');
                                if (response.error) {
                                    alert_modal(response.error);
                                } else if (response.success == 1) {
                                    window.location.href = maestros_data.dashboard_url;
                                } else if (response.success == 2) {
                                    alert_modal('El número de cédula ingresado no se encuentra afiliado a Comfenalco Valle, tus datos se entregaran a nuestros agentes quienes se comunicarán contigo');
                                    $('#alert-modal').on('hidden.bs.modal', function(){
                                        window.location.href = maestros_data.home_url;
                                    });
                                } else if (response.success == 3) {
                                    alert_modal('El número de cédula ingresado se encuentra afiliado a Comfenalco Valle, pero no registra como maestro afiliado. Tus datos serán verificados por el personal encargado, este proceso puede tomar entre tres(3) y cinco(5) días hábiles. La respuesta será enviada al correo electrónico proporcionado.');
                                    $('#alert-modal').on('hidden.bs.modal', function(){
                                        window.location.href = maestros_data.home_url;
                                    });
                                }
                            }, 'json');
                        }   
                    }, 'json');
                }
            }, 'json');
        } else if (action == 'update') {
            if (home_yes[0].checked) {
                home = 1;
            } else {
                home = 0;
            }
            if (saves_yes[0].checked) {
                saves = 1;
            } else {
                saves = 0;
            }
            $.post(maestros_data.ajax, {
                action : 'update_user',
                user_id : $('#current-user-id').val(),
                nonce : maestros_data.user_nonce,
                name : name.val(),
                second_name : second_name.val(),
                lastname : lastname.val(),
                second_lastname : second_lastname.val(),
                city : city.val(),
                phone : phone.val(),
                address : address.val(),
                hood : hood.val(),
                marital_status : marital_status.val(),
                scale : scale.val(), 
                decree : decree.val(), 
                institute : institute.val(), 
                home : home, 
                saves : saves, 
                sports : sports.val(), 
                family_activities : family_activities.val(), 
                family_place : family_place.val(), 
                family_programs : family_programs.val(), 
                programs : programs.val(),
                pwd : pwd.val(),
                childrens : childrens.val(),
                child_1 : child_1.val(),
                child_2 : child_2.val(),
                child_3 : child_3.val(),
                child_4 : child_4.val(),
                child_5 : child_5.val(),
                child_6 : child_6.val(),
                birthday : birthday.val()
            }, function(response) {
                the_loader('hide');
                if (response.error) {
                    alert_modal(response.error);
                } else if (response.success == 1) {
                    alert_modal('Datos actualizados correctamente');
                    $('#alert-modal').on('hidden.bs.modal', function() {
                        window.location.href = maestros_data.dashboard_url;
                    });
                } else if (response.success == 2) {
                    alert_modal('Datos actualizados correctamente.<p><strong>Contraseña cambiada:</strong> Debes iniciar sesión con tu nueva contraseña.</p>');
                    $('#alert-modal').on('hidden.bs.modal', function() {
                        window.location.href = maestros_data.dashboard_url;
                    });
                }
            }, 'json');
        } else if (action == 'new_user') {
            $.post(maestros_data.ajax, {
                nonce : maestros_data.db_nonce,
                action : 'verify_document',
                document : documento.val()
            }, function(response) {
                the_loader('hide');
                if (response.error_1) {
                    alert_modal(response.error_1);
                } else {
                    if (response.error) {
                        var afiliation = 0,
                            action = 'register_user_no_afiliated',
                            maestro = 0;
                    } else {
                        var afiliation = 1,
                            action = 'register_user',
                            maestro = 0;
                        if (response.success == 1) {
                            maestro = 1;
                        }
                    }
                    if (home_yes[0].checked) {
                        home = 1;
                    } else {
                        home = 0;
                    }
                    if (saves_yes[0].checked) {
                        saves = 1;
                    } else {
                        saves = 0;
                    }
                    $.post(maestros_data.ajax, {
                        action : action,
                        user_id : $('#current-user-id').val(),
                        nonce : maestros_data.ref_nonce,
                        name : name.val(),
                        second_name : second_name.val(),
                        lastname : lastname.val(),
                        second_lastname : second_lastname.val(),
                        doc_type : doc_type.val(),
                        documento : documento.val(),
                        city : city.val(),
                        phone : phone.val(),
                        address : address.val(),
                        hood : hood.val(),
                        marital_status : marital_status.val(),
                        scale : scale.val(), 
                        decree : decree.val(), 
                        institute : institute.val(), 
                        home : home, 
                        saves : saves, 
                        sports : sports.val(), 
                        family_activities : family_activities.val(), 
                        family_place : family_place.val(), 
                        family_programs : family_programs.val(), 
                        programs : programs.val(), 
                        ref_code : ref_code.val(), 
                        email : email.val(), 
                        pwd : pwd.val(),
                        afiliation : afiliation,
                        ter_con : 1,
                        childrens : childrens.val(),
                        child_1 : child_1.val(),
                        child_2 : child_2.val(),
                        child_3 : child_3.val(),
                        child_4 : child_4.val(),
                        child_5 : child_5.val(),
                        child_6 : child_6.val(),
                        birthday : birthday.val(),
                        maestro : maestro
                    }, function(response) {
                        the_loader('hide');
                        if (response.error) {
                            alert_modal(response.error);
                        } else if (response.success == 1) {
                            window.location.href = maestros_data.dashboard_url;
                        } else if (response.success == 2) {
                            alert_modal('El número de cédula ingresado no se encuentra afiliado a Comfenalco Valle, tus datos se entregaran a nuestros agentes quienes se comunicarán contigo');
                            $('#alert-modal').on('hidden.bs.modal', function(){
                                window.location.href = maestros_data.home_url;
                            });
                        } else if (response.success == 3) {
                            alert_modal('La cédula ingresada esta afiliada a Comfenalco Valle, pero no registra como maestro afiliado, se enviarán los datos ingresados al personal encargado de la verificación.');
                            $('#alert-modal').on('hidden.bs.modal', function() {
                                window.location.href = maestros_data.dashboard_url;
                            });
                        }
                    }, 'json');
                }   
            }, 'json');
        }  
    }
}

function add_alert(msg, type, field) {
    var num = Math.floor((Math.random() * 999) + 1);
    var alert = '<div style="display: none;" class="alert alert-' + num + ' ' + type + ' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + msg + '</div>';
    $(field).prepend(alert);
    $(field).find('.alert-' + num).fadeIn();
    setTimeout(function() {
        $('.alert-' + num).fadeOut(function() {
            $(this).remove();
        });
    }, 5000);
}

function goto_page(url) {
    window.location.href = url;
}

function logout() {
    the_loader('show');
    $.post(maestros_data.ajax, {
        action : 'logout',
        nonce : maestros_data.user_nonce
    }, function(response) {
       if (response == 'logged_out') {
           window.location.href = maestros_data.home_url;
       } else {
           the_loader('hide');
           alert_modal('No hemos podido cerrar tu sesión, por favor inténtalo nuevamente o borra el cache de tu navegador');
       }
    });
}

function login(where) {
    var place = '';
    switch (where) {
        case 'modal' :
            place = '#login-modal ';
            break;
        case 'page' :
            place = '#login-page ';
            break;
    }
    var log = $(place + 'form.form-signin input#user').val(),
        pwd = $(place + 'form.form-signin input#pass').val();
    
    the_loader('show');
    
    $.post(maestros_data.ajax, {
        action : 'login_app',
        nonce : maestros_data.user_nonce,
        log : log,
        pwd : pwd
    }, function(response) {
        the_loader('hide');
        if (response.error) {
            alert_modal(response.error);
        } else if(response.user) {
            var redirect_url = maestros_data.dashboard_url;
            window.location.href = redirect_url;
        } else {
            alert_modal('En este momento no es posible iniciar sesión, por favor inténtalo más tarde');
        }
    }, 'json');
}

function alert_modal(msg) {
    the_loader('hide');
    var modal = $('#alert-modal'),
        modal_body = modal.find('.modal-body');
    modal_body.html(msg);
    modal.modal('show');
}

function add_referido() {
    var table = $('#referidos .referidos table'),
        source = table.find('tbody tr.source'),
        ind = table.attr('items');
    
    ind++;
    
    source.clone()
        .appendTo(table.find('tbody'))
        .hide()
        .removeClass('source')
        .find('th[scope="row"]')
        .html(ind)
        .parent().fadeIn()
        .find('input').val('');
    
    table.attr('items', ind);
}

function is_empty_field(type, field) {
    var back = false;
    switch (type) {
        case 'input' :
            if (!field.val() || field.val() == '' || field.val() == null) {
                back = true;
            }
            break;
        case 'select' : 
            if (!field.val() || field.val() == '' || field.val() == null || field.val() == 'blank') {
                back = true;
            }
            break;
        case 'radio' :
            var name = field.attr('name');
            if ($('input[name="' + name + '"]:radio:checked').length == 0) {
                back = true;
            }
            break;
        case 'checkbox' :
            if (!field[0].checked) {
                back = true;
            }
            break;
        case 'recaptcha' :
            $.post('https://www.google.com/recaptcha/api/siteverify', {
                secret : maestros_data.rcs,
                response : $('textarea[name="g-recaptcha-response]').val()
            }, function(response) {
                if (!response.success) {
                    back = true;
                }
            });
            break;
        case 'date' :
            if (!field.val() || field.val() == '' || field.val() == null || field.val() == '0000-00-00') {
                back = true;
            }
            break;
    }
    return back;
}

function recaptcha_init() {

}

function verify_document(document) {
    var back;
    $.post(maestros_data.ajax, {
        nonce : maestros_data.db_nonce,
        action : 'verify_document',
        document : document
    }, function(response) {
        if (response.error) {
            back = false;
        } else if (response.success) {
            back = true;
        }
    }, 'json');
    return back;
}

function verify_refcode(code, email) {
    var back;
    $.post(maestros_data.ajax, {
        nonce : maestros_data.db_nonce,
        action : 'verify_refcode',
        code : code,
        email : email
    }, function(response) {
        if (response.error) {
            alert_modal(response.error);
            back = false;
        } else {
            back = true;
        }
    }, 'json');

    return back;
}

function invite_the_people() {
    var people = $('table.items tbody tr'),
        total_people = people.length,
        people_percent = (1*100)/total_people,
        actual_percent = 0,
        modal = $('#loader-modal'),
        progress = modal.find('.progress .progress-bar'),
        time = 200,
        user_id = $('input#user-id').val(),
        invitations = 0;
    
    modal.modal('show');
    modal.find('ul.list-group').html('');
    people.each(function() {
        var name = $(this).find('input.name').val(),
            email = $(this).find('input.email').val(),
            num = $(this).find('th[scope="row"]').html();
        setTimeout(function(){
            var msg = '';
            if (!email) {
                msg = '<i>(' + num + ')</i> Datos incompletos'; 
                var alert = '<li class="list-group-item">' + msg + '</li>';
                modal.find('ul.list-group').append(alert);
                var new_percent = Math.ceil(actual_percent + people_percent);
                if (new_percent > 100) new_percent = 100;
                progress.css('width', new_percent + '%').find('span').html(new_percent);
                actual_percent = new_percent;
                if (new_percent == 100) {
                    modal.find('.modal-footer').fadeIn();
                }
            } else {
                $.post(maestros_data.ajax, {
                    action : 'invite_people',
                    nonce : maestros_data.ref_nonce,
                    name : name,
                    email : email,
                    user_id : user_id
                }, function(response) {
                    if (response.error) {
                        var alert = '<li class="list-group-item"><i>(' + num + ')</i> ' + response.error + '</li>';
                        modal.find('ul.list-group').append(alert);
                    } else {
                        invitations++;
                    }
                    var new_percent = Math.ceil(actual_percent + people_percent);
                    if (new_percent > 100) new_percent = 100;
                    progress.css('width', new_percent + '%').find('span').html(new_percent);
                    actual_percent = new_percent;
                    if (new_percent == 100) {
                        var alert = '<li class="list-group-item">Se han invitado exitosamente a <strong>' + invitations + '</strong> referidos.</li>';
                        modal.find('ul.list-group').append(alert);
                        modal.find('.modal-footer').fadeIn();
                    }
                }, 'json')
            }
        }, time);
        time += 200;
    });
    modal.on('hidden.bs.modal', function(){
        window.location.reload();
    });
}

function reset_ref_table() {
    var table = $('.referidos'),
        reset_val = '<h3>Invítalos aquí y suma puntos</h3><table class="table table-bordered table-striped items" items="1"><thead><tr><th>#</th><th>Nombre</th><th>Correo</th></tr></thead><tbody><tr class="source"><th scope="row">1</th><td class="name"><input type="text" placeholder="Nombre" class="name" /></td><td class="email"><input type="email" placeholder="Correo" class="email" /></td></tr></tbody></table><button type="button" onclick="add_referido();" class="btn btn-sm btn-default adder">Añadir Referido</button><div class="divider clearfix"></div><button type="button" onclick="invite_the_people();" class="btn btn-lg btn-primary inviter">Invitar a mis referidos</button>';

    table.fadeOut(function(){
        table.html(reset_val);
        table.fadeIn();
    })
}

function the_loader(action) {
    var loader = $('#loader-app');
    if (action == 'show') {
        loader.fadeIn();
    }
    if (action == 'hide') {
        loader.fadeOut();
    }
}

function authorize_user(user_id) {
    the_loader('show');
    $.post(maestros_data.ajax, {
        action : 'update_user_data',
        nonce : maestros_data.ref_nonce,
        field : 'MAESTRO',
        value : '1',
        user : user_id
    }, function(response) {
        the_loader('hide');
        alert_modal(response);
        $('#alert-modal').on('hidden.bs.modal', function() {
            window.location.reload();
        });
    });
}