<?php
	/**
	 * Template Name: Admin Backend
	 */
	
	if (get_user_role() == 'editor' || get_user_role() == 'administrator') {
		get_header('admin');
		switch (get_query_var('uaction')) {
			case 'authorize' :
            	get_template_part('templates/admin/admin', 'authorize');
				break;
			case 'manager' :
				if (backend_allowed(get_current_user_id())) {
					get_template_part('templates/admin/admin', 'manager');
				} else {
            		get_template_part('templates/admin/admin', 'searcher');
				}
				break;
			default :
            	get_template_part('templates/admin/admin', 'searcher');
				break;
		}
		get_footer('admin');
	} else {
        wp_redirect(get_bloginfo('url'));
        exit;		
	}
?>