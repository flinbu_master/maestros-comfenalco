<?php
    /**
     * Template Name: Scripts
     */ 
    
    // $db_data = array(
    //     'name' => 'guayabay_comfenalco',
    //     'user' => 'guayabay_develol',
    //     'pass' => 'Eg+-997185',
    //     'table' => 'wp_old_data',
    //     'server' => 'localhost'
    // );
    $exec = 0;

    if ($exec) :
    $old_fields = array('ciudad','celular','documento','segundoApellido','segundoNombre','primerApellido','primerNombre','correo','direccion','barrio','institucion','fechaDeNacimiento','digitador','TerminosYcondiciones','CantidadHijos','estadocivil','escalafon','decreto','asesor','vivienda','cuentaAhorros','deporte','dondeDeporte','actividades','dondeActividades','programaDeEducacion','cursosParaEsposYPadres','cursosParaHijos','empresa');
    $new_fields = array(
        'primerNombre' => 'NOMBRE',
        'segundoNombre' => 'SEGUNDO_NOMBRE',
        'primerApellido' => 'APELLIDO',
        'segundoApellido' => 'SEGUNDO_APELLIDO',
        'cedula_ciudadania' => 'TIPO_DOCUMENTO',
        'documento' => 'NUM_DOCUMENTO',
        'ciudad' => 'CIUDAD',
        'celular' => 'CELULAR',
        'direccion' => 'DIRECCION',
        'barrio' => 'BARRIO',
        'institucion' => 'INSTITUCION',
        'correo' => 'EMAIL',
        'TerminosYcondiciones' => 'TER_CON',
        'CantidadHijos' => 'CANT_HIJOS',
        'estadocivil' => 'EST_CIVIL',
        'escalafon' => 'ESCALAFON',
        'decreto' => 'DECRETO',
        'asesor' => 'NOM_ASESOR',
        'vivienda' => 'VIVIENDA',
        'deporte' => 'DEPORTE',
        'dondeDeporte' => 'LUGAR_DEPORTE',
        'actividades' => 'ACT_TL_FAMILIA',
        'dondeActividades' => 'LUGAR_TL_FAMILIA',
        'programaDeEducacion' => 'PROG_EDU',
        'cursosParaEsposYPadres' => 'CURSOS_ESP_PADRES',
        'cursosParaHijos' => 'CURSOS_HIJOS',
        'cuentaAhorros' => 'C_BANCARIA',
        'fechaDeNacimiento' => 'CUMPLEANOS',
        'digitador' => 'DIGITADOR',
      	'empresa' => 'EMPRESA',
        'fechaFormulario' => 'FECHA_INGRESO'
		);
    
    $fields = "`NOMBRE`,`SEGUNDO_NOMBRE`,`APELLIDO`,`SEGUNDO_APELLIDO`,`TIPO_DOCUMENTO`,`NUM_DOCUMENTO`,`CIUDAD`,`CELULAR`,`DIRECCION`,`BARRIO`,`INSTITUCION`,`CUMPLEANOS`,`EMPRESA`,`EMAIL`,`TER_CON`,`CANT_HIJOS`,`EDADES_HIJOS`,`EST_CIVIL`,`ESCALAFON`,`DECRETO`,`NOM_ASESOR`,`VIVIENDA`,`DEPORTE`,`LUGAR_DEPORTE`,`ACT_TL_FAMILIA`,`LUGAR_TL_FAMILIA`,`PROG_EDU`,`CURSOS_ESP_PADRES`,`CURSOS_HIJOS`,`FECHA_INGRESO`,`COD_REFERIDOS`,`VAL_AFILIADO`,`MAESTRO`,`PIONEER`,`PUNTOS_PENDIENTES`,`TOTAL_DE_PUNTOS`,`AHORROS`,`DIGITADOR`";
    

    $users = get_users(array(
        'role' => 'subscriber',
        'number' => 500,
        'offset' => 500
    ));
    $users_obj = array();

    $db_data = get_db_connection_data();
    $link = mysql_connect($db_data['server'], $db_data['user'], $db_data['pass']) or die(mysql_error());
    mysql_select_db($db_data['name'], $link) or die(mysql_error());
    
    $ti = 1;
    foreach ($users as $user) {
      $user_email = $user->data->user_email;
      $query_maestros = "SELECT * FROM `InscritosMaestros` WHERE `correo`='$user_email'";
      $rs = mysql_query($query_maestros, $link) or die(mysql_error());

      $meta_data = array();

      if (mysql_num_rows($rs) > 0) {
        while ($data = mysql_fetch_array($rs)) {
            $meta_data = $data;
            $meta_data['table'] = 'InscritosMaestros';
            
            $nombre = addslashes((isset($data['primerNombre'])) ? $data['primerNombre'] : '');
            $segundo_nombre = addslashes((isset($data['segundoNombre'])) ? $data['segundoNombre'] : '');
            $apellido = addslashes((isset($data['primerApellido'])) ? $data['primerApellido'] : '');
            $segundo_apellido = addslashes((isset($data['segundoApellido'])) ? $data['segundoApellido'] : '');
            $tipo_documento = addslashes(('cc') ? 'cc' : '');
            $num_documento = addslashes((isset($data['documento'])) ? $data['documento'] : '');
            $ciudad = addslashes((isset($data['ciudad'])) ? $data['ciudad'] : '');
            $celular = addslashes((isset($data['celular'])) ? $data['celular'] : '');
            $direccion = addslashes((isset($data['direccion'])) ? $data['direccion'] : '');
            $barrio = addslashes((isset($data['barrio'])) ? $data['barrio'] : '');
            $institucion = addslashes((isset($data['institucion'])) ? $data['institucion'] : '');
            $cumpleanos = addslashes((isset($data['fechaDeNacimiento'])) ? $data['fechaDeNacimiento'] : '');
            $empresa = addslashes('');
            $ter_con = addslashes(1);
            $cant_hijos = addslashes((isset($data['CantidadHijos'])) ? $data['CantidadHijos'] : '0');
            $edades_hijos = addslashes((isset($data['hijo1'])) ? $data['hijo1'] : '0' . '/' . (isset($data['hijo2'])) ? $data['hijo2'] : '0' . '/' . (isset($data['hijo3'])) ? $data['hijo3'] : '0' . '/' . (isset($data['hijo4'])) ? $data['hijo4'] : '0' . '/' . (isset($data['hijo5'])) ? $data['hijo5'] : '0' . '/0');
            $est_civil = addslashes((isset($data['estadocivil'])) ? $data['estadocivil'] : '');
            $escalafon = addslashes((isset($data['escalafon'])) ? $data['escalafon'] : '');
            $decreto = addslashes(str_replace('- DECRETO ', '', (isset($data['decreto'])) ? $data['decreto'] : ''));
            $nom_asesor = addslashes((isset($data['asesor'])) ? $data['asesor'] : '');
            $vivienda = addslashes(($data['vivienda'] == 'si') ? 1 : 0);
            $ahorros = addslashes(($data['cuentaAhorros'] == 'si') ? 1 : 0);
            $deporte = addslashes((isset($data['deporte'])) ? $data['deporte'] : '');
            $lugar_deporte = addslashes((isset($data['dondeDeporte'])) ? $data['dondeDeporte'] : '');
            $act_tl_familia = addslashes((isset($data['actividades'])) ? $data['actividades'] : '');
            $lugar_tl_familia = addslashes((isset($data['dondeActividades'])) ? $data['dondeActividades'] : '');
            $prog_edu = addslashes((isset($data['programaDeEducacion'])) ? $data['programaDeEducacion'] : '');
            $cursos_esp_padres = addslashes((isset($data['cursosParaEsposYPadres'])) ? $data['cursosParaEsposYPadres'] : '');
            $cursos_hijos = addslashes((isset($data['cursosParaHijos'])) ? $data['cursosParaHijos'] : '');
            $fecha_ingreso = (isset($data['FechaFormulario'])) ? $data['FechaFormulario'] : 'NOW()';
            $cod_referidos = strtoupper(substr($nombre, 0, 1) . substr($apellido, 0, 1)) . $user->ID;
            $val_afiliado = addslashes(1);
            $maestro = addslashes(1);
            $pioneer = addslashes(1);
            $puntos_pendientes = addslashes(0);
            $total_de_puntos = addslashes(500);
            $digitador = addslashes(NULL);
                
            $values = "'$nombre','$segundo_nombre','$apellido','$segundo_apellido','$tipo_documento','$num_documento','$ciudad','$celular','$direccion','$barrio','$institucion','$cumpleanos','$empresa','$user_email','$ter_con','$cant_hijos','$edades_hijos','$est_civil','$escalafon','$decreto','$nom_asesor','$vivienda','$deporte','$lugar_deporte','$act_tl_familia','$lugar_tl_familia','$prog_edu','$cursos_esp_padres','$cursos_hijos','$fecha_ingreso','$cod_referidos','$val_afiliado','$maestro','$pioneer','$puntos_pendientes','$total_de_puntos','$ahorros','$digitador'";
            
            $insert_query = "INSERT INTO `wp_registro_maestro`($fields) VALUES ($values)";
            echo  $ti . ' - ' . $insert_query . '<br />';
            
            mysql_query($insert_query, $link) or die(mysql_error());
            add_extra_data_by_user_email($user_email,'document',$num_documento);
            
            $puntos = get_theme_option('reg_points');
            $xq_puntos = 'Registro';
            $points_query = "INSERT INTO `wp_act_puntos`(`CEDULA`,`email_act_puntos`,`punto_recibido`,`accion_punto`,`email_referido`,`fecha_de_ingreso`,`fecha_activacion`,`ACTIVE`) VALUES ('$num_documento','$user_email','$puntos','$xq_puntos','','$fecha_ingreso',NOW(),'1')";
            mysql_query($points_query, $link) or die(mysql_error());
        }
      } else {
        $query_no_maestros = "SELECT * FROM `InscritosNoMaestros` WHERE `correo`='$user_email'";
        $rs_ni = mysql_query($query_no_maestros, $link) or die(mysql_error());

        if (mysql_num_rows($rs_ni) > 0) {
          while ($data = mysql_fetch_array($rs_ni)){
            $meta_data = $data;
            $meta_data['table'] = 'InscritosNoMaestros';
            
            $nombre = addslashes((isset($data['primerNombre'])) ? $data['primerNombre'] : '');
            $segundo_nombre = addslashes((isset($data['segundoNombre'])) ? $data['segundoNombre'] : '');
            $apellido = addslashes((isset($data['primerApellido'])) ? $data['primerApellido'] : '');
            $segundo_apellido = addslashes((isset($data['segundoApellido'])) ? $data['segundoApellido'] : '');
            $tipo_documento = addslashes(('cc') ? 'cc' : '');
            $num_documento = addslashes((isset($data['documento'])) ? $data['documento'] : '');
            $ciudad = addslashes((isset($data['ciudad'])) ? $data['ciudad'] : '');
            $celular = addslashes((isset($data['celular'])) ? $data['celular'] : '');
            $direccion = addslashes((isset($data['direccion'])) ? $data['direccion'] : '');
            $barrio = addslashes((isset($data['barrio'])) ? $data['barrio'] : '');
            $institucion = addslashes((isset($data['institucion'])) ? $data['institucion'] : '');
            $cumpleanos = addslashes((isset($data['fechaDeNacimiento'])) ? $data['fechaDeNacimiento'] : '');
            $empresa = addslashes('');
            $ter_con = addslashes(1);
            $cant_hijos = addslashes((isset($data['CantidadHijos'])) ? $data['CantidadHijos'] : '0');
            $edades_hijos = addslashes((isset($data['hijo1'])) ? $data['hijo1'] : '0' . '/' . (isset($data['hijo2'])) ? $data['hijo2'] : '0' . '/' . (isset($data['hijo3'])) ? $data['hijo3'] : '0' . '/' . (isset($data['hijo4'])) ? $data['hijo4'] : '0' . '/' . (isset($data['hijo5'])) ? $data['hijo5'] : '0' . '/0');
            $est_civil = addslashes((isset($data['estadocivil'])) ? $data['estadocivil'] : '');
            $escalafon = addslashes((isset($data['escalafon'])) ? $data['escalafon'] : '');
            $decreto = addslashes(str_replace('- DECRETO ', '', (isset($data['decreto'])) ? $data['decreto'] : ''));
            $nom_asesor = addslashes((isset($data['asesor'])) ? $data['asesor'] : '');
            $vivienda = addslashes(($data['vivienda'] == 'si') ? 1 : 0);
            $ahorros = addslashes(($data['cuentaAhorros'] == 'si') ? 1 : 0);
            $deporte = addslashes((isset($data['deporte'])) ? $data['deporte'] : '');
            $lugar_deporte = addslashes((isset($data['dondeDeporte'])) ? $data['dondeDeporte'] : '');
            $act_tl_familia = addslashes((isset($data['actividades'])) ? $data['actividades'] : '');
            $lugar_tl_familia = addslashes((isset($data['dondeActividades'])) ? $data['dondeActividades'] : '');
            $prog_edu = addslashes((isset($data['programaDeEducacion'])) ? $data['programaDeEducacion'] : '');
            $cursos_esp_padres = addslashes((isset($data['cursosParaEsposYPadres'])) ? $data['cursosParaEsposYPadres'] : '');
            $cursos_hijos = addslashes((isset($data['cursosParaHijos'])) ? $data['cursosParaHijos'] : '');
            $fecha_ingreso = (isset($data['FechaFormulario'])) ? strtotime($data['FechaFormulario']) : 'NOW()';
            $cod_referidos = addslashes(strtoupper(substr($nombre, 0, 1) . substr($apellido, 0, 1)) . $user->ID);
            $val_afiliado = addslashes(1);
            $maestro = addslashes(0);
            $pioneer = addslashes(1);
            $puntos_pendientes = addslashes(0);
            $total_de_puntos = addslashes(500);
            $digitador = addslashes(NULL);
                
            $values = "'$nombre','$segundo_nombre','$apellido','$segundo_apellido','$tipo_documento','$num_documento','$ciudad','$celular','$direccion','$barrio','$institucion','$cumpleanos','$empresa','$user_email','$ter_con','$cant_hijos','$edades_hijos','$est_civil','$escalafon','$decreto','$nom_asesor','$vivienda','$deporte','$lugar_deporte','$act_tl_familia','$lugar_tl_familia','$prog_edu','$cursos_esp_padres','$cursos_hijos','$fecha_ingreso','$cod_referidos','$val_afiliado','$maestro','$pioneer','$puntos_pendientes','$total_de_puntos','$ahorros','$digitador'";
            
            $insert_query = "INSERT INTO `wp_registro_maestro`($fields) VALUES ($values)";
            echo $ti . ' - ' . $insert_query . '<br />';
            
            mysql_query($insert_query, $link) or die(mysql_error());
            add_extra_data_by_user_email($user_email,'document',$num_documento);
            
            $puntos = get_theme_option('reg_points');
            $xq_puntos = 'Registro';
            $points_query = "INSERT INTO `wp_act_puntos`(`CEDULA`,`email_act_puntos`,`punto_recibido`,`accion_punto`,`email_referido`,`fecha_de_ingreso`,`fecha_activacion`,`ACTIVE`) VALUES ('$num_documento','$user_email','$puntos','$xq_puntos','','$fecha_ingreso',NOW(),'1')";
            mysql_query($points_query, $link) or die(mysql_error());
          }
        } else {
            $meta_data = 'User not found';
            echo $ti . ' - User not found <br />';
        }
      }
      $users_obj[] = array(
        'data' => $user_email,
        'metadata' => $meta_data
      );
        
        $ti++;
    }

    mysql_close($link);
    //print_pre($users_obj);
endif;
?>