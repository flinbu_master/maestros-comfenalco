<?php
	/**
	 * Template Name: Email Invitation
	 */
	$user_id = get_query_var('uname');
	$user = get_user_by('id', $user_id);
	$name = $user->data->display_name;
	$email = urldecode(get_query_var('uemail'));
	$code = get_user_refer_code($user_id);
	$logo = wp_get_attachment_image_src(get_theme_option('logo'), 'full');
	$logo_campaign = wp_get_attachment_image_src(get_theme_option('logo_campaign'), 'full');
	$url = get_permalink(get_theme_option('register_page')) . urlencode($email) . '/' . $code;
?>
<!DOCTYPE html "-//w3c//dtd xhtml 1.0 transitional //en" "http://www.w3.org/tr/xhtml1/dtd/xhtml1-transitional.dtd"><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head>
    <!--[if gte mso 9]><xml>
     <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
     </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">

    <title>Invitación a Maestros de la Gente</title>
        
</head>
<body style="width: 100% !important;min-width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100% !important;margin: 0;padding: 0;background-color: #ffe0b3">
    <style id="media-query">
      /*  Media Queries */
@media only screen and (max-width: 500px) {
  .prova {
    width: 500px; }
  table[class="body"] img {
    width: 100% !important;
    height: auto !important; }
  table[class="body"] center {
    min-width: 0 !important; }
  table[class="body"] .container {
    width: 95% !important; }
  table[class="body"] .row {
    width: 100% !important;
    display: block !important; }
  table[class="body"] .wrapper {
    display: block !important;
    padding-right: 0 !important; }
  table[class="body"] .columns, table[class="body"] .column {
    table-layout: fixed !important;
    float: none !important;
    width: 100% !important;
    padding-right: 0px !important;
    padding-left: 0px !important;
    display: block !important; }
  table[class="body"] .wrapper.first .columns, table[class="body"] .wrapper.first .column {
    display: table !important; }
  table[class="body"] table.columns td, table[class="body"] table.column td {
    width: 100% !important; }
  table[class="body"] table.columns td.expander {
    width: 1px !important; }
  table[class="body"] .right-text-pad, table[class="body"] .text-pad-right {
    padding-left: 10px !important; }
  table[class="body"] .left-text-pad, table[class="body"] .text-pad-left {
    padding-right: 10px !important; }
  table[class="body"] .hide-for-small, table[class="body"] .show-for-desktop {
    display: none !important; }
  table[class="body"] .show-for-small, table[class="body"] .hide-for-desktop {
    display: inherit !important; }
  table[class="icon-table"] {
    width: 100% !important; }
    table[class="icon-table"] table {
      display: block !important;
      width: 100% !important; }
    table[class="icon-table"] table td {
      padding-bottom: 10px !important; }
  .mixed-two-up .col {
    width: 100% !important; } }
 

      @media screen and (max-width: 500px) {
          div[class="col"] {
              width: 100% !important;
          }
      }

    @media screen and (min-width: 501px) {
        table[class="block-grid"] {
            width: 500px !important;
        }
    }
    </style>
    <table class="body" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;height: 100%;width: 100%;table-layout: fixed" cellpadding="0" cellspacing="0" width="100%" border="0">
      <tbody><tr style="vertical-align: top">
        <td class="center" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;text-align: center;background-color: #ffe0b3" align="center" valign="top">
        
          <!--[if (gte mso 9)|(IE)]>
          <table width='500' class="ieCell" align="center" cellpadding="0" cellspacing="0" border="0">
              <tr>
                  <td>
          <![endif]-->
          <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top;background-color: transparent" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
            <tbody><tr style="vertical-align: top">
              <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" width="100%">
                <table class="container" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;max-width: 500px;margin: 0 auto;text-align: inherit" cellpadding="0" cellspacing="0" align="center" width="100%" border="0"> 
                  <tbody><tr style="vertical-align: top">
                    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" width="100%">
                      <table class="block-grid" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;width: 100%;max-width: 500px;color: #000000;background-color: transparent" cellpadding="0" cellspacing="0" width="100%" bgcolor="transparent">
                          <tbody><tr style="vertical-align: top">
                              <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;text-align: center;font-size: 0">
                                  <!--[if (gte mso 9)|(IE)]>
                                  <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                      <tr>
                                          <td valign="top">
                                  <![endif]-->
                                  <div class="col num12" style="display: inline-block;vertical-align: top;width: 100%">
                                      <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                          <tbody><tr style="vertical-align: top">
                                              <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;border-top: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-left: 0px solid transparent">
<table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" width="100%" border="0" align="center">
    <tbody><tr style="vertical-align: top">
        <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;width: 100%;padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;text-align: center" align="center">
            <div style="max-width: 100%;" align="center">
                <img class="center" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: 100%;clear: both;display: block;border: none;height: auto;line-height: 100%;margin: 0 auto;float: none;max-width: 298px" border="0" width="100%" align="center" src="<?=$logo_campaign[0];?>" alt="Maestros de la Gente" title="Maestros de la Gente">
            </div>
        </td>
    </tr>
</tbody></table>
<table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" width="100%">
  <tbody><tr style="vertical-align: top">
    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
        <div style="color:#fe721e;line-height:120%;font-family:Tahoma,Verdana,Segoe,sans-serif;">            
        	<div style="font-size:14px;line-height:17px;text-align:center;color:#fe721e;font-family:Tahoma,Verdana,Segoe,sans-serif;"><span style="font-family:arial, helvetica, sans-serifpx; font-size:14px; line-height:17px;"><strong><span style="font-size: 28px; line-height: 33px;"><?=get_theme_option('email_invitation_title');?></span></strong></span></div>
        </div>
    </td>
  </tr>
</tbody></table>
                                              </td>
                                          </tr>
                                      </tbody></table>
                                  </div>
                                  <!--[if (gte mso 9)|(IE)]>
                                  </td><td>
                                  <![endif]-->
                                  <!--[if (gte mso 9)|(IE)]>
                                          </td>
                                      </tr>
                                  </table>
                                  <![endif]-->
                              </td>
                          </tr>
                      </tbody></table>
                    </td>
                  </tr>
                </tbody></table>
              </td>
            </tr>
          </tbody></table>
          <!--[if (gte mso 9)|(IE)]>
                     </td>
              </tr>
          </table>
          <![endif]-->
          <!--[if (gte mso 9)|(IE)]>
          <table width='500' class="ieCell" align="center" cellpadding="0" cellspacing="0" border="0">
              <tr>
                  <td>
          <![endif]-->
          <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top;background-color: #333333" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
            <tbody><tr style="vertical-align: top">
              <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" width="100%">
                <table class="container" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;max-width: 500px;margin: 0 auto;text-align: inherit" cellpadding="0" cellspacing="0" align="center" width="100%" border="0"> 
                  <tbody><tr style="vertical-align: top">
                    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" width="100%">
                      <table class="block-grid" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;width: 100%;max-width: 500px;color: #333;background-color: transparent" cellpadding="0" cellspacing="0" width="100%" bgcolor="transparent">
                          <tbody><tr style="vertical-align: top">
                              <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;text-align: center;font-size: 0">
                                  <!--[if (gte mso 9)|(IE)]>
                                  <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                      <tr>
                                          <td valign="top">
                                  <![endif]-->
                                  <div class="col num12" style="display: inline-block;vertical-align: top;width: 100%">
                                      <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                          <tbody><tr style="vertical-align: top">
                                              <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 30px;padding-right: 0px;padding-bottom: 30px;padding-left: 0px;border-top: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-left: 0px solid transparent">
<table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" width="100%">
  <tbody><tr style="vertical-align: top">
    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;padding-top: 25px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
        <div style="color:#ffffff;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">            
        	<div style="font-size:18px;line-height:22px;text-align:center;color:#ffffff;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;"><span style="font-size:24px; line-height:29px;" mce-data-marked="1"><strong>Tu amigo <?=$name;?> te ha invitado</strong></span></div>
        </div>
    </td>
  </tr>
</tbody></table>
<table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" width="100%">
  <tbody><tr style="vertical-align: top">
    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;padding-top: 0px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
        <div style="color:#dedede;line-height:150%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">            
        	<div style="font-size:14px;line-height:21px;text-align:center;color:#dedede;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">Entra y&nbsp;reg&#237;strate; ingresa al siguien enlace y suma puntos para participar por uno de los 5 viajes a <strong>San Andr&#233;s</strong> con un&nbsp;acompa&#241;ante.</div>
        </div>
    </td>
  </tr>
</tbody></table>
<table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" align="center" width="100%" border="0" cellspacing="0">
    <tbody><tr style="vertical-align: top">
        <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px" align="center">
            <div>
                <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top;border-top: 1px solid #555555;width: 100%" align="center" border="0" cellspacing="0">
                    <tbody><tr style="vertical-align: top">
                        <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" align="center">
                            &nbsp;
                        </td>
                    </tr>
                </tbody></table>
            </div>
        </td>
    </tr>
</tbody></table><table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" width="100%">
  <tbody><tr style="vertical-align: top">
    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
        <div style="color:#ffffff;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">            
        	<div style="font-size:14px;line-height:17px;text-align:center;color:#ffffff;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">Tu c&#243;digo de verificiaci&#243;n es:<br></div>
        </div>
    </td>
  </tr>
</tbody></table>
<table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" width="100%">
  <tbody><tr style="vertical-align: top">
    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
        <div style="color:#ffffff;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">            
        	<div style="font-size:14px;line-height:17px;text-align:center;color:#ffffff;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;"><span style="font-size:36px;font-family:arial, helvetica, sans-serifpx; line-height:43px;"><strong><span style="line-height: 43px; font-size: 36px;"><?=$code;?></span></strong></span><br></div>
        </div>
    </td>
  </tr>
</tbody></table>
<table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" align="center" width="100%" border="0" cellspacing="0">
    <tbody><tr style="vertical-align: top">
        <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px" align="center">
            <div>
                <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top;border-top: 1px solid #555555;width: 100%" align="center" border="0" cellspacing="0">
                    <tbody><tr style="vertical-align: top">
                        <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" align="center">
                            &nbsp;
                        </td>
                    </tr>
                </tbody></table>
            </div>
        </td>
    </tr>
</tbody></table><table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr style="vertical-align: top">
    <td class="center" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;text-align: center;padding-top: 20px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px" align="center">

        <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr style="vertical-align: top">
            <td class="button-container" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" align="center">
              <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">

                <tbody><tr style="vertical-align: top">
                  <td class="button" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;text-align: center;height: 40px" width="100%" valign="middle">
            
                      <!--[if mso]>
                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://dev.tocalapared.com" 
                            style="
                            height:70px;
                            v-text-anchor:middle;
                            width:166px;" 
                            arcsize="22%" 
                            strokecolor="" 
                            fillcolor="#7bb200" >
                        <w:anchorlock/>
                          <center 
                            style="color:#ffffff;
                            font-family:Arial, &#39;Helvetica Neue&#39;, Helvetica, sans-serif;
                            font-size:20px">
                             <div style="font-family:inherit;font-size:12px;line-height:24px;color:#ffffff;text-align:left;"><span style="font-size: 20px; line-height: 40px;" data-mce-style="font-size: 20px; line-height: 28px;">REGISTRARME</span></div>
                            </center>
                        </v:roundrect>
                      <![endif]-->

                      <a style="display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;background-color: #7bb200;                        -webkit-border-radius: 15px;                         -moz-border-radius: 15px;                         border-radius: 15px;                         vertical-align: middle;                         padding-top: 5px;                         padding-right: 25px;                        padding-bottom: 5px;                        padding-left: 25px;                        text-align:;                        font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;-webkit-border-radius: 15px;-moz-border-radius: 15px;border-radius: 15px;border-top: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 5px solid #628e00;border-left: 0px solid transparent;color: #ffffff;max-width: 166px;width: 100%;mso-hide: all" href="<?=$url;?>" target="_blank">
                          <div style="text-align: center !important;font-family: inherit;font-size: 12px;line-height: 24px;color: #ffffff"><span style="font-size: 20px; line-height: 40px;" data-mce-style="font-size: 20px; line-height: 28px;">REGISTRARME</span></div>
                      </a>

                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>
        </tbody></table>

    </td>
  </tr>
</tbody></table><table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" width="100%">
  <tbody><tr style="vertical-align: top">
    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
        <div style="color:#dddddd;line-height:180%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">            
        	<div style="font-size:14px;line-height:25px;text-align:center;color:#dddddd;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;"><span style="font-size:11px; line-height:25px;">Si no puedes ingresar con el enlace, trata copiando y pegano la siguiente url en tu navegador:</span><br></div><div style="font-size:14px;line-height:25px;text-align:center;color:#dddddd;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;"><span style="text-decoration: underline; font-size:14px; line-height:25px;"><span style="font-size: 12px; line-height: 21px;" data-mce-style="font-size: 12px; line-height: 13px;"><?=$url;?></span></span></div>
        </div>
    </td>
  </tr>
</tbody></table>
<table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" align="center" width="100%" border="0" cellspacing="0">
    <tbody><tr style="vertical-align: top">
        <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px" align="center">
            <div>
                <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top;border-top: 0px solid transparent;width: 100%" align="center" border="0" cellspacing="0">
                    <tbody><tr style="vertical-align: top">
                        <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" align="center">
                            &nbsp;
                        </td>
                    </tr>
                </tbody></table>
            </div>
        </td>
    </tr>
</tbody></table>                                              </td>
                                          </tr>
                                      </tbody></table>
                                  </div>
                                  <!--[if (gte mso 9)|(IE)]>
                                  </td><td>
                                  <![endif]-->
                                  <!--[if (gte mso 9)|(IE)]>
                                          </td>
                                      </tr>
                                  </table>
                                  <![endif]-->
                              </td>
                          </tr>
                      </tbody></table>
                    </td>
                  </tr>
                </tbody></table>
              </td>
            </tr>
          </tbody></table>
          <!--[if (gte mso 9)|(IE)]>
                     </td>
              </tr>
          </table>
          <![endif]-->
          <!--[if (gte mso 9)|(IE)]>
          <table width='500' class="ieCell" align="center" cellpadding="0" cellspacing="0" border="0">
              <tr>
                  <td>
          <![endif]-->
          <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top;background-color: #ffffff" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
            <tbody><tr style="vertical-align: top">
              <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" width="100%">
                <table class="container" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;max-width: 500px;margin: 0 auto;text-align: inherit" cellpadding="0" cellspacing="0" align="center" width="100%" border="0"> 
                  <tbody><tr style="vertical-align: top">
                    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" width="100%">
                      <table class="block-grid" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;width: 100%;max-width: 500px;color: #333;background-color: transparent" cellpadding="0" cellspacing="0" width="100%" bgcolor="transparent">
                          <tbody><tr style="vertical-align: top">
                              <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;text-align: center;font-size: 0">
                                  <!--[if (gte mso 9)|(IE)]>
                                  <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                      <tr>
                                          <td valign="top">
                                  <![endif]-->
                                  <div class="col num12" style="display: inline-block;vertical-align: top;width: 100%">
                                      <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                          <tbody><tr style="vertical-align: top">
                                              <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 30px;padding-right: 0px;padding-bottom: 30px;padding-left: 0px;border-top: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-left: 0px solid transparent">
<table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" width="100%" border="0" align="center">
    <tbody><tr style="vertical-align: top">
        <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;width: 100%;text-align: center" align="center">
            <div style="max-width: 100%;" align="center">
              <a href="" target="_blank">
                <img class="center" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: 100%;clear: both;display: block;border: none;height: auto;line-height: 100%;margin: 0 auto;float: none;max-width: 204px" border="0" width="100%" align="center" src="<?=$logo[0];?>" alt="Comfenalco Valle" title="Comfenalco Valle">
              </a>
            </div>
        </td>
    </tr>
</tbody></table>
                                              </td>
                                          </tr>
                                      </tbody></table>
                                  </div>
                                  <!--[if (gte mso 9)|(IE)]>
                                  </td><td>
                                  <![endif]-->
                                  <!--[if (gte mso 9)|(IE)]>
                                          </td>
                                      </tr>
                                  </table>
                                  <![endif]-->
                              </td>
                          </tr>
                      </tbody></table>
                    </td>
                  </tr>
                </tbody></table>
              </td>
            </tr>
          </tbody></table>
          <!--[if (gte mso 9)|(IE)]>
                     </td>
              </tr>
          </table>
          <![endif]-->
        </td>
      </tr>
    </tbody></table>

</body></html>