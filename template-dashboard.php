<?php
    /**
     * Template Name: Dashboard
     */ 
    get_header('page');
?>
<section id="dashboard">
    <div class="container main">
        <?php if(is_user_logged_in()) : $current_user = wp_get_current_user(); ?>
            <?php if (get_user_role() == 'subscriber') : ?>
                <?php if (is_pioneer($current_user->ID)) : ?>
                    <h2 class="not-logged-in">Debes actualizar tus datos para continuar</h2>
                    <script>
                        jQuery(document).ready(function() {
                            var alert = '<h3>Bienvenido, para continuar por favor actualiza tus datos, al hacerlo, sumaras <strong><?=get_theme_option('update_points');?></strong> puntos.</h3>';
                            alert_modal(alert);
                            jQuery('#alert-modal').on('hidden.bs.modal', function(){
                                window.location.href = "<?php bloginfo('url'); ?>/actualizar-datos/";
                            });
                        });
                    </script>
                <?php else: ?>
                    <div class="col-xs-12 col-md-8">
                        <?php get_template_part('templates/user/dashboard', 'tabs'); ?>    
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <?php get_template_part('templates/user/dashboard', 'points'); ?>
                    </div>
                <?php endif; ?>
            <?php elseif (get_user_role() == 'editor') : ?>
                <div class="col-xs-12">
                    <?php get_template_part('templates/user/dashboard', 'editor'); ?>
                </div>
            <?php endif; ?>
        <?php else : ?>
            <h2 class="not-logged-in">Debes iniciar sesión con tu correo electrónico y contraseña para continuar.</h2>
            <script>
                jQuery(document).ready(function() {
                    jQuery('#login-modal').modal('show');
                    jQuery('#login-modal').on('hidden.bs.modal', function() {
                        window.location.href = '<?php bloginfo('home'); ?>';
                    });
                });    
            </script>
        <?php endif; ?>
    </div>
</section>
<?php get_footer(); ?>