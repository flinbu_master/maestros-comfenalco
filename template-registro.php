<?php
    /**
     * Template Name: Registro
     */ 

    error_reporting(0);
    if (is_user_logged_in() && (get_query_var('uaction') == 'update' || get_query_var('uaction') == 'new')) {
        if (get_query_var('uaction') == 'new' && get_user_role() != 'editor') {
            wp_redirect(get_permalink(get_theme_option('dashboard_page')));
            exit; 
        } else {
            get_header('page');
            get_template_part('templates/forms/register', 'form');
            get_footer();        
        }    
    } elseif (is_user_logged_in()) {
        wp_redirect(get_permalink(get_theme_option('dashboard_page')));
        exit;
    } elseif (!is_user_logged_in() && get_query_var('uaction') == 'new') {
        wp_redirect(get_bloginfo('url'));
        exit;        
    } elseif (!is_user_logged_in() && !get_query_var('ref_code')) {
        wp_redirect(get_bloginfo('url'));
        exit;
    } else {
        get_header('page');
        get_template_part('templates/forms/register', 'form');
        get_footer(); 
    }
?>