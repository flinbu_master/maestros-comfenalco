<?php get_template_part('templates/admin/admin', 'functions'); ?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			meta_tags();
			wp_head();
		?>
	</head>
	<body <?php body_class(); ?>>
		<main>
			<header>
				<nav class="navbar navbar-default">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" data-toggle="collapse" data-target="#main-menu" aria-expanded="false" class="navbar-toggle collapsed">
								<span class="sr-only">Toggle Navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a href="<?php bloginfo('url'); ?>" class="navbar-brand">Maestros de la Gente</a>
						</div>
						<div class="collapse navbar-collapse" id="main-menu">
							<ul class="nav navbar-nav navbar-right">
								<li><a href="<?php the_permalink(); ?>"/>Buscar</a></li>
								<?php if (backend_allowed(get_current_user_id())) : ?>
									<li><a href="<?php the_permalink(); ?>/manager/"/>Manager</a></li>
								<?php endif; ?>
								<li><a href="#logout" class="logout-btn">Cerrar Sesión</a></li>
							</ul>
						</div>
					</div>
				</nav>
			</header>