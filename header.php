<!DOCTYPE html>
<html>
    <head>
        <?php
            meta_tags();
            wp_head();
        ?>
    </head>
    <body <?=(is_front_page() || is_home()) ? 'id="page-top"' : '';?> <?php body_class((is_front_page() || is_home()) ? 'index' : ''); ?>>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" data-toggle="collapse" data-target="#main-menu" class="navbar-toggle">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?php bloginfo('home'); ?>" class="navbar-brand">
                        <?php
                            $logo = wp_get_attachment_image_src(get_theme_option('logo'), 'full');
                            if($logo) :
                        ?>
                            <img src="<?=$logo[0];?>" alt="<?php bloginfo('name'); ?>" />
                        <?php else : ?>
                            <h1 class="logo"><?php bloginfo('name'); ?></h1>
                        <?php endif; ?>
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="main-menu">
                    <?php 
                        wp_nav_menu(array(
                            'theme_location' => 'header_menu',
                            'depth' => 0,
                            'container' => false,
                            'fallback_cb' => false,
                            'menu_class' => 'nav navbar-nav navbar-right',
                            'walker' => new BootstrapNavMenuWalker()
                        ));
                    ?>
                </div>
                <div class="supersubsidio"><img src="<?=get_template_directory_uri(); ?>/assets/img/vigilado.png"></div>
            </div>
        </nav>