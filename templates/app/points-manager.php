<div class="col-xs-12 points-story">
	<h3>Resumen general de puntos</h3>
	<div class="points-table row">
		<?php $user_points = get_points_story(get_current_user_id()); ?>
		<div class="form-group col-xs-12">
			<input type="text" class="hidden" id="filter" />
			<label for="filter-status">Filtrar por: </label>
			<select id="filter-status" class="filter-status">
				<option value="clear">No filtrar</option>
				<option value="aplicados">Aplicados</option>
				<option value="pendientes">Pendientes</option>
				<option value="invalidos">Invalidos</option>
			</select>
		</div>
		<div class="table-responsive col-xs-12">
			<table class="table table-bordered" data-page-navigation=".pagination" data-page-size="20" data-sort="false" data-filter="#filter" data-filter-text-only="true">
				<thead>
					<tr>
						<th>Pts.</th>
						<th>Motivo</th>
						<th>Fecha de Activación</th>
						<th>Estado</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($user_points as $points) : ?>
						<tr>
							<td><?=$points['points'];?></td>
							<td><?=str_replace('&lt;', '- <strong>(',str_replace('&gt;', ')</strong>', $points['action']));?></td>
							<td><?=get_nice_date($points['active_date'], 'Pendiente');?></td>
							<?php
								$state_label = 'label-default status-pendientes';
								if ($points['status'] == 'aplicados') {
									$state_label = 'label-success status-aplicados';
								} elseif ($points['status'] == 'invalidos') {
									$state_label = 'label-warning status-na';
								}
							?>
							<td><span class="label <?=$state_label;?>" title="<?=$points['status'];?>"><?=$points['status'];?></span></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot class="hide-if-no-paging">
					<tr>
						<td colspan="5">
							<ul class="pagination pagination-centered"></ul>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>