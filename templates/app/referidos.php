<input type="hidden" id="user-id" value="<?=get_current_user_id();?>">
<div class="col-xs-12" id="referidos">
    <div class="row">
        <div class="header col-xs-12">
            <h3 class="title"><?=get_theme_option('challenge_title');?></h3>
            <p><?=get_theme_option('challenge_description');?></p>
        </div>
        <div class="instructions col-xs-12"><?=get_theme_option('challenge_instructions');?></div>
        <div class="col-xs-12 referidos">
            <h3>Invitalos aquí y suma puntos</h3>
            <table class="table table-bordered table-striped items" items="1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="source">
                        <th scope="row">1</th>
                        <td class="name">
                            <input type="text" placeholder="Nombre" class="name" />
                        </td>
                        <td class="email">
                            <input type="email" placeholder="Correo" class="email" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <button type="button" onclick="add_referido();" class="btn btn-sm btn-default adder"><i class="fa fa-plus"></i> Añadir Referido</button>
            <div class="divider clearfix"></div>
            <button type="button" onclick="invite_the_people();" class="btn btn-lg btn-primary inviter">Invitar a mis referidos</button>
        </div>
    </div>
</div>