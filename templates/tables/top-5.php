<?php $top = get_top(5); ?>
<div class="bs-example vamos">
    <table class="table">
        <thead>
            <tr>
                <h3><i class="fa fa-trophy"></i> ¡ASÍ VAMOS!</h3>
            </tr>
        </thead>
        <tbody>
            <?php for ($i = 0; $i < count($top); $i++) : ?>
                <tr class="<?=($i%2) ? 'blank' : 'active';?>">
                    <td class="number"><?=$i + 1;?></td>
                    <td class="nombre"><?=$top[$i]['display_name'];?></td>
                    <td class="points"><?=$top[$i]['points'];?> pts.</td>
                </tr>
            <?php endfor; ?>
        </tbody>
    </table>
</div>