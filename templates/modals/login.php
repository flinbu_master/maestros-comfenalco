<?php global $login_place; ?>
<div class="modal fade" id="login-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">SUMA PUNTOS INVITANDO A TUS MAESTROS COLEGAS</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-md-7">
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="row">
                                        <img src="<?=get_template_directory_uri();?>/assets/img/vigilado.png" />
                                    </div>
                                </div>
                                <div class="col-xs-10"><?=replace_labels(get_theme_option('login_instructions'));?></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-5">
                            <div class="row">
                                <?php $login_place = 'modal'; get_template_part('templates/forms/login'); ?>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="clearfix"></div>        
            </div>
        </div>
    </div>
</div>