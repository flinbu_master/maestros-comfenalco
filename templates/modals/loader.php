<div class="modal fade" id="loader-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progress-bar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">
                        <span>0</span>%
                    </div>
                </div>
                <ul class="list-group">
                </ul>
            </div>
            <div class="modal-footer" style="display: none;">
                <button type="button" data-dismiss="modal" class="btn btn-default center-block">Aceptar</button>
            </div>
        </div>
    </div>
</div>