<?php
	if (!is_user_logged_in()) {
    	get_template_part('templates/modals/login');
    }
    get_template_part('templates/modals/alert');
    get_template_part('templates/modals/loader'); 
    get_template_part('templates/modals/share'); 
?>