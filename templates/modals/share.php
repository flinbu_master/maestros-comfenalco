<div class="modal fade" id="share-modal">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title">¡Compartir!</h3>
			</div>
			<div class="modal-body center-text">
				<?php 
                    if (function_exists('sharing_display')) {
                        sharing_display( '', true ); 
                    }
                ?>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default center-block">Cancelar</button>
			</div>
		</div>
	</div>
</div>