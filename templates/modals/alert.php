<div class="modal fade" id="alert-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body text-center"></div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default center-block">Aceptar</button>
            </div>
        </div>
    </div>
</div>