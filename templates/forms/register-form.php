<?php 
    $valle_municipios = get_custom_data('valle_municipios'); 
    $asesores = get_custom_data('asesores');
    $instituciones = get_custom_data('instituciones');
    $values = array();
    if (get_query_var('uaction') == 'update') {
        $values = get_all_user_data(get_current_user_id());
    }
?>
<div class="container main registro">
    <div class="col-xs-12">
        <?php
            if (get_query_var('uaction') == 'update') {
                $h1 = 'Actualiza tus datos';
            } elseif (get_query_var('uaction') == 'new') {
                $h1 = 'Registro Nuevo Usuario';
            } else {
                $h1 = 'Regístrate y Suma Puntos';
            }
        ?>
        <h2><?=$h1;?></h2>
        <form class="register">
            <?php if (get_query_var('uaction') == 'update' || get_query_var('uaction') == 'new') : ?>
                <input type="hidden" id="current-user-id" value="<?=get_current_user_id();?>">
            <?php endif; ?>
            <div class="form-group col-xs-12 col-md-6">
                <label for="name" class="required">Primer Nombre:</label>
                <input type="text" class="form-control" name="name" id="name" value="<?=($values['NOMBRE']) ? $values['NOMBRE'] : '';?>" />
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="second-name">Segundo Nombre:</label>
                <input type="text" class="form-control" name="second-name" id="second-name" value="<?=($values['SEGUNDO_NOMBRE']) ? $values['SEGUNDO_NOMBRE'] : '';?>" />
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="lastname" class="required">Primer Apellido:</label>
                <input type="text" class="form-control" name="lastname" id="lastname" value="<?=($values['APELLIDO']) ? $values['APELLIDO'] : '';?>" />
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="second-lastname">Segundo Apellido:</label>
                <input type="text" class="form-control" name="second-lastname" id="second-lastname" value="<?=($values['SEGUNDO_APELLIDO']) ? $values['SEGUNDO_APELLIDO'] : '';?>" />
            </div>
            <div class="form-group col-xs-12 col-md-6 <?=(get_query_var('uaction') == 'update') ? 'hidden' : '';?>">
                <label for="doc-type" class="required">Tipo de Documento:</label>
                <select name="doc-type" id="doc-type" class="form-control">
                    <option value="blank">-- Seleccione un opción --</option>
                    <option value="cc" <?=($values['TIPO_DOCUMENTO'] == 'cc') ? 'selected="selected"' : '';?>>Cédula de Ciudadanía</option>
                    <option value="ce" <?=($values['TIPO_DOCUMENTO'] == 'ce') ? 'selected="selected"' : '';?>>Cédula de Extranjería</option>
                    <option value="ps" <?=($values['TIPO_DOCUMENTO'] == 'ps') ? 'selected="selected"' : '';?>>Pasaporte</option>
                </select>
            </div>
            <div class="form-group col-xs-12 col-md-6 <?=(get_query_var('uaction') == 'update') ? 'hidden' : '';?>">
                <label for="document" class="required">Número de Documento:</label>
                <input type="text" class="form-control" name="document" id="document" value="<?=($values['NUM_DOCUMENTO']) ? $values['NUM_DOCUMENTO'] : '';?>" />
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="birthday">Fecha de Naciemiento:</label>
                <input type="text" name="birthday" class="form-control date-picker" id="birthday" placeholder="aaaa-mm-dd" value="<?=($values['CUMPLEANOS']) ? $values['CUMPLEANOS'] : ''; ?>" />
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="city" class="required">Ciudad de Residencia:</label>
                <select name="city" id="city" class="form-control chosen-select" data-placeholder="Seleccione una ciudad">
                    <?php foreach($valle_municipios as $city) : ?>
                        <option value="<?=$city;?>" <?=($values['CIUDAD'] == $city) ? 'selected="selected"' : '';?>><?=$city;?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-xs-12 col-md-6 form-group">
                <label for="phone" class="required">Celular:</label>
                <input type="text" name="phone" class="form-control" id="phone" value="<?=($values['CELULAR']) ? $values['CELULAR'] : '';?>" />
            </div>
            <div class="col-xs-12 col-md-6 form-group">
                <label for="address" class="required">Dirección:</label>
                <input type="text" name="address" class="form-control" id="address" value="<?=($values['DIRECCION']) ? $values['DIRECCION'] : '';?>" />
            </div>
            <div class="col-xs-12 col-md-6 form-group">
                <label for="hood" class="required">Barrio:</label>
                <input type="text" name="hood" class="form-control" id="hood" value="<?=($values['BARRIO']) ? $values['BARRIO'] : '';?>" />
            </div>
            <div class="divider"></div>
            <?php $total_childs = 6; ?>
            <div class="form-group col-xs-12 col-md-6">
                <label for="childrens">Número de Hijos:</label>
                <select name="childrens" id="childrens" class="form-control">
                    <?php for ($i = 0; $i <= $total_childs; $i++) : ?>
                        <option value="<?=$i;?>" <?=($values['CANT_HIJOS'] == $i) ? 'selected="selected"' : '';?>><?=$i;?></option>
                    <?php endfor; ?>
                </select>
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <div class="row">
                    <?php for ($i = 1; $i <= $total_childs; $i++) : ?>
                    <div class="children children-<?=$i;?> col-xs-6 col-md-4 <?=($i > $values['CANT_HIJOS']) ? 'hidden' : '';?>">
                        <label for="child-<?=$i;?>">Edad Hijo <?=$i;?>:</label>
                        <?php 
                            if ($values['EDADES_HIJOS']) {
                                $edades_hijos = explode('/', $values['EDADES_HIJOS']);
                            }
                        ?>
                        <input type="number" min="0" max="99" step="1" class="form-control" name="child-<?=$i;?>" id="child-<?=$i;?>" value="<?=($edades_hijos) ? $edades_hijos[$i - 1] : '';?>" />
                    </div>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="marital-status">Estado Civil:</label>
                <select name="marital-status" class="form-control" id="marital-status">
                    <option value="blank">-- Seleccione una opción --</option>
                    <option value="Soltero" <?=($values['EST_CIVIL'] == 'Soltero') ? 'selected="selected"' : ''; ?>>Soltero(a)</option>
                    <option value="Casado" <?=($values['EST_CIVIL'] == 'Casado') ? 'selected="selected"' : ''; ?>>Casado(a)</option>
                    <option value="Unión Libre" <?=($values['EST_CIVIL'] == 'Unión Libre') ? 'selected="selected"' : ''; ?>>Unión Libre</option>
                    <option value="Viuda(o)" <?=($values['EST_CIVIL'] == 'Viuda(o)') ? 'selected="selected"' : ''; ?>>Viuda(o)</option>
                    <option value="Divorciado(a)" <?=($values['EST_CIVIL'] == 'Divorciado(a)') ? 'selected="selected"' : ''; ?>>Divorciado(a) / Separado(a)</option>
                </select>
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="scale">Escalafón</label>
                <select name="scale" id="scale" class="form-control">
                    <option value="blank">-- Seleccione una opción --</option>
                    <?php for ($i = 1; $i < 15; $i++) :?>
                        <option value="<?=$i;?>" <?=($values['ESCALAFON'] == $i) ? 'selected="selected"' : ''; ?>><?=$i;?></option>
                    <?php endfor; ?>
                </select>
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="decree">Decreto que lo ampara:</label>
                <select name="decree" id="decree" class="form-control">
                    <option value="">-- Seleccione una opción --</option>
                    <option value="1278 de 2002" <?=($values['DECRETO'] == '1278 de 2002') ? 'selected="selected"' : ''; ?>>Decreto 1278 de 2002</option>
                    <option value="2277 de1 1979" <?=($values['DECRETO'] == '2277 de1 1979') ? 'selected="selected"' : ''; ?>>Decreto 2277 de 1979</option>
                </select>
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="institute">Institución:</label>
                <select name="institute" id="institute" class="form-control chosen-select" data-placeholder="Seleccione la institución a la que pertenece">
                    <?php for ($i = 0; $i < count($instituciones); $i++) : ?>
                        <option value="<?=$instituciones[$i];?>" <?=($values['INSTITUCION'] == $instituciones[$i]) ? 'selected="selected"' : '';?>><?=$instituciones[$i];?></option>
                    <?php endfor; ?>
                </select>
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="adviser">Asesor que lo atendió:</label>
                <select name="adviser" id="adviser" class="form-control">
                    <option value="blank">-- Seleccione una opción --</option>
                    <?php foreach ($asesores as $asesor) : ?>
                        <option value="<?=$asesor['name'];?> - <?=$asesor['city'];?>" <?=($values['NOM_ASESOR'] == $asesor['name']) ? 'selected="selected"' : ''; ?>><?=$asesor['name'];?> - <?=$asesor['city'];?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="divider"></div>
            <div class="form-group col-xs-12 col-md-6">
                <label>¿Tiene vivienda propia?</label>
                <div class="col-xs-12">
                    <label class="radio-inline" for="home-yes">
                        <input type="radio" name="home" id="home-yes" value="Si" <?=($values['VIVIENDA'] == '1') ? 'checked="checked"' : ''; ?> /> Si
                    </label>
                    <label class="radio-inline" for="home-no">
                        <input type="radio" name="home" id="home-no" value="No" <?=($values['VIVIENDA'] == '0') ? 'checked="checked"' : ''; ?> /> No
                    </label>
                </div>
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label>¿Cuenta con ahorros para adquirir vivienda propia?</label>
                <div class="col-xs-12">
                    <label for="saves-yes" class="radio-inline">
                        <input type="radio" name="saves" value="Si" id="saves-yes" <?=($values['AHORROS'] == '1') ? 'checked="checked"' : ''; ?> /> Si
                    </label>
                    <label for="saves-no" class="radio-inline">
                        <input type="radio" name="saves" value="No" id="saves-no" <?=($values['AHORROS'] == '0') ? 'checked="checked"' : ''; ?> /> No
                    </label>
                </div>
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="sport">¿Qué deporte practica usted?</label>
                <input type="text" name="sport" class="form-control" id="sport" value="<?=($values['DEPORTE']) ? $values['DEPORTE'] : '';?>" />
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="family-activities">¿Qué actividades de entretenimiento realiza con su familia en su tiempo libre?</label>
                <input type="text" name="family-activities" class="form-control" id="family-activities" value="<?=($values['ACT_TL_FAMILIA']) ? $values['ACT_TL_FAMILIA'] : '';?>" />
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="family-place">¿En donde realiza estas actividades?</label>
                <input type="text" name="family-place" class="form-control" id="family-place" value="<?=($values['LUGAR_TL_FAMILIA']) ? $values['LUGAR_TL_FAMILIA'] : '';?>" />
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="programs">¿Qué programas de educación y capacitación son de interés para usted?</label>
                <input type="text" name="programs" class="form-control" id="programs" value="<?=($values['PROG_EDU']) ? $values['PROG_EDU'] : '';?>" />
            </div>
            <div class="form-group col-xs-12 col-md-6">
                <label for="family-programs">¿Qué programas de educación y capacitación son de interés para su esposo y/o padres?</label>
                <input type="text" name="family-programs" class="form-control" id="family-programs" value="<?=($values['CURSOS_ESP_PADRES']) ? $values['CURSOS_ESP_PADRES'] : '';?>" />
            </div>
            <?php if (get_query_var('ref_code') || get_query_var('uaction') == 'new') : ?>
                <div class="divider"></div>
                <?php if (get_query_var('ref_code')) : ?>
                    <div class="form-group form-group-lg col-xs-12 col-md-8 col-md-offset-2">
                        <label for="refercode" class="center-text control-label">Código de Referido</label>
                        <input type="text" name="refercode" class="form-control xl text-center" id="refercode" value="<?=get_query_var('ref_code');?>" />
                    </div>            
                <?php endif; ?>
                <?php if(get_query_var('uemail') || get_query_var('uaction') == 'new') : ?>
                    <div class="form-group col-xs-12 col-md-6">
                        <label for="email">Correo Electrónico:</label>
                        <input type="email" name="email" class="form-control" <?=(get_query_var('uaction') != 'new') ? 'readonly' : '';?> id="email" value="<?=(get_query_var('uemail')) ? urldecode(get_query_var('uemail')) : '';?>" />
                    </div>
                	<div class="form-group col-xs-12 col-md-6">
                        <label for="password">Contraseña:</label>
                        <input type="password" name="password" id="password" class="form-control" />
                    </div>
                <?php endif; ?>    
            <?php endif; ?>
            <?php if (get_query_var('uaction') == 'update') : ?>
                <div class="divider"></div>
                <div class="form-group col-xs-12 col-md-6">
                    <label for="password">Nueva Contraseña:</label>
                    <input type="password" name="password" id="password" class="form-control" />
                    <span class="help-block">Si desea cambiar su contraseña, ingrese la nueva. Deberá iniciar sesión de nuevo con esta contraseña.</span>
                </div>
            <?php endif; ?>
            <div class="divider"></div>
            <div class="form-group col-xs-12 col-md-6 col-md-offset-3 <?=(get_query_var('uaction') == 'update' || get_query_var('uaction') == 'new') ? 'hidden' : '';?>">
                <label for="ter_con" class="center-text">
                    <input type="checkbox" name="ter_con" id="ter_con" <?=(get_query_var('uaction') == 'update') ? 'checked="checked"' : '';?> /> Acepto los <a href="<?=get_permalink(get_theme_option('terms_page'));?>" title="Términos y condiciones">Términos y Condiciones</a>
                </label>
            </div>
            <div class="form-group col-xs-12 col-md-4 col-md-offset-4">
                <?php
                    $action = 'register';
                    $btn_label = 'Finalizar Registro';
                    if (get_query_var('uaction') == 'update') {
                        $action = 'update';
                        $btn_label = 'Actualizar Datos';
                    }
                    if (get_query_var('uaction') == 'new') {
                        $action = 'new_user';
                        $btn_label = 'Crear Usuario';
                    }
                ?>
                <button type="submit" id="register" onclick="check_register('<?=$action;?>')" class="btn btn-primary prevent-default"><?=$btn_label;?></button>  
                <?php if (get_query_var('uaction') == 'update' || get_query_var('uaction') == 'new') : ?>
                    <button type="button" onclick="goto_page('<?=get_permalink(get_theme_option('dashboard_page'));?>');" class="btn btn-default prevent-default">Cancelar</button>
                <?php endif; ?>
            </div>
        </form>
    </div>
</div>