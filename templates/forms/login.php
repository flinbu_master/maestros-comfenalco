<?php global $login_place; ?>
<form class="form-signin">
    <?php if (!is_user_logged_in()) : ?>
        <div class="form-signin-heading">INGRESA A TU PERFÍL <img src="<?=get_template_directory_uri();?>/assets/img/owl.png" /></div>
        <input type="text" class="form-control" placeholder="Correo Electrónico" required id="user" />
        <input type="password" placeholder="Contraseña" class="form-control" required id="pass" />
        <button class="btn btn-lg btn-primary btn-block enviar prevent-default" onclick="login('<?=$login_place;?>');" type="submit">INGRESAR</button>
        <a href="<?=get_permalink(get_theme_option('recovery_page'));?>" class="passrecovery">¿Has olvidado tu contraseña?</a>
    <?php else : $current_user = wp_get_current_user();?>
        <div class="form-signin-heading logged-in">Hola, <?=$current_user->display_name;?> <img src="<?=get_template_directory_uri();?>/assets/img/owl.png" /></div>
        <div class="data col-xs-12 text-center">
            <p>En este momento tienes:</p>
            <div class="points"><?=get_user_points($current_user->ID)->active;?> <span>Puntos</span></div>
            <a href="<?=get_permalink(get_theme_option('dashboard_page'));?>" class="btn btn-lg btn-primary enviar">Más información</a>
        </div>
        <div class="clearfix"></div>
    <?php endif; ?>
</form>