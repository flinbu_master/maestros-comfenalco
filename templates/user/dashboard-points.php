<?php
	$user_id = get_current_user_id();
	$user_points = get_user_points($user_id);
?>
<div class="row user-points">
	<div class="col-xs-12 points active">
		<h3>TUS PUNTOS</h3>
		<div class="point"><?=$user_points->active;?></div>
	</div>
	<div class="col-xs-12 points pending">
		<h4>PUNTOS PENDIENTES</h4>
		<div class="point"><?=$user_points->pending;?></div>
	</div>
</div>