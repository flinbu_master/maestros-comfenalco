<div>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#reto-actual" aria-control="reto-actual" role="tab" data-toggle="tab">Reto Actual</a>
        </li>
        <li role="presentation">
            <a href="#mis-puntos" aria-control="mis-puntos" role="tab" data-toggle="tab">Mis Puntos</a>
        </li>
        <li role="presentation">
            <a href="#mi-perfil" aria-control="mi-perfil" role="tab" data-toggle="tab">Mi Perfil</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade in active" role="tabpanel" id="reto-actual">
            <?php get_template_part('templates/app/referidos'); ?>
        </div>
        <div class="tab-pane fade" role="tabpanel" id="mis-puntos">
            <?php get_template_part('templates/app/points-manager'); ?>
        </div>
        <div class="tab-pane fade" role="tabpanel" id="mi-perfil">
            <?php get_template_part('templates/user/dashboard', 'profile'); ?>
        </div>
    </div>
</div>