<?php $user = wp_get_current_user(); ?>
<div class="row">
	<div class="col-xs-12 col-md-8">
        <h3>Mis registrados:</h3>
        <div class="table-responsive col-xs-12">
            <table class="table table-bordered" data-page-navigation=".pagination" data-page-size="30" data-sort="true">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Documento</th>
                        <th>Nombre</th>
                        <th>Fecha</th>
                        <th>Puntos</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $registers = get_submitted_profiles($user->ID);
                        if ($registers) :
                            $i = 1;
                            foreach($registers as $register) :
                    ?>
                                <tr>
                                    <td><?=$i;?></td>
                                    <td><?=$register['document'];?></td>
                                    <td><?=$register['name'];?></td>
                                    <td><?=$register['date'];?></td>
                                    <td><?=$register['points'];?></td>
                                </tr>
                    <?php       $i++; 
                            endforeach; 
                        endif;
                    ?>
                </tbody>
                <tfoot class="hide-if-no-paging">
                    <tr>
                        <td colspan="5">
                            <ul class="pagination pagination-centered"></ul>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <h4>Hola, <?=$user->data->display_name;?></h4>
        <div class="col-xs-12">
            <a href="<?php bloginfo('url'); ?>/nuevo-registro/" class="btn btn-primary btn-block"><i class="fa fa-user-plus"></i> Añadir Registro</a>
            <a href="#logout" class="btn btn-default btn-block logout-btn"><i class="fa fa-sign-out"></i> Cerrar Sesión</a>
        </div>
    </div>
</div>