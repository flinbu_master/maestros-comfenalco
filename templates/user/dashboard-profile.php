<div class="row user-profile">
    <div class="col-xs-12">
        <div class="btn-group pull-right" role="group">
            <a href="<?php bloginfo('url'); ?>/actualizar-datos/" class="btn btn-defautl"><i class="fa fa-pencil"></i> Editar Perfil</a>
            <a class="btn btn-default logout-btn"><i class="fa fa-sign-out"></i> Cerrar Sesión</a>
        </div>
    </div>
    <section class="profile col-xs-12">
    	<?php 
    		$user = get_user_by('id', get_current_user_id());
    		$user_data = get_all_user_data(get_current_user_id()); 
    	?>
    	<div class="col-xs-12 center-text">
    		<h3><?=$user->data->display_name;?></h3>
    	</div>
    	<div class="col-xs-12">
    		<div class="col-xs-6 text-align-right"><strong>C.C.:</strong></div> 
    		<div class="col-xs-6"><?=$user_data['NUM_DOCUMENTO'];?></div>
    	</div>
    	<div class="col-xs-12">
    		<div class="col-xs-6 text-align-right"><strong>Ciudad:</strong></div> 
    		<div class="col-xs-6"><?=$user_data['CIUDAD'];?></div>
    	</div>
    	<div class="col-xs-12">
    		<div class="col-xs-6 text-align-right"><strong>Celular:</strong></div> 
    		<div class="col-xs-6"><?=$user_data['CELULAR'];?></div>
    	</div>
    	<div class="col-xs-12">
    		<div class="col-xs-6 text-align-right"><strong>Dirección:</strong></div> 
    		<div class="col-xs-6"><?=$user_data['DIRECCION'];?></div>
    	</div>
    	<div class="col-xs-12">
    		<div class="col-xs-6 text-align-right"><strong>Intitución:</strong></div> 
    		<div class="col-xs-6"><?=$user_data['INSTITUCION'];?></div>
    	</div>
    	<div class="col-xs-12">
    		<div class="col-xs-6 text-align-right"><strong>Fecha de Nacimiento:</strong></div> 
    		<div class="col-xs-6"><?=$user_data['CUMPLEANOS'];?></div>
    	</div>
    	<div class="divider"></div>
    	<div class="col-xs-12 center-text">
    		<a href="<?php bloginfo('url');?>/actualizar-datos/" class="btn btn-primary">Editar Perfil</a>
    	</div>
    </section>
</div>