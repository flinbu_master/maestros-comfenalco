<section class="search col-xs-12">
	<div class="container">
		<form action="<?=get_permalink(get_the_ID()); ?>search/" method="POST" class="col-xs-12 col-md-6 col-md-offset-3">
			<div class="form-group col-xs-12">
				<input type="text" placeholder="Número de identificación" class="form-control" id="search-box" name="q" value="<?=(get_query_var('uaction') == 'search') ? $_POST['q'] : '';?>">
				<button type="submit" class="btn btn-primary btn-block">Buscar</button>
			</div>
		</form>
		<div class="divider"></div>
		<div class="results col-xs-12">
			<?php 
				if (get_query_var('uaction') == 'search') :
					get_template_part('templates/admin/admin-search-results');
				endif; 
			?>
		</div>
	</div>
</section>