<div class="col-xs-12">
    <h2>Autorización No Maestros</h2>
    <div class="row">
        <div class="table-responsive col-xs-12">
            <?php 
                if (get_query_var('udoc')) : 
                    $document = get_query_var('udoc');
                    $user = get_users(array(
                        'meta_key' => 'document',
                        'meta_value' => $document,
                        'fields' => 'ID'
                    ));
                    if ($user) :
                        $user_id = $user[0];
                        $userdata = get_all_user_data($user_id);
                        $userdata = unutf8ize($userdata);
                        
            ?>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Documento</th>
                                    <th>Nombre</th>
                                    <th>Activar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?=$userdata['NUM_DOCUMENTO'];?></td>
                                    <td><?=$userdata['NOMBRE'];?> <?=$userdata['APELLIDO'];?></td>
                                    <td>
                                        <?php if(!is_maestro($user_id)) : ?>
                                            <button class="btn btn-primary prevent-default" onclick="authorize_user(<?=$user_id;?>);" type="button">Activar</button>
                                        <?php else : ?>
                                            <span class="label label-success">Usuario Activo</span>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
            <?php endif; else : ?>

            <?php endif; ?>
        </div>
    </div>
</div>