<?php $totals = get_totals(); ?>
<section class="container manager">
	<div class="container-fluid">
		<header>	
			<div class="col-xs-12">
				<div class="btn-toolbar">
					<div class="btn-group" role="group" aria-label="...">
						<a href="<?php the_permalink(); ?>" class="btn btn-default"><i class="fa fa-plus"></i> Añadir Datos</a>
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i> Ver Datos <span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li><a href="#">Todo</a></li>
								<li><a href="#">Afiliados</a></li>
								<li><a href="#">No Afiliados</a></li>
								<li><a href="#">Maestros</a></li>
								<li><a href="#">No Maestros</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>
		<section>
			<article class="col-xs-12 total">
				<div class="page-header">
					<h3>Total Registros</h3>
				</div>
				<p class="number"><?=$totals->all;?></p>
			</article>
			<article class="col-xs-12 col-sm-6 col-md-3 total">
				<div class="page-header">
					<h3>Afiliados</h3>
				</div>
				<p class="number"><?=$totals->afiliados;?></p>
				<p class="pct"><?=$totals->pct_afiliados;?><span>%</span></p>
			</article>
			<article class="col-xs-12 col-sm-6 col-md-3 total">
				<div class="page-header">
					<h3>No Afiliados</h3>
				</div>
				<p class="number"><?=$totals->no_afiliados;?></p>
				<p class="pct"><?=$totals->pct_no_afiliados;?><span>%</span></p>
			</article>
			<article class="col-xs-12 col-sm-6 col-md-3 total">
				<div class="page-header">
					<h3>Maestros</h3>
				</div>
				<p class="number"><?=$totals->maestros;?></p>
				<p class="pct"><?=$totals->pct_maestros;?><span>%</span></p>
			</article>
			<article class="col-xs-12 col-sm-6 col-md-3 total">
				<div class="page-header">
					<h3>No Maestros</h3>
				</div>
				<p class="number"><?=$totals->no_maestros;?></p>
				<p class="pct"><?=$totals->pct_no_maestros;?><span>%</span></p>
			</article>
		</section>
	</div>
</section>