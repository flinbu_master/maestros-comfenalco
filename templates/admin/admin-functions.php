<?php
	function dequeue_this_files() {
		wp_dequeue_style('maestros-styles');
		wp_dequeue_style('freelancer-styles');
		wp_dequeue_style('bootstrap-styles');

		wp_enqueue_style('admin-styles');
	}
	add_action('wp_enqueue_scripts', 'dequeue_this_files');
?>