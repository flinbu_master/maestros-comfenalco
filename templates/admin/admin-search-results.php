<?php
	$document = $_POST['q'];
	$user = get_users(array(
		'meta_key' => 'document',
		'meta_value' => $document,
		'fields' => 'ID'
	));

	if ($user) :
		$user_id = $user[0];
		$userdata = get_all_user_data($user_id);
		$userdata = unutf8ize($userdata);
		$userpoints = get_points_story($user_id);
		$userpoints = unutf8ize($userpoints);
	?>
		<div class="col-xs-12 col-md-4">
			<h3>Datos del Usuario:</h3>
			<div class="row">
				<div class="table-responsive col-xs-12">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="30%"><strong>Nombres:</strong></td>
								<td width="70%"><?=$userdata['NOMBRE'];?> <?=$userdata['SEGUNDO_NOMBRE'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Apellidos:</strong></td>
								<td width="70%"><?=$userdata['APELLIDO'];?> <?=$userdata['SEGUNDO_APELLIDO'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Número de Documento:</strong></td>
								<td width="70%"><?=$userdata['NUM_DOCUMENTO'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Tipo de Documento:</strong></td>
								<td width="70%"><?=$userdata['TIPO_DOCUMENTO'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Fecha de Nacimiento:</strong></td>
								<td width="70%"><?=$userdata['CUMPLEANOS'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Ciudad:</strong></td>
								<td width="70%"><?=$userdata['CIUDAD'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Celular:</strong></td>
								<td width="70%"><?=$userdata['CELULAR'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Dirección:</strong></td>
								<td width="70%"><?=$userdata['DIRECCION'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Barrio:</strong></td>
								<td width="70%"><?=$userdata['BARRIO'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Institución:</strong></td>
								<td width="70%"><?=$userdata['INSTITUCION'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Email:</strong></td>
								<td width="70%"><?=$userdata['EMAIL'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Cantidad de Hijos:</strong></td>
								<td width="70%"><?=$userdata['CANT_HIJOS'];?></td>
							</tr>
							<?php 
								$edades = explode('/', $userdata['EDADES_HIJOS']);
								for ($i = 0; $i < count($edades); $i++) :
									if ($edades[$i] != 0) :
										?>
										<tr>
											<td width="30%"><strong>Edad Hijo <?=$i + 1;?>:</strong></td>
											<td width="70%"><?=$edades[$i];?></td>
										</tr>
										<?php
									endif;
								endfor;
							?>
							<tr>
								<td width="30%"><strong>Estado Civil:</strong></td>
								<td width="70%"><?=$userdata['EST_CIVIL'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Escalafón:</strong></td>
								<td width="70%"><?=$userdata['ESCALAFON'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Decreto:</strong></td>
								<td width="70%"><?=$userdata['DECRETO'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Asesor:</strong></td>
								<td width="70%"><?=$userdata['NOM_ASESOR'];?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Vivienda Propia:</strong></td>
								<td width="70%"><?=($userdata['VIVIENDA'] == 1) ? 'Si' : 'No';?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Ahorros:</strong></td>
								<td width="70%"><?=($userdata['AHORROS'] == 1) ? 'Si' : 'No';?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Deporte:</strong></td>
								<td width="70%"><?=($userdata['DEPORTE']);?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Actividades Familia:</strong></td>
								<td width="70%"><?=($userdata['ACT_TL_FAMILIA']);?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Lugar Actividades:</strong></td>
								<td width="70%"><?=($userdata['LUGAR_TL_FAMILIA']);?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Programas Educativos:</strong></td>
								<td width="70%"><?=($userdata['PROG_EDU']);?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Cursos Padres/Pareja:</strong></td>
								<td width="70%"><?=($userdata['CURSOS_ESP_PADRES']);?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Cursos Hijos:</strong></td>
								<td width="70%"><?=($userdata['CURSOS_HIJOS']);?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Afiliado:</strong></td>
								<td width="70%"><?=($userdata['VAL_AFILIADO'] == 1) ? 'Si' : 'No';?></td>
							</tr>
							<tr>
								<td width="30%"><strong>Fecha de Ingreso:</strong></td>
								<td width="70%"><?=($userdata['FECHA_INGRESO']);?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-8">
			<h3>Detalle de puntos: <small>(<strong>Total:</strong> <?=$userdata['TOTAL_DE_PUNTOS'];?> / <strong>Pendientes:</strong> <?=$userdata['PUNTOS_PENDIENTES'];?>)</small></h3>
			<div class="row">
				<div class="admin-points-table table-responsive col-xs-12">
					<table class="table table-bordered" data-page-navigation=".pagination" data-page-size="40">
						<thead>
							<tr>
								<th>Pts.</th>
								<th>Motivo</th>
								<th data-type="numeric">Fecha Ingreso</th>
								<th data-type="numeric">Fecha Activación</th>
								<th>Estado</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($userpoints as $points) : ?>
								<tr>
									<td><?=$points['points'];?></td>
									<td><?=$points['action'];?></td>
									<td data-value="<?=strtotime($points['submit_date']);?>"><?=$points['submit_date'];?></td>
									<td data-value="<?=strtotime($points['active_date']);?>"><?=$points['active_date'];?></td>
									<?php
										$state_label = 'label-default status-pendientes';
										if ($points['status'] == 'aplicados') {
											$state_label = 'label-success status-aplicados';
										} elseif ($points['status'] == 'invalidos') {
											$state_label = 'label-warning status-na';
										}
									?>
									<td><span class="label <?=$state_label;?>" title="<?=$points['status'];?>"><?=$points['status'];?></span></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot class="hide-if-no-paging">
							<tr>
								<td colspan="5">
									<ul class="pagination pagination-centered"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	<?php else : ?>
		<h3 class="center-text">No se ha encontrado ningún usuario</h4>
	<?php endif; ?>