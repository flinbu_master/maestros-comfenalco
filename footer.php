        <footer class="text-center">
            <div class="footer-above">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-xs-12 col-md-4 izq">
                            <?php echo get_theme_option('more_information'); ?>
                        </div>
                        <div class="footer-col col-xs-12 col-md-4">
                            <?php
                                $footer_menu = get_theme_option('footer_menu');
                                //print_pre($footer_menu);
                                wp_nav_menu(array(
                                    'menu' => $footer_menu->ID,
                                    'menu_class' => 'list-group',
                                    'container' => false
                                ));
                            ?>
                        </div>
                        <div class="footer-col col-xs-12 col-md-4">
                            <?php
                                $footer_logo = wp_get_attachment_image_src(get_theme_option('logo_footer'), 'full');
                                if($footer_logo) :
                            ?>
                            <img src="<?=$footer_logo[0];?>" alt="<?php bloginfo('name'); ?>" class="img-responsive center-block" />
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php copyright(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <?php 
            get_template_part('templates/modals/modals');
            get_template_part('templates/app/loader');
            wp_footer(); 
        ?>
    </body>
</html>