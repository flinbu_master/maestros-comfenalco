<?php get_header('page'); ?>
<section id="page-404">
	<div class="container main">
		<div class="col-xs-12 col-md-6 col-md-offset-3 center-text">
			<h1>404</h1>
			<h3>No encontramos lo que buscas</h3>
		</div>
	</div>
</section>
<?php get_footer(); ?>