<?php
    /**
     * Template Name: Registro
     */ 
    if (is_user_logged_in()) {
        wp_redirect(get_permalink(get_theme_option('dashboard_page')));
        exit;
    } else {
        get_header('page');
        get_template_part('templates/forms/register', 'action');
        get_footer(); 
    }
?>